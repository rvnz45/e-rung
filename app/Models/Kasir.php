<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
class Kasir extends Model
{
    protected $table = 'trans_kasir';
    protected $primaryKey = 'id';
    protected $keyType = 'string';
    public $timestamps = false;
    protected $fillable = [
        'id','tanggal','time','pembeli','total_item','total_asal','total_nilai','diskon','total_bayar','total_terbayar','total_hutang','keterangan','user_id','kasir_lock_id'
    ];
    const order = ['trans_kasir.tanggal' => 'DESC'];
    const columns = ['trans_kasir.pembeli','trans_kasir.total_item','trans_kasir.total_nilai','trans_kasir.diskon','trans_kasir.total_bayar','trans_kasir.total_hutang'];

    public static function getAllKasir($input,$type='row'){
        $dt_produk = DB::table('trans_kasir')
        	->where('time','>=',strtotime($input['tgl_pembelian_a'].' 00:00:00'))
        	->where('time','<=',strtotime($input['tgl_pembelian_b'].' 23:59:59'))
            ->select('trans_kasir.*', DB::raw('@rownum:= @rownum +1 As rownum'));

        if ($type!='total') {
            $i = 0;
            $search_value = $input['search'];
            if(!empty($search_value['value'])){
                foreach (self::columns as $item){
                    ($i==0) ? $dt_produk->where($item,'like', '%'.$search_value['value'].'%') : $dt_produk->orWhere($item,'like', '%'.$search_value['value'].'%');
                    $i++;
                }
            }

            $order_column = $input['order'];
            if($order_column[0]['column'] != 0){
                $dt_produk->orderBy(self::columns[($order_column[0]['column']-1)], $order_column['0']['dir']);
            } 
            else if(isset($input['order'])){
                $order = self::order;
                $dt_produk->orderBy(key($order), $order[key($order)]);
            }
            if ($type!='raw') {
                $length = $input['length'];
                if($length !== false){
                    if($length != -1) {
                        $dt_produk->offset($input['start']);
                        $dt_produk->limit($input['length']);
                    }
                }
            }
        }
        if ($type=='raw' || $type=='total') {
            $dt_produk = $dt_produk->count();
        }else{
            $dt_produk = $dt_produk->get();
        }
        
        return $dt_produk;
    }
    const order_kasir = ['trans_produk.nama' => 'ASC'];
    const columns_kasir = ['trans_kasir_detail.produk_id','trans_produk.nama'];
    public static function getAllLaporanProdukKasir($input,$type='row'){
        $dt_produk_kasir = DB::table('trans_kasir')
        	->where('trans_kasir.time','>=',strtotime($input['tgl_pembelian_a'].' 00:00:00'))
        	->where('trans_kasir.time','<=',strtotime($input['tgl_pembelian_b'].' 23:59:59'))
        	->join('trans_kasir_detail','trans_kasir_detail.kasir_id','=','trans_kasir.id')
        	->join('trans_produk','trans_produk.id','=','trans_kasir_detail.produk_id')
            ->select('trans_produk.nama',DB::raw('sum(trans_kasir_detail.jumlah) as total_terbeli'),DB::raw('sum(trans_kasir_detail.jumlah * trans_kasir_detail.harga_beli) as total_harga_beli'),DB::raw('sum(trans_kasir_detail.jumlah * trans_kasir_detail.harga_terjual) as total_harga_terjual'))
            ->groupBy('trans_kasir_detail.produk_id');

        if ($type!='total') {
            $i = 0;
            $search_value = $input['search'];
            if(!empty($search_value['value'])){
                foreach (self::columns_kasir as $item){
                    ($i==0) ? $dt_produk_kasir->where($item,'like', '%'.$search_value['value'].'%') : $dt_produk_kasir->orWhere($item,'like', '%'.$search_value['value'].'%');
                    $i++;
                }
            }

            $order_column = $input['order'];
            if($order_column[0]['column'] != 0){
                $dt_produk_kasir->orderBy(self::columns_kasir[($order_column[0]['column']-1)], $order_column['0']['dir']);
            } 
            else if(isset($input['order'])){
                $order = self::order_kasir;
                $dt_produk_kasir->orderBy(key($order), $order[key($order)]);
            }
            if ($type!='raw') {
                $length = $input['length'];
                if($length !== false){
                    if($length != -1) {
                        $dt_produk_kasir->offset($input['start']);
                        $dt_produk_kasir->limit($input['length']);
                    }
                }
            }
        }
        if ($type=='raw' || $type=='total') {
            $dt_produk_kasir = $dt_produk_kasir->count();
        }else{
            $dt_produk_kasir = $dt_produk_kasir->get();
        }
        
        return $dt_produk_kasir;
    }
    
}
