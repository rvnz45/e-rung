<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\App_config;
use DB;

class Produk extends Model
{
    protected $table = 'trans_produk';
    protected $primaryKey = 'id';
    protected $keyType = 'string';
    public $timestamps = false;
    protected $fillable = [
        'id','sub_id','nama','foto','stok','harga_beli','harga_jual','is_expire','tgl_kadaluarsa','diskon','is_tracking','is_alert','stok_min','total_terbeli','status','satuan_id','user_id','kategori_id','tipe_id'
    ];
    const order = ['trans_produk.nama' => 'ASC'];
    const columns = ['trans_produk.nama','trans_produk.foto','trans_produk.stok','trans_produk.tgl_kadaluarsa','trans_produk.harga_jual'];

    public function Satuan()
	{
	    return $this->belongsTo('App\Models\Satuan');
	}
	public function Kategori()
	{
	    return $this->belongsTo('App\Models\Kategori');
	}
	public function Tipe()
	{
	    return $this->belongsTo('App\Models\Tipe');
	}
	public function User()
	{
	    return $this->belongsTo('App\User');
	}
	public static function getAllProduk($input,$type='row'){
        if (isset($input['sumber']) && $input['sumber']=='pengeluaran') {
            $config = App_config::where('id','pengaturanPengeluaran')->first();
            $pengaturan = json_decode($config->value);
        }else if (isset($input['sumber']) && $input['sumber']=='kasir') {
            $config = App_config::where('id','pengaturanKasir')->first();
            $pengaturan = json_decode($config->value);
        }
        $dt_produk = DB::table('trans_produk')
            ->join('mst_satuan',  'trans_produk.satuan_id', '=', 'mst_satuan.id')
            ->select('trans_produk.*', 'mst_satuan.nama as mst_satuan_nama', DB::raw('@rownum:= @rownum +1 As rownum'));
        if (!empty($input['produk_id'])) {
            $dt_produk = $dt_produk->where('sub_id',$input['produk_id']);   
        }else{
            $dt_produk = $dt_produk->whereNull('sub_id');
        }
        if (isset($input['sumber']) && ($input['sumber']=='pengeluaran' || $input['sumber']=='kasir')) {
            if (count($pengaturan) > 0) {
                $dt_produk = $dt_produk->whereIn('tipe_id',$pengaturan);
            }
        }
        if ($type!='total') {
            $i = 0;
            $search_value = $input['search'];
            if(!empty($search_value['value'])){
                foreach (self::columns as $item){
                    ($i==0) ? $dt_produk->where($item,'like', '%'.$search_value['value'].'%') : $dt_produk->orWhere($item,'like', '%'.$search_value['value'].'%');
                    $i++;
                }
            }

            $order_column = $input['order'];
            if($order_column[0]['column'] != 0){
                $dt_produk->orderBy(self::columns[($order_column[0]['column']-1)], $order_column['0']['dir']);
            } 
            else if(isset($input['order'])){
                $order = self::order;
                $dt_produk->orderBy(key($order), $order[key($order)]);
            }
            if ($type!='raw') {
                $length = $input['length'];
                if($length !== false){
                    if($length != -1) {
                        $dt_produk->offset($input['start']);
                        $dt_produk->limit($input['length']);
                    }
                }
            }
        }
        if ($type=='raw' || $type=='total') {
            $dt_produk = $dt_produk->count();
        }else{
            $dt_produk = $dt_produk->get();
        }
        
        return $dt_produk;
    }
    public static function hasChildMaxMin($id){
        return self::select(DB::raw('max(harga_jual) harga_max'),DB::raw('min(harga_jual) harga_min'),DB::raw('max(tgl_kadaluarsa) tgl_kadaluarsa_max'),DB::raw('min(tgl_kadaluarsa) tgl_kadaluarsa_min'))->where('sub_id',$id)->first();
    }
    public static function hasChild($id){
        return self::where('sub_id',$id);
    }
    public static function UpdateStok($produk_id,$type='update'){
            $produk = self::select('id')->where('sub_id',$produk_id);
            if ($produk->exists()) {
                $detail = self::select(DB::raw('sum(stok) as total_stok'))->where('sub_id',$produk_id)->first();

                $update= DB::table('trans_produk')
                    ->where('id', $produk_id)
                    ->update([
                        'stok'      => $detail->total_stok > 0 ? $detail->total_stok : 0,
                        'harga_jual'=> 0,
                        'harga_beli'=> 0,
                        'tgl_kadaluarsa'=> NULL,
                    ]);
            }else{
                if ($type=='delete') {
                    $update= DB::table('trans_produk')
                    ->where('id', $produk_id)
                    ->update([
                        'stok'      => 0,
                    ]);
                }
            }
    }
}
