<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class App_menu extends Model
{
    protected $table = 'app_menu';
    protected $primaryKey = 'id';
    public $timestamps = false;

    protected $fillable = [
        'postion','sub_id','sort'
    ];
    public function App_file()
	{
	    return $this->belongsTo('App\Models\App_file');
	}
	public function App_theme()
	{
	    return $this->belongsTo('App\Models\App_theme');
	}
}
