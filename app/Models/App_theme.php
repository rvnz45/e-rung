<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class App_theme extends Model
{
	protected $table = 'app_theme';
    protected $primaryKey = 'id';
    public $timestamps = false;

    protected $fillable = [
        'name','folder'
    ];
    public function App_file()
    {
        return $this->hasMany('App\Models\App_file');
    }
    public function App_menu()
    {
        return $this->hasMany('App\Models\App_menu');
    }
}
