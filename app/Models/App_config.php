<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class App_config extends Model
{
    protected $table = 'app_config';
    protected $primaryKey = 'id';
    public $timestamps = false;
    protected $keyType = 'string';

    protected $fillable = [
        'id','value'
    ];
}
