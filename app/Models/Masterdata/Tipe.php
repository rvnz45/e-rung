<?php

namespace App\Models\Masterdata;

use Illuminate\Database\Eloquent\Model;
use DB;

class Tipe extends Model
{
    protected $table = 'mst_tipe';
    protected $primaryKey = 'id';
    protected $keyType = 'string';
    public $timestamps = false;
    protected $fillable = [
        'id','nama','status'
    ];
    const order = ['mst_tipe.nama' => 'ASC'];
    const columns = ['mst_tipe.nama'];

    public function Produk()
	{
	    return $this->hasMany('App\Models\Produk');
	}
    public static function getAllTipe($input,$type='row'){
        $dt_tipe = DB::table('mst_tipe')
            ->select('mst_tipe.*');
        if ($type!='total') {
            $i = 0;
            $search_value = $input['search'];
            if(!empty($search_value['value'])){
                foreach (self::columns as $item){
                    ($i==0) ? $dt_tipe->where($item,'like', '%'.$search_value['value'].'%') : $dt_tipe->orWhere($item,'like', '%'.$search_value['value'].'%');
                    $i++;
                }
            }

            $order_column = $input['order'];
            if($order_column[0]['column'] != 0){
                $dt_tipe->orderBy(self::columns[($order_column[0]['column']-1)], $order_column['0']['dir']);
            } 
            else if(isset($input['order'])){
                $order = self::order;
                $dt_tipe->orderBy(key($order), $order[key($order)]);
            }
            if ($type!='raw') {
                $length = $input['length'];
                if($length !== false){
                    if($length != -1) {
                        $dt_tipe->offset($input['start']);
                        $dt_tipe->limit($input['length']);
                    }
                }
            }
        }
        if ($type=='raw' || $type=='total') {
            $dt_tipe = $dt_tipe->count();
        }else{
            $dt_tipe = $dt_tipe->get();
        }
        
        return $dt_tipe;
    }
    public static function getID(){
    	$tipe = DB::table('mst_tipe')
    	->selectRaw('max(id) maxid')->first();
    	$num = 1;
    	if ($tipe->maxid!=null) {
    		$num = $tipe->maxid+1;
    	}
    	return sprintf('%02d', $num);
    }
}
