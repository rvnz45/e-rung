<?php

namespace App\Models\Masterdata;

use Illuminate\Database\Eloquent\Model;
use DB;

class Kategori extends Model
{
    protected $table = 'mst_kategori';
    protected $primaryKey = 'id';
    protected $keyType = 'string';
    public $timestamps = false;
    protected $fillable = [
        'id','nama','status'
    ];
    const order = ['mst_kategori.nama' => 'ASC'];
    const columns = ['mst_kategori.nama'];

    public function Produk()
	{
	    return $this->hasMany('App\Models\Produk');
	}
    public static function getAllKategori($input,$type='row'){
        $dt_kategori = DB::table('mst_kategori')
            ->select('mst_kategori.*');
        if ($type!='total') {
            $i = 0;
            $search_value = $input['search'];
            if(!empty($search_value['value'])){
                foreach (self::columns as $item){
                    ($i==0) ? $dt_kategori->where($item,'like', '%'.$search_value['value'].'%') : $dt_kategori->orWhere($item,'like', '%'.$search_value['value'].'%');
                    $i++;
                }
            }

            $order_column = $input['order'];
            if($order_column[0]['column'] != 0){
                $dt_kategori->orderBy(self::columns[($order_column[0]['column']-1)], $order_column['0']['dir']);
            } 
            else if(isset($input['order'])){
                $order = self::order;
                $dt_kategori->orderBy(key($order), $order[key($order)]);
            }
            if ($type!='raw') {
                $length = $input['length'];
                if($length !== false){
                    if($length != -1) {
                        $dt_kategori->offset($input['start']);
                        $dt_kategori->limit($input['length']);
                    }
                }
            }
        }
        if ($type=='raw' || $type=='total') {
            $dt_kategori = $dt_kategori->count();
        }else{
            $dt_kategori = $dt_kategori->get();
        }
        
        return $dt_kategori;
    }
    public static function getID(){
        $tipe = DB::table('mst_kategori')
        ->selectRaw('max(id) maxid')->first();
        $num = 1;
        if ($tipe->maxid!=null) {
            $num = $tipe->maxid+1;
        }
        return sprintf('%02d', $num);
    }
}
