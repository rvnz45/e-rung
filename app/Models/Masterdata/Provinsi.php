<?php

namespace App\Models\Masterdata;

use Illuminate\Database\Eloquent\Model;
Use DB;

class Provinsi extends Model
{
    protected $table = 'mst_provinsi';
    protected $primaryKey = 'id';
    protected $keyType = 'string';
    public $timestamps = false;
    protected $fillable = [
        'id','nama', 'latitude','longitude'
    ];
    const order = ['mst_provinsi.nama' => 'ASC'];
    const columns = ['mst_provinsi.nama','mst_provinsi.latitude','mst_provinsi.longitude'];
    public function Kota()
    {
        return $this->hasMany('App\Models\Masterdata\Kota');
    }
    public static function getAllProvinsi($input,$type='row'){
        $dt_provinsi = DB::table('mst_provinsi')
            ->select('mst_provinsi.*');
        if ($type!='total') {
            $i = 0;
            $search_value = $input['search'];
            if(!empty($search_value['value'])){
                foreach (self::columns as $item){
                    ($i==0) ? $dt_provinsi->where($item,'like', '%'.$search_value['value'].'%') : $dt_provinsi->orWhere($item,'like', '%'.$search_value['value'].'%');
                    $i++;
                }
            }

            $order_column = $input['order'];
            if($order_column[0]['column'] != 0){
                $dt_provinsi->orderBy(self::columns[($order_column[0]['column']-1)], $order_column['0']['dir']);
            } 
            else if(isset($input['order'])){
                $order = self::order;
                $dt_provinsi->orderBy(key($order), $order[key($order)]);
            }
            if ($type!='raw') {
                $length = $input['length'];
                if($length !== false){
                    if($length != -1) {
                        $dt_provinsi->offset($input['start']);
                        $dt_provinsi->limit($input['length']);
                    }
                }
            }
        }
        if ($type=='raw' || $type=='total') {
            $dt_provinsi = $dt_provinsi->count();
        }else{
            $dt_provinsi = $dt_provinsi->get();
        }
        
        return $dt_provinsi;
    }
}
