<?php

namespace App\Models\Masterdata;

use Illuminate\Database\Eloquent\Model;
use DB;

class Satuan extends Model
{
    protected $table = 'mst_satuan';
    protected $primaryKey = 'id';
    protected $keyType = 'string';
    public $timestamps = false;
    protected $fillable = [
        'nama', 'jenis'
    ];
    const order = ['mst_satuan.nama' => 'ASC'];
    const columns = ['mst_satuan.nama','mst_satuan.jenis'];

    public function Produk()
	{
	    return $this->hasMany('App\Models\Produk');
	}
    public static function getAllSatuan($input,$type='row'){
        $dt_satuan = DB::table('mst_satuan')
            ->select('mst_satuan.*');
        if ($type!='total') {
            $i = 0;
            $search_value = $input['search'];
            if(!empty($search_value['value'])){
                foreach (self::columns as $item){
                    ($i==0) ? $dt_satuan->where($item,'like', '%'.$search_value['value'].'%') : $dt_satuan->orWhere($item,'like', '%'.$search_value['value'].'%');
                    $i++;
                }
            }

            $order_column = $input['order'];
            if($order_column[0]['column'] != 0){
                $dt_satuan->orderBy(self::columns[($order_column[0]['column']-1)], $order_column['0']['dir']);
            } 
            else if(isset($input['order'])){
                $order = self::order;
                $dt_satuan->orderBy(key($order), $order[key($order)]);
            }
            if ($type!='raw') {
                $length = $input['length'];
                if($length !== false){
                    if($length != -1) {
                        $dt_satuan->offset($input['start']);
                        $dt_satuan->limit($input['length']);
                    }
                }
            }
        }
        if ($type=='raw' || $type=='total') {
            $dt_satuan = $dt_satuan->count();
        }else{
            $dt_satuan = $dt_satuan->get();
        }
        
        return $dt_satuan;
    }
}
