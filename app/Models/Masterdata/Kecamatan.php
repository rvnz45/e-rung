<?php

namespace App\Models\Masterdata;

use Illuminate\Database\Eloquent\Model;
use DB;

class Kecamatan extends Model
{
    protected $table = 'mst_kecamatan';
    protected $primaryKey = 'id';
    protected $keyType = 'string';
    public $timestamps = false;
    protected $fillable = [
        'id','nama', 'latitude','longitude','kota_id'
    ];
    const order = ['mst_kecamatan.nama' => 'ASC'];
    const columns = ['mst_kota.nama','mst_kecamatan.nama','mst_kecamatan.latitude','mst_kecamatan.longitude'];
    public function Kota()
	{
	    return $this->belongsTo('App\Models\Masterdata\Kota');
	}
	public function Desa()
    {
        return $this->hasMany('App\Models\Masterdata\Desa');
    }
    public static function getAllKecamatan($input,$type='row'){
        $dt_kecamatan = DB::table('mst_kecamatan')
            ->join('mst_kota',  'mst_kota.id', '=', 'mst_kecamatan.kota_id')
            ->select('mst_kecamatan.*', 'mst_kota.nama as mst_kota_nama');
        if ($type!='total') {
            $i = 0;
            $search_value = $input['search'];
            if(!empty($search_value['value'])){
                foreach (self::columns as $item){
                    ($i==0) ? $dt_kecamatan->where($item,'like', '%'.$search_value['value'].'%') : $dt_kecamatan->orWhere($item,'like', '%'.$search_value['value'].'%');
                    $i++;
                }
            }

            $order_column = $input['order'];
            if($order_column[0]['column'] != 0){
                $dt_kecamatan->orderBy(self::columns[($order_column[0]['column']-1)], $order_column['0']['dir']);
            } 
            else if(isset($input['order'])){
                $order = self::order;
                $dt_kecamatan->orderBy(key($order), $order[key($order)]);
            }
            if ($type!='raw') {
                $length = $input['length'];
                if($length !== false){
                    if($length != -1) {
                        $dt_kecamatan->offset($input['start']);
                        $dt_kecamatan->limit($input['length']);
                    }
                }
            }
        }
        if ($type=='raw' || $type=='total') {
            $dt_kecamatan = $dt_kecamatan->count();
        }else{
            $dt_kecamatan = $dt_kecamatan->get();
        }
        
        return $dt_kecamatan;
    }
}
