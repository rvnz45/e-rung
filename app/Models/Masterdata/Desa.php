<?php

namespace App\Models\Masterdata;

use Illuminate\Database\Eloquent\Model;
Use DB;

class Desa extends Model
{
    protected $table = 'mst_desa';
    protected $primaryKey = 'id';
    protected $keyType = 'string';
    public $timestamps = false;
    protected $fillable = [
        'id','nama', 'latitude','longitude','kecamatan_id'
    ];
    const order = ['mst_desa.nama' => 'ASC'];
    const columns = ['mst_kecamatan.nama','mst_desa.nama','mst_desa.latitude','mst_desa.longitude'];
     
    public function Kecamatan()
	{
	    return $this->belongsTo('App\Models\Masterdata\Kecamatan');
	}
    
    public static function getAllDesa($input,$type='row'){
        $dt_desa = DB::table('mst_desa')
            ->join('mst_kecamatan',  'mst_kecamatan.id', '=', 'mst_desa.kecamatan_id')
            ->select('mst_desa.*', 'mst_kecamatan.nama as mst_kecamatan_nama');
        if ($type!='total') {
            $i = 0;
            $search_value = $input['search'];
            if(!empty($search_value['value'])){
                foreach (self::columns as $item){
                    ($i==0) ? $dt_desa->where($item,'like', '%'.$search_value['value'].'%') : $dt_desa->orWhere($item,'like', '%'.$search_value['value'].'%');
                    $i++;
                }
            }

            $order_column = $input['order'];
            if($order_column[0]['column'] != 0){
                $dt_desa->orderBy(self::columns[($order_column[0]['column']-1)], $order_column['0']['dir']);
            } 
            else if(isset($input['order'])){
                $order = self::order;
                $dt_desa->orderBy(key($order), $order[key($order)]);
            }
            if ($type!='raw') {
                $length = $input['length'];
                if($length !== false){
                    if($length != -1) {
                        $dt_desa->offset($input['start']);
                        $dt_desa->limit($input['length']);
                    }
                }
            }
        }
        if ($type=='raw' || $type=='total') {
            $dt_desa = $dt_desa->count();
        }else{
            $dt_desa = $dt_desa->get();
        }
        
        return $dt_desa;
    }
}
