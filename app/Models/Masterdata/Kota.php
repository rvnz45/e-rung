<?php

namespace App\Models\Masterdata;

use Illuminate\Database\Eloquent\Model;
Use DB;

class Kota extends Model
{
    protected $table = 'mst_kota';
    protected $primaryKey = 'id';
    protected $keyType = 'string';
    public $timestamps = false;

    protected $fillable = [
        'id','nama', 'latitude','longitude','provinsi_id'
    ];
    const order = ['mst_kota.nama' => 'ASC'];
    const columns = ['mst_provinsi.nama','mst_kota.nama','mst_kota.latitude','mst_kota.longitude'];

    public function Provinsi()
	{
	    return $this->belongsTo('App\Models\Masterdata\Provinsi');
	}
	public function Kecamatan()
    {
        return $this->hasMany('App\Models\Masterdata\Kecamatan');
    }
    public static function getAllKota($input,$type='row'){
        $dt_kota = DB::table('mst_kota')
            ->join('mst_provinsi',  'mst_provinsi.id', '=', 'mst_kota.provinsi_id')
            ->select('mst_kota.*', 'mst_provinsi.nama as mst_provinsi_nama');
        if ($type!='total') {
            $i = 0;
            $search_value = $input['search'];
            if(!empty($search_value['value'])){
                foreach (self::columns as $item){
                    ($i==0) ? $dt_kota->where($item,'like', '%'.$search_value['value'].'%') : $dt_kota->orWhere($item,'like', '%'.$search_value['value'].'%');
                    $i++;
                }
            }

            $order_column = $input['order'];
            if($order_column[0]['column'] != 0){
                $dt_kota->orderBy(self::columns[($order_column[0]['column']-1)], $order_column['0']['dir']);
            } 
            else if(isset($input['order'])){
                $order = self::order;
                $dt_kota->orderBy(key($order), $order[key($order)]);
            }
            if ($type!='raw') {
                $length = $input['length'];
                if($length !== false){
                    if($length != -1) {
                        $dt_kota->offset($input['start']);
                        $dt_kota->limit($input['length']);
                    }
                }
            }
        }
        if ($type=='raw' || $type=='total') {
            $dt_kota = $dt_kota->count();
        }else{
            $dt_kota = $dt_kota->get();
        }
        
        return $dt_kota;
    }
}
