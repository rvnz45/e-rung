<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class App_user_level extends Model
{
    protected $table = 'app_user_level';
    protected $primaryKey = 'level';
    protected $keyType = 'string';
    public $timestamps = false;
    protected $fillable = [
        'level', 'value'
    ];
    const order = ['level' => 'ASC'];
    const columns = ['level','value'];
    public function App_user_access()
    {
        return $this->hasMany('App\Models\App_user_access');
    }
    public static function getAllLevel($input,$type='row'){
        $dt_desa = DB::table('app_user_level')
            ->select('app_user_level.*');
        if ($type!='total') {
            $i = 0;
            $search_value = $input['search'];
            if(!empty($search_value['value'])){
                foreach (self::columns as $item){
                    ($i==0) ? $dt_desa->where($item,'like', '%'.$search_value['value'].'%') : $dt_desa->orWhere($item,'like', '%'.$search_value['value'].'%');
                    $i++;
                }
            }

            $order_column = $input['order'];
            if($order_column[0]['column'] != 0){
                $dt_desa->orderBy(self::columns[($order_column[0]['column']-1)], $order_column['0']['dir']);
            } 
            else if(isset($input['order'])){
                $order = self::order;
                $dt_desa->orderBy(key($order), $order[key($order)]);
            }
            if ($type!='raw') {
                $length = $input['length'];
                if($length !== false){
                    if($length != -1) {
                        $dt_desa->offset($input['start']);
                        $dt_desa->limit($input['length']);
                    }
                }
            }
        }
        if ($type=='raw' || $type=='total') {
            $dt_desa = $dt_desa->count();
        }else{
            $dt_desa = $dt_desa->get();
        }
        
        return $dt_desa;
    }
}
