<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class KasirLock extends Model
{
    protected $table = 'trans_kasir_lock';
    protected $primaryKey = 'id';
    protected $keyType = 'string';
    public $timestamps = false;
    protected $fillable = [
        'id','tanggal','time','nilai_awal','nilai_akhir','nilai_fisik','status','user_id','keterangan'
    ];
}
