<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class App_file extends Model
{
    protected $table = 'app_file';
    protected $primaryKey = 'id';
    public $timestamps = false;

    protected $fillable = [
        'filename','module'
    ];
    public function App_lang()
	{
	    return $this->belongsTo('App\Models\App_lang');
	}
	public function App_theme()
    {
        return $this->belongsTo('App\Models\App_theme');
    }
    public function App_menu()
    {
        return $this->hasMany('App\Models\App_menu');
    }
    public function App_user_access()
    {
        return $this->hasMany('App\Models\App_user_access');
    }
}
