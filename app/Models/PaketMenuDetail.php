<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class PaketMenuDetail extends Model
{
    protected $table = 'trans_paket_menu_detail';
    protected $primaryKey = 'id';
    protected $keyType = 'string';
    public $timestamps = false;
    protected $fillable = [
        'id','paket_menu_id','produk_id','jumlah','harga','keterangan'
    ];
    const order = ['trans_produk.nama' => 'ASC'];
    const columns = ['trans_produk.nama','trans_paket_menu_detail.produk_id','trans_paket_menu_detail.jumlah','trans_paket_menu_detail.harga','trans_paket_menu_detail.keterangan'];
    public static function getPaket($id=''){
        $dt_paketMenuDetail = DB::table('trans_paket_menu_detail')
            ->join('trans_produk','trans_produk.id','=','trans_paket_menu_detail.produk_id')
            ->select('trans_paket_menu_detail.*','trans_produk.nama as trans_produk_nama')
            ->where('trans_paket_menu_detail.paket_menu_id',$id);
        return $dt_paketMenuDetail;
    }
    public static function getAllPaketMenuDetail($input,$type='row'){
        $dt_paketMenuDetail = DB::table('trans_paket_menu_detail')
        	->join('trans_produk','trans_produk.id','=','trans_paket_menu_detail.produk_id')
        	->join('mst_satuan','mst_satuan.id','=','trans_produk.satuan_id')
            ->select('trans_paket_menu_detail.*','trans_produk.nama as trans_produk_nama' ,'mst_satuan.nama as mst_satuan_nama',DB::raw('@rownum:= @rownum +1 As rownum'))
            ->where('trans_paket_menu_detail.paket_menu_id',$input['paket_menu_id']);
        if ($type!='total') {
            $i = 0;
            $search_value = $input['search'];
            if(!empty($search_value['value'])){
                foreach (self::columns as $item){
                    ($i==0) ? $dt_paketMenuDetail->where($item,'like', '%'.$search_value['value'].'%') : $dt_paketMenuDetail->orWhere($item,'like', '%'.$search_value['value'].'%');
                    $i++;
                }
            }

            $order_column = $input['order'];
            if($order_column[0]['column'] != 0){
                $dt_paketMenuDetail->orderBy(self::columns[($order_column[0]['column']-1)], $order_column['0']['dir']);
            } 
            else if(isset($input['order'])){
                $order = self::order;
                $dt_paketMenuDetail->orderBy(key($order), $order[key($order)]);
            }
            if ($type!='raw') {
                $length = $input['length'];
                if($length !== false){
                    if($length != -1) {
                        $dt_paketMenuDetail->offset($input['start']);
                        $dt_paketMenuDetail->limit($input['length']);
                    }
                }
            }
        }
        if ($type=='raw' || $type=='total') {
            $dt_paketMenuDetail = $dt_paketMenuDetail->count();
        }else{
            $dt_paketMenuDetail = $dt_paketMenuDetail->get();
        }
        return $dt_paketMenuDetail;
    }
    public static function getID($paket_menu_id){
    	$paketMenu = DB::table('trans_paket_menu_detail')
    		->selectRaw('max(id) as id_detail')
    		->where('paket_menu_id',$paket_menu_id)->first();
    	if($paketMenu->id_detail > 0){
    		return sprintf('%05d', ($paketMenu->id_detail+1));
    	}
    	return sprintf('%05d', 1);
    }
}
