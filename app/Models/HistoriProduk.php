<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HistoriProduk extends Model
{
    protected $table = 'histori_produk';
    protected $primaryKey = 'id';
    protected $fillable = [
        'link','jenis','produk_id','stok_awal','stok_akhir','stok_selisih','harga','keterangan'
    ];
}
