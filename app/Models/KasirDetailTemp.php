<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class KasirDetailTemp extends Model
{
    protected $table = 'trans_kasir_detail_temp';
    protected $primaryKey = 'id';
    protected $keyType = 'string';
    public $timestamps = false;
    protected $fillable = [
        'id','kasir_lock_id','produk_id','jumlah','harga_beli','harga_jual','diskon','harga_terjual','keterangan'
    ];
    const order = ['trans_produk.nama' => 'ASC'];
    const columns = ['trans_produk.nama','mst_satuan.nama','trans_kasir_detail_temp.jumlah','trans_kasir_detail_temp.harga_terjual','trans_kasir_detail_temp.keterangan'];


    public static function getID($kasir_lock_id){
    	$pengeluaran = DB::table('trans_kasir_detail_temp')
    		->selectRaw('max(id) as id_detail')
    		->where('kasir_lock_id',$kasir_lock_id)->first();
    	if($pengeluaran->id_detail > 0){
    		return sprintf('%05d', ($pengeluaran->id_detail+1));
    	}
    	return sprintf('%05d', 1);
    }
    public static function getAllkeranjang($input,$type='row'){
        $dt_keranjang = DB::table('trans_kasir_detail_temp')
        	->join('trans_produk','trans_produk.id','=','trans_kasir_detail_temp.produk_id')
        	->join('mst_satuan','mst_satuan.id','=','trans_produk.satuan_id')
            ->select('trans_kasir_detail_temp.*','trans_produk.nama as trans_produk_nama' ,'mst_satuan.nama as mst_satuan_nama',DB::raw('@rownum:= @rownum +1 As rownum'));
        if ($type!='total') {
            $i = 0;
            $search_value = $input['search'];
            if(!empty($search_value['value'])){
                foreach (self::columns as $item){
                    ($i==0) ? $dt_keranjang->where($item,'like', '%'.$search_value['value'].'%') : $dt_keranjang->orWhere($item,'like', '%'.$search_value['value'].'%');
                    $i++;
                }
            }

            $order_column = $input['order'];
            if($order_column[0]['column'] != 0){
                $dt_keranjang->orderBy(self::columns[($order_column[0]['column']-1)], $order_column['0']['dir']);
            } 
            else if(isset($input['order'])){
                $order = self::order;
                $dt_keranjang->orderBy(key($order), $order[key($order)]);
            }
            if ($type!='raw') {
                $length = $input['length'];
                if($length !== false){
                    if($length != -1) {
                        $dt_keranjang->offset($input['start']);
                        $dt_keranjang->limit($input['length']);
                    }
                }
            }
        }
        if ($type=='raw' || $type=='total') {
            $dt_keranjang = $dt_keranjang->count();
        }else{
            $dt_keranjang = $dt_keranjang->get();
        }
        return $dt_keranjang;
    }
}
