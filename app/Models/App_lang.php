<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class App_lang extends Model
{
    protected $table = 'app_lang';
    protected $primaryKey = 'id';
    protected $keyType = 'string';
    public $timestamps = false;

    protected $fillable = [
        'lang'
    ];
    public function App_file()
    {
        return $this->hasMany('App\Models\App_file');
    }
}
