<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class App_user_acces extends Model
{
    protected $table = 'app_user_acces';
    protected $primaryKey = 'id';
    public $timestamps = false;

    protected $fillable = [
        'doshow','doadd','doedit','dodel'
    ];
    public function App_file()
	{
	    return $this->belongsTo('App\Models\App_file');
	}
	public function App_user_level()
	{
	    return $this->belongsTo('App\Models\App_user_level');
	}
}
