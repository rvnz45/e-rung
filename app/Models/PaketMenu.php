<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class PaketMenu extends Model
{
    protected $table = 'trans_paket_menu';
    protected $primaryKey = 'id';
    protected $keyType = 'string';
    public $timestamps = false;
    protected $fillable = [
        'id','tanggal','keterangan','total_nilai','total_item','user_id','status'
    ];
    const order = ['trans_paket_menu.tanggal' => 'DESC'];
    const columns = ['trans_paket_menu.tanggal','trans_paket_menu.total_nilai','trans_paket_menu.keterangan','trans_paket_menu.user_id'];

    public static function getAllPaketMenu($input,$type='row'){
        $dt_paketMenu = DB::table('trans_paket_menu')
            ->select('trans_paket_menu.*', DB::raw('@rownum:= @rownum +1 As rownum'));
        if ($type!='total') {
            $i = 0;
            $search_value = $input['search'];
            if(!empty($search_value['value'])){
                foreach (self::columns as $item){
                    ($i==0) ? $dt_paketMenu->where($item,'like', '%'.$search_value['value'].'%') : $dt_paketMenu->orWhere($item,'like', '%'.$search_value['value'].'%');
                    $i++;
                }
            }

            $order_column = $input['order'];
            if($order_column[0]['column'] != 0){
                $dt_paketMenu->orderBy(self::columns[($order_column[0]['column']-1)], $order_column['0']['dir']);
            } 
            else if(isset($input['order'])){
                $order = self::order;
                $dt_paketMenu->orderBy(key($order), $order[key($order)]);
            }
            if ($type!='raw') {
                $length = $input['length'];
                if($length !== false){
                    if($length != -1) {
                        $dt_paketMenu->offset($input['start']);
                        $dt_paketMenu->limit($input['length']);
                    }
                }
            }
        }
        if ($type=='raw' || $type=='total') {
            $dt_paketMenu = $dt_paketMenu->count();
        }else{
            $dt_paketMenu = $dt_paketMenu->get();
        }
        return $dt_paketMenu;
    }
}
