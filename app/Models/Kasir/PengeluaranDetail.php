<?php

namespace App\Models\Kasir;

use Illuminate\Database\Eloquent\Model;
use DB;

class PengeluaranDetail extends Model
{
    protected $table = 'trans_pengeluaran_detail';
    protected $primaryKey = 'id';
    protected $keyType = 'string';
    public $timestamps = false;
    protected $fillable = [
        'id','pengeluaran_id','produk_id','jumlah','harga','keterangan'
    ];
    const order = ['trans_produk.nama' => 'ASC'];
    const columns = ['trans_produk.nama','trans_pengeluaran_detail.produk_id','trans_pengeluaran_detail.jumlah','trans_pengeluaran_detail.harga','trans_pengeluaran_detail.keterangan'];

    public static function getAllPengeluaranDetail($input,$type='row'){
        $dt_pengeluaranDetail = DB::table('trans_pengeluaran_detail')
        	->join('trans_produk','trans_produk.id','=','trans_pengeluaran_detail.produk_id')
        	->join('mst_satuan','mst_satuan.id','=','trans_produk.satuan_id')
            ->select('trans_pengeluaran_detail.*','trans_produk.nama as trans_produk_nama' ,'mst_satuan.nama as mst_satuan_nama',DB::raw('@rownum:= @rownum +1 As rownum'))
            ->where('trans_pengeluaran_detail.pengeluaran_id',$input['pengeluaran_id']);
        if ($type!='total') {
            $i = 0;
            $search_value = $input['search'];
            if(!empty($search_value['value'])){
                foreach (self::columns as $item){
                    ($i==0) ? $dt_pengeluaranDetail->where($item,'like', '%'.$search_value['value'].'%') : $dt_pengeluaranDetail->orWhere($item,'like', '%'.$search_value['value'].'%');
                    $i++;
                }
            }

            $order_column = $input['order'];
            if($order_column[0]['column'] != 0){
                $dt_pengeluaranDetail->orderBy(self::columns[($order_column[0]['column']-1)], $order_column['0']['dir']);
            } 
            else if(isset($input['order'])){
                $order = self::order;
                $dt_pengeluaranDetail->orderBy(key($order), $order[key($order)]);
            }
            if ($type!='raw') {
                $length = $input['length'];
                if($length !== false){
                    if($length != -1) {
                        $dt_pengeluaranDetail->offset($input['start']);
                        $dt_pengeluaranDetail->limit($input['length']);
                    }
                }
            }
        }
        if ($type=='raw' || $type=='total') {
            $dt_pengeluaranDetail = $dt_pengeluaranDetail->count();
        }else{
            $dt_pengeluaranDetail = $dt_pengeluaranDetail->get();
        }
        return $dt_pengeluaranDetail;
    }
    public static function getID($pengeluaran_id){
    	$pengeluaran = DB::table('trans_pengeluaran_detail')
    		->selectRaw('max(id) as id_detail')
    		->where('pengeluaran_id',$pengeluaran_id)->first();
    	if($pengeluaran->id_detail > 0){
    		return sprintf('%05d', ($pengeluaran->id_detail+1));
    	}
    	return sprintf('%05d', 1);
    }
}
