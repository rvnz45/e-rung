<?php

namespace App\Models\Kasir;

use Illuminate\Database\Eloquent\Model;
use DB;

class PembelianDetail extends Model
{
    protected $table = 'trans_pembelian_detail';
    protected $primaryKey = 'id';
    protected $keyType = 'string';
    public $timestamps = false;
    protected $fillable = [
        'id','pembelian_id','produk_id','jumlah','harga','keterangan'
    ];
    const order = ['trans_produk.nama' => 'ASC'];
    const columns = ['trans_produk.nama','trans_pembelian_detail.produk_id','trans_pembelian_detail.jumlah','trans_pembelian_detail.harga','trans_pembelian_detail.keterangan'];

    public static function getAllPembelianDetail($input,$type='row'){
        $dt_pembelianDetail = DB::table('trans_pembelian_detail')
        	->join('trans_produk','trans_produk.id','=','trans_pembelian_detail.produk_id')
        	->join('mst_satuan','mst_satuan.id','=','trans_produk.satuan_id')
            ->select('trans_pembelian_detail.*','trans_produk.nama as trans_produk_nama' ,'mst_satuan.nama as mst_satuan_nama',DB::raw('@rownum:= @rownum +1 As rownum'))
            ->where('trans_pembelian_detail.pembelian_id',$input['pembelian_id']);
        if ($type!='total') {
            $i = 0;
            $search_value = $input['search'];
            if(!empty($search_value['value'])){
                foreach (self::columns as $item){
                    ($i==0) ? $dt_pembelianDetail->where($item,'like', '%'.$search_value['value'].'%') : $dt_pembelianDetail->orWhere($item,'like', '%'.$search_value['value'].'%');
                    $i++;
                }
            }

            $order_column = $input['order'];
            if($order_column[0]['column'] != 0){
                $dt_pembelianDetail->orderBy(self::columns[($order_column[0]['column']-1)], $order_column['0']['dir']);
            } 
            else if(isset($input['order'])){
                $order = self::order;
                $dt_pembelianDetail->orderBy(key($order), $order[key($order)]);
            }
            if ($type!='raw') {
                $length = $input['length'];
                if($length !== false){
                    if($length != -1) {
                        $dt_pembelianDetail->offset($input['start']);
                        $dt_pembelianDetail->limit($input['length']);
                    }
                }
            }
        }
        if ($type=='raw' || $type=='total') {
            $dt_pembelianDetail = $dt_pembelianDetail->count();
        }else{
            $dt_pembelianDetail = $dt_pembelianDetail->get();
        }
        return $dt_pembelianDetail;
    }
    public static function getID($pembelian_id){
    	$pembelian = DB::table('trans_pembelian_detail')
    		->selectRaw('max(id) as id_detail')
    		->where('pembelian_id',$pembelian_id)->first();
    	if($pembelian->id_detail > 0){
    		return sprintf('%05d', ($pembelian->id_detail+1));
    	}
    	return sprintf('%05d', 1);
    }
}
