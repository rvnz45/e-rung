<?php

namespace App\Models\Kasir;

use Illuminate\Database\Eloquent\Model;
use DB;

class Pembelian extends Model
{
    protected $table = 'trans_pembelian';
    protected $primaryKey = 'id';
    protected $keyType = 'string';
    public $timestamps = false;
    protected $fillable = [
        'id','tanggal','keterangan','total_nilai','total_item','user_id','status'
    ];
    const order = ['trans_pembelian.tanggal' => 'DESC'];
    const columns = ['trans_pembelian.tanggal','trans_pembelian.total_nilai','trans_pembelian.keterangan','trans_pembelian.user_id'];

    public static function getAllPembelian($input,$type='row'){
        $dt_pembelian = DB::table('trans_pembelian')
            ->select('trans_pembelian.*', DB::raw('@rownum:= @rownum +1 As rownum'));
        if ($type!='total') {
            $i = 0;
            $search_value = $input['search'];
            if(!empty($search_value['value'])){
                foreach (self::columns as $item){
                    ($i==0) ? $dt_pembelian->where($item,'like', '%'.$search_value['value'].'%') : $dt_pembelian->orWhere($item,'like', '%'.$search_value['value'].'%');
                    $i++;
                }
            }

            $order_column = $input['order'];
            if($order_column[0]['column'] != 0){
                $dt_pembelian->orderBy(self::columns[($order_column[0]['column']-1)], $order_column['0']['dir']);
            } 
            else if(isset($input['order'])){
                $order = self::order;
                $dt_pembelian->orderBy(key($order), $order[key($order)]);
            }
            if ($type!='raw') {
                $length = $input['length'];
                if($length !== false){
                    if($length != -1) {
                        $dt_pembelian->offset($input['start']);
                        $dt_pembelian->limit($input['length']);
                    }
                }
            }
        }
        if ($type=='raw' || $type=='total') {
            $dt_pembelian = $dt_pembelian->count();
        }else{
            $dt_pembelian = $dt_pembelian->get();
        }
        return $dt_pembelian;
    }

}
