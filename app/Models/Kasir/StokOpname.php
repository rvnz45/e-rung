<?php

namespace App\Models\Kasir;

use Illuminate\Database\Eloquent\Model;
use DB;

class StokOpname extends Model
{
    protected $table = 'trans_stok_opname';
    protected $primaryKey = 'id';
    protected $keyType = 'string';
    public $timestamps = false;
    protected $fillable = [
        'id','tanggal','keterangan','total_nilai','total_item','user_id','status'
    ];
    const order = ['trans_stok_opname.tanggal' => 'DESC'];
    const columns = ['trans_stok_opname.tanggal','trans_stok_opname.total_nilai','trans_stok_opname.keterangan','trans_stok_opname.user_id'];

    public static function getAllStokOpname($input,$type='row'){
        $dt_stokOpname = DB::table('trans_stok_opname')
            ->select('trans_stok_opname.*', DB::raw('@rownum:= @rownum +1 As rownum'));
        if ($type!='total') {
            $i = 0;
            $search_value = $input['search'];
            if(!empty($search_value['value'])){
                foreach (self::columns as $item){
                    ($i==0) ? $dt_stokOpname->where($item,'like', '%'.$search_value['value'].'%') : $dt_stokOpname->orWhere($item,'like', '%'.$search_value['value'].'%');
                    $i++;
                }
            }

            $order_column = $input['order'];
            if($order_column[0]['column'] != 0){
                $dt_stokOpname->orderBy(self::columns[($order_column[0]['column']-1)], $order_column['0']['dir']);
            } 
            else if(isset($input['order'])){
                $order = self::order;
                $dt_stokOpname->orderBy(key($order), $order[key($order)]);
            }
            if ($type!='raw') {
                $length = $input['length'];
                if($length !== false){
                    if($length != -1) {
                        $dt_stokOpname->offset($input['start']);
                        $dt_stokOpname->limit($input['length']);
                    }
                }
            }
        }
        if ($type=='raw' || $type=='total') {
            $dt_stokOpname = $dt_stokOpname->count();
        }else{
            $dt_stokOpname = $dt_stokOpname->get();
        }
        return $dt_stokOpname;
    }
}
