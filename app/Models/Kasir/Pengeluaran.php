<?php

namespace App\Models\Kasir;

use Illuminate\Database\Eloquent\Model;
use DB;

class Pengeluaran extends Model
{
    protected $table = 'trans_pengeluaran';
    protected $primaryKey = 'id';
    protected $keyType = 'string';
    public $timestamps = false;
    protected $fillable = [
        'id','tanggal','keterangan','total_nilai','total_item','user_id','status'
    ];
    const order = ['trans_pengeluaran.tanggal' => 'DESC'];
    const columns = ['trans_pengeluaran.tanggal','trans_pengeluaran.total_nilai','trans_pengeluaran.keterangan','trans_pengeluaran.user_id'];

    public static function getAllPengeluaran($input,$type='row'){
        $dt_pengeluaran = DB::table('trans_pengeluaran')
            ->select('trans_pengeluaran.*', DB::raw('@rownum:= @rownum +1 As rownum'));
        if ($type!='total') {
            $i = 0;
            $search_value = $input['search'];
            if(!empty($search_value['value'])){
                foreach (self::columns as $item){
                    ($i==0) ? $dt_pengeluaran->where($item,'like', '%'.$search_value['value'].'%') : $dt_pengeluaran->orWhere($item,'like', '%'.$search_value['value'].'%');
                    $i++;
                }
            }

            $order_column = $input['order'];
            if($order_column[0]['column'] != 0){
                $dt_pengeluaran->orderBy(self::columns[($order_column[0]['column']-1)], $order_column['0']['dir']);
            } 
            else if(isset($input['order'])){
                $order = self::order;
                $dt_pengeluaran->orderBy(key($order), $order[key($order)]);
            }
            if ($type!='raw') {
                $length = $input['length'];
                if($length !== false){
                    if($length != -1) {
                        $dt_pengeluaran->offset($input['start']);
                        $dt_pengeluaran->limit($input['length']);
                    }
                }
            }
        }
        if ($type=='raw' || $type=='total') {
            $dt_pengeluaran = $dt_pengeluaran->count();
        }else{
            $dt_pengeluaran = $dt_pengeluaran->get();
        }
        return $dt_pengeluaran;
    }
}
