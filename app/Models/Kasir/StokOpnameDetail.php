<?php

namespace App\Models\Kasir;

use Illuminate\Database\Eloquent\Model;
use DB;

class StokOpnameDetail extends Model
{
    protected $table = 'trans_stok_opname_detail';
    protected $primaryKey = 'id';
    protected $keyType = 'string';
    public $timestamps = false;
    protected $fillable = [
        'id','stok_opname_id','produk_id','jumlah','jumlah_fisik','selisih','harga','keterangan'
    ];
    const order = ['trans_produk.nama' => 'ASC'];
    const columns = ['trans_produk.nama','trans_stok_opname_detail.produk_id','trans_stok_opname_detail.jumlah','trans_stok_opname_detail.jumlah_fisik','trans_stok_opname_detail.harga','trans_stok_opname_detail.keterangan'];

    public static function getAllStokOpnameDetail($input,$type='row'){
        $dt_stokOpnameDetail = DB::table('trans_stok_opname_detail')
        	->join('trans_produk','trans_produk.id','=','trans_stok_opname_detail.produk_id')
        	->join('mst_satuan','mst_satuan.id','=','trans_produk.satuan_id')
            ->select('trans_stok_opname_detail.*','trans_produk.nama as trans_produk_nama' ,'mst_satuan.nama as mst_satuan_nama',DB::raw('@rownum:= @rownum +1 As rownum'))
            ->where('trans_stok_opname_detail.stok_opname_id',$input['stok_opname_id']);
        if ($type!='total') {
            $i = 0;
            $search_value = $input['search'];
            if(!empty($search_value['value'])){
                foreach (self::columns as $item){
                    ($i==0) ? $dt_stokOpnameDetail->where($item,'like', '%'.$search_value['value'].'%') : $dt_stokOpnameDetail->orWhere($item,'like', '%'.$search_value['value'].'%');
                    $i++;
                }
            }

            $order_column = $input['order'];
            if($order_column[0]['column'] != 0){
                $dt_stokOpnameDetail->orderBy(self::columns[($order_column[0]['column']-1)], $order_column['0']['dir']);
            } 
            else if(isset($input['order'])){
                $order = self::order;
                $dt_stokOpnameDetail->orderBy(key($order), $order[key($order)]);
            }
            if ($type!='raw') {
                $length = $input['length'];
                if($length !== false){
                    if($length != -1) {
                        $dt_stokOpnameDetail->offset($input['start']);
                        $dt_stokOpnameDetail->limit($input['length']);
                    }
                }
            }
        }
        if ($type=='raw' || $type=='total') {
            $dt_stokOpnameDetail = $dt_stokOpnameDetail->count();
        }else{
            $dt_stokOpnameDetail = $dt_stokOpnameDetail->get();
        }
        return $dt_stokOpnameDetail;
    }
    public static function getID($stok_opname_id){
    	$stokOpname = DB::table('trans_stok_opname_detail')
    		->selectRaw('max(id) as id_detail')
    		->where('stok_opname_id',$stok_opname_id)->first();
    	if($stokOpname->id_detail > 0){
    		return sprintf('%05d', ($stokOpname->id_detail+1));
    	}
    	return sprintf('%05d', 1);
    }
}
