<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class App_user_activity extends Model
{
    protected $table = 'app_user_activity';
    protected $primaryKey = 'id';
    public $timestamps = false;

    protected $fillable = [
        'dtime', 'activity'
    ];
    public function User()
    {
        return $this->hasMany('App\User');
    }
}
