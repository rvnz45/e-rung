<?php

namespace App\Providers;

use App\Models\App_config;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\URL;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // URL::forceScheme('https');
        view()->composer('*', function ($view) {
            $data = Auth::user();
            // dd($data);
            $view->with('dt_user', $data);
        });
    }
}
