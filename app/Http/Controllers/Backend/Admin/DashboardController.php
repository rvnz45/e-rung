<?php

namespace App\Http\Controllers\Backend\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(){
    	if(Auth::check()){
    		return view('admin/layouts/dasboard');
	     }
         return redirect('/admin_bqs/loginxxx')->withSuccess('Oppes! You have entered invalid credentials');
    }
}
