<?php

namespace App\Http\Controllers\Backend\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Models\Produk;
use App\Models\Kasir;
use App\Models\Kasir\Pembelian;
use App\Models\Kasir\Pengeluaran;
Use View;
Use DB;

use App\Models\App_config;

class LandingPageController extends Controller
{
    public function index()
    {
    	$config = [];
    	foreach (App_config::all() as $key) {
    		$config[$key->id] = $key->value;
    	}
        return view('admin.landingpage.all')->with($config);
    }  
    function store(Request $request){
    	$dt_auth 	= Auth::user();
		if ($request->ajax()) {
		 // Setup the validator
			$rules = [
				"title_login"			=> 'required',

				// "logo"						=> 'required',
				"pembukaan_toko"			=> 'required',
				"pembukaan_toko_tambahan"	=> 'required',

				"title_aboutUs"				=> 'required',
				"desc_title_aboutUs"		=> 'required',
				"detail_aboutUs1"			=> 'required',
				"desc_detail_aboutUs1"		=> 'required',
				"detail_aboutUs2"			=> 'required',
				"desc_detail_aboutUs2"		=> 'required',
				"detail_aboutUs3"			=> 'required',
				"desc_detail_aboutUs3"		=> 'required',
				// "img_aboutUs"				=> 'required',

				"title_fact"				=> 'required',
				"desc_title_fact"			=> 'required',
				"detail_fact_angka1"		=> 'required',
				"detail_fact_title1"		=> 'required',
				"detail_fact_angka2"		=> 'required',
				"detail_fact_title2"		=> 'required',
				"detail_fact_angka3"		=> 'required',
				"detail_fact_title3"		=> 'required',
				"detail_fact_angka4"		=> 'required',
				"detail_fact_title4"		=> 'required',

				"title_service"				=> 'required',
				"desc_service"				=> 'required',
				"detail_service1"			=> 'required',
				"desc_detail_service1"		=> 'required',
				"detail_service2"			=> 'required',
				"desc_detail_service2"		=> 'required',
				"detail_service3"			=> 'required',
				"desc_detail_service3"		=> 'required',
				"detail_service4"			=> 'required',
				"desc_detail_service4"		=> 'required',
				"detail_service5"			=> 'required',
				"desc_detail_service5"		=> 'required',
				"detail_service6"			=> 'required',
				"desc_detail_service6"		=> 'required',

				"title_action"				=> 'required',
				"desc_action"				=> 'required',
				"btn_action"				=> 'required',

				"title_tim"					=> 'required',
				"desc_tim"					=> 'required',
				// "img_tim1"					=> 'required',
				"nama_img_tim1"				=> 'required',
				"jabatan_img_tim1"			=> 'required',
				// "img_tim2"					=> 'required',
				"nama_img_tim2"				=> 'required',
				"jabatan_img_tim2"			=> 'required',
				// "img_tim3"					=> 'required',
				"nama_img_tim3"				=> 'required',
				"jabatan_img_tim3"			=> 'required',
				// "img_tim4"					=> 'required',
				"nama_img_tim4"				=> 'required',
				"jabatan_img_tim4"			=> 'required',

				"title_contact"				=> 'required',
				"desc_contact"				=> 'required',
				"peta_contact"				=> 'required',
				"web_alamat"				=> 'required',
				"web_email"					=> 'required',
				"web_nohp"					=> 'required',
				"web_twitter"				=> 'required',
				"web_facebook"				=> 'required',
				"web_instagram"				=> 'required',
			];
			$validator = Validator::make($request->all(), $rules);
			if ($validator->fails()) {
				return response()->json([
					'type' => 'error',
					'errors' => $validator->getMessageBag()->toArray()
				]);
			} else {
				$input = $request->all();

				if ($request->background_login!='') {
					$gambar = App_config::where('id','background_login')->first();
					if(file_exists(public_path($gambar->value))){
				    	unlink(public_path($gambar->value));
				    }
				    $input['image'] = 'background_login_'.time().'.'.$request->background_login->extension();
		    		$request->background_login->move(public_path('images/landingpage'), $input['image']);
		    		DB::table('app_config')
					->where('id', 'background_login')
					->update(['value' => 'images/landingpage/'.$input['image']]);
			    }

				if ($request->logo!='') {
					$gambar = App_config::where('id','logo')->first();
					if(file_exists(public_path($gambar->value))){
				    	unlink(public_path($gambar->value));
				    }
				    $input['image'] = 'logo_'.time().'.'.$request->logo->extension();
		    		$request->logo->move(public_path('images/landingpage'), $input['image']);
		    		DB::table('app_config')
					->where('id', 'logo')
					->update(['value' => 'images/landingpage/'.$input['image']]);
			    }
			    if ($request->img_aboutUs!='') {
					$gambar = App_config::where('id','img_aboutUs')->first();
					if(file_exists(public_path($gambar->value))){
				    	unlink(public_path($gambar->value));
				    }
				    $input['image'] = 'img_aboutUs_'.time().'.'.$request->img_aboutUs->extension();
		    		$request->img_aboutUs->move(public_path('images/landingpage'), $input['image']);
		    		DB::table('app_config')
					->where('id', 'img_aboutUs')
					->update(['value' => 'images/landingpage/'.$input['image']]);
			    }
			    if ($request->img_tim1!='') {
					$gambar = App_config::where('id','img_tim1')->first();
					if(file_exists(public_path($gambar->value))){
				    	unlink(public_path($gambar->value));
				    }
				    $input['image'] = 'img_tim1_'.time().'.'.$request->img_tim1->extension();
		    		$request->img_tim1->move(public_path('images/landingpage'), $input['image']);
		    		DB::table('app_config')
					->where('id', 'img_tim1')
					->update(['value' => 'images/landingpage/'.$input['image']]);
			    }
			    if ($request->img_tim2!='') {
					$gambar = App_config::where('id','img_tim2')->first();
					if(file_exists(public_path($gambar->value))){
				    	unlink(public_path($gambar->value));
				    }
				    $input['image'] = 'img_tim2_'.time().'.'.$request->img_tim2->extension();
		    		$request->img_tim2->move(public_path('images/landingpage'), $input['image']);
		    		DB::table('app_config')
					->where('id', 'img_tim2')
					->update(['value' => 'images/landingpage/'.$input['image']]);
			    }
			    if ($request->img_tim3!='') {
					$gambar = App_config::where('id','img_tim3')->first();
					if(file_exists(public_path($gambar->value))){
				    	unlink(public_path($gambar->value));
				    }
				    $input['image'] = 'img_tim3_'.time().'.'.$request->img_tim3->extension();
		    		$request->img_tim3->move(public_path('images/landingpage'), $input['image']);
		    		DB::table('app_config')
					->where('id', 'img_tim3')
					->update(['value' => 'images/landingpage/'.$input['image']]);
			    }
			    if ($request->img_tim4!='') {
					$gambar = App_config::where('id','img_tim4')->first();
					if(file_exists(public_path($gambar->value))){
				    	unlink(public_path($gambar->value));
				    }
				    $input['image'] = 'img_tim4_'.time().'.'.$request->img_tim4->extension();
		    		$request->img_tim4->move(public_path('images/landingpage'), $input['image']);
		    		DB::table('app_config')
					->where('id', 'img_tim4')
					->update(['value' => 'images/landingpage/'.$input['image']]);
			    }
				DB::table('app_config')
					->where('id', 'title_login')
					->update(['value' => $input['title_login']]);

				DB::table('app_config')
					->where('id', 'pembukaan_toko')
					->update(['value' => $input['pembukaan_toko']]);

				DB::table('app_config')
					->where('id', 'pembukaan_toko_tambahan')
					->update(['value' => $input['pembukaan_toko_tambahan']]);
				DB::table('app_config')
					->where('id', 'title_aboutUs')
					->update(['value' => $input['title_aboutUs']]);
				DB::table('app_config')
					->where('id', 'desc_title_aboutUs')
					->update(['value' => $input['desc_title_aboutUs']]);
				DB::table('app_config')
					->where('id', 'detail_aboutUs1')
					->update(['value' => $input['detail_aboutUs1']]);
				DB::table('app_config')
					->where('id', 'desc_detail_aboutUs1')
					->update(['value' => $input['desc_detail_aboutUs1']]);
				DB::table('app_config')
					->where('id', 'detail_aboutUs2')
					->update(['value' => $input['detail_aboutUs2']]);
				DB::table('app_config')
					->where('id', 'desc_detail_aboutUs2')
					->update(['value' => $input['desc_detail_aboutUs2']]);
				DB::table('app_config')
					->where('id', 'detail_aboutUs3')
					->update(['value' => $input['detail_aboutUs3']]);
				DB::table('app_config')
					->where('id', 'desc_detail_aboutUs3')
					->update(['value' => $input['desc_detail_aboutUs3']]);

				DB::table('app_config')
					->where('id', 'title_fact')
					->update(['value' => $input['title_fact']]);
				DB::table('app_config')
					->where('id', 'desc_title_fact')
					->update(['value' => $input['desc_title_fact']]);
				DB::table('app_config')
					->where('id', 'detail_fact_angka1')
					->update(['value' => $input['detail_fact_angka1']]);
				DB::table('app_config')
					->where('id', 'detail_fact_title1')
					->update(['value' => $input['detail_fact_title1']]);
				DB::table('app_config')
					->where('id', 'detail_fact_angka2')
					->update(['value' => $input['detail_fact_angka2']]);
				DB::table('app_config')
					->where('id', 'detail_fact_title2')
					->update(['value' => $input['detail_fact_title2']]);
				DB::table('app_config')
					->where('id', 'detail_fact_angka3')
					->update(['value' => $input['detail_fact_angka3']]);
				DB::table('app_config')
					->where('id', 'detail_fact_title3')
					->update(['value' => $input['detail_fact_title3']]);
				DB::table('app_config')
					->where('id', 'detail_fact_angka4')
					->update(['value' => $input['detail_fact_angka4']]);
				DB::table('app_config')
					->where('id', 'detail_fact_title4')
					->update(['value' => $input['detail_fact_title4']]);
				DB::table('app_config')
					->where('id', 'title_service')
					->update(['value' => $input['title_service']]);
				DB::table('app_config')
					->where('id', 'desc_service')
					->update(['value' => $input['desc_service']]);
				DB::table('app_config')
					->where('id', 'detail_service1')
					->update(['value' => $input['detail_service1']]);
				DB::table('app_config')
					->where('id', 'desc_detail_service1')
					->update(['value' => $input['desc_detail_service1']]);
				DB::table('app_config')
					->where('id', 'detail_service2')
					->update(['value' => $input['detail_service2']]);
				DB::table('app_config')
					->where('id', 'desc_detail_service2')
					->update(['value' => $input['desc_detail_service2']]);
				DB::table('app_config')
					->where('id', 'detail_service3')
					->update(['value' => $input['detail_service3']]);
				DB::table('app_config')
					->where('id', 'desc_detail_service3')
					->update(['value' => $input['desc_detail_service3']]);
				DB::table('app_config')
					->where('id', 'detail_service4')
					->update(['value' => $input['detail_service4']]);
				DB::table('app_config')
					->where('id', 'desc_detail_service4')
					->update(['value' => $input['desc_detail_service4']]);
				DB::table('app_config')
					->where('id', 'detail_service5')
					->update(['value' => $input['detail_service5']]);
				DB::table('app_config')
					->where('id', 'desc_detail_service5')
					->update(['value' => $input['desc_detail_service5']]);
				DB::table('app_config')
					->where('id', 'detail_service6')
					->update(['value' => $input['detail_service6']]);
				DB::table('app_config')
					->where('id', 'desc_detail_service6')
					->update(['value' => $input['desc_detail_service6']]);
				DB::table('app_config')
					->where('id', 'title_action')
					->update(['value' => $input['title_action']]);
				DB::table('app_config')
					->where('id', 'desc_action')
					->update(['value' => $input['desc_action']]);
				DB::table('app_config')
					->where('id', 'btn_action')
					->update(['value' => $input['btn_action']]);
				DB::table('app_config')
					->where('id', 'title_tim')
					->update(['value' => $input['title_tim']]);
				DB::table('app_config')
					->where('id', 'desc_tim')
					->update(['value' => $input['desc_tim']]);
				DB::table('app_config')
					->where('id', 'nama_img_tim1')
					->update(['value' => $input['nama_img_tim1']]);
				DB::table('app_config')
					->where('id', 'jabatan_img_tim1')
					->update(['value' => $input['jabatan_img_tim1']]);
				DB::table('app_config')
					->where('id', 'nama_img_tim2')
					->update(['value' => $input['nama_img_tim2']]);
				DB::table('app_config')
					->where('id', 'jabatan_img_tim2')
					->update(['value' => $input['jabatan_img_tim2']]);
				DB::table('app_config')
					->where('id', 'nama_img_tim3')
					->update(['value' => $input['nama_img_tim3']]);
				DB::table('app_config')
					->where('id', 'jabatan_img_tim3')
					->update(['value' => $input['jabatan_img_tim3']]);
				DB::table('app_config')
					->where('id', 'nama_img_tim4')
					->update(['value' => $input['nama_img_tim4']]);
				DB::table('app_config')
					->where('id', 'jabatan_img_tim4')
					->update(['value' => $input['jabatan_img_tim4']]);
				DB::table('app_config')
					->where('id', 'title_contact')
					->update(['value' => $input['title_contact']]);
				DB::table('app_config')
					->where('id', 'desc_contact')
					->update(['value' => $input['desc_contact']]);
				DB::table('app_config')
					->where('id', 'peta_contact')
					->update(['value' => $input['peta_contact']]);
				DB::table('app_config')
					->where('id', 'web_alamat')
					->update(['value' => $input['web_alamat']]);
				DB::table('app_config')
					->where('id', 'web_email')
					->update(['value' => $input['web_email']]);
				DB::table('app_config')
					->where('id', 'web_nohp')
					->update(['value' => $input['web_nohp']]);
				DB::table('app_config')
					->where('id', 'web_twitter')
					->update(['value' => $input['web_twitter']]);
				DB::table('app_config')
					->where('id', 'web_facebook')
					->update(['value' => $input['web_facebook']]);
				DB::table('app_config')
					->where('id', 'web_instagram')
					->update(['value' => $input['web_instagram']]);
				return response()->json(['type' => 'success', 'message' => "Successfully Created"]);

			}
		} else {
			return response()->json(['status' => 'false', 'message' => "Access only ajax request"]);
		}
	}
	function getChartData(Request $request){
		if ($request->ajax()) {
			$date1 = date("Y-m-".'01');
			$date2 = date("Y-m-t");
			$produk = Produk::where(DB::raw('sub_id is null or sub_id=""'))->count();
			$kasir 	= Kasir::select(DB::raw('sum(total_asal) as total_asal'),DB::raw('sum(total_bayar) as total_bayar'))->where('tanggal','>=',$date1)->where('tanggal','<=',$date2)->first();
			$kasir 	= Kasir::select(DB::raw('sum(total_asal) as total_asal'),DB::raw('sum(total_bayar) as total_bayar'))->where('tanggal','>=',$date1)->where('tanggal','<=',$date2)->first();
			$pembelian 	= Pembelian::select(DB::raw('sum(total_nilai) as total_nilai'))->where('tanggal','>=',$date1)->where('tanggal','<=',$date2)->first();
			$pengeluaran= Pengeluaran::select(DB::raw('sum(total_nilai) as total_nilai'))->where('tanggal','>=',$date1)->where('tanggal','<=',$date2)->first();
			$kasirData 	= Kasir::select(DB::raw('sum(total_bayar) as total_bayar'),DB::raw('substring(tanggal,9,10) as tgl'))->where('tanggal','>=',$date1)->where('tanggal','<=',$date2)->groupBy(DB::raw('substring(tanggal,9,10)'))->get();
			foreach ($kasirData as $key) {
				$totalBayar[(int)$key->tgl] = $key->total_bayar;
			}
			$dt_tanggal = [];
			$dt_pembayaran = [];
			for ($i=1; $i <=date("t") ; $i++) { 
				$dt_tanggal[] = $i;
				$dt_pembayaran[] = isset($totalBayar[$i]) ? $totalBayar[$i] : 0;
			}
			return response()->json(['status' => 'true', 'produk' => $produk, 'kasir' => number_format($kasir->total_bayar,2,',','.'), 'pembelian' => number_format($pembelian->total_nilai,2,',','.'), 'pengeluaran' => number_format($pengeluaran->total_nilai,2,',','.'),'dt_tanggal' => $dt_tanggal,'dt_pembayaran' => $dt_pembayaran]);
		} else {
			return response()->json(['status' => 'false', 'message' => "Access only ajax request"]);
		}
	}
}
