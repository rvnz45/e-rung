<?php

namespace App\Http\Controllers\Backend\Admin\Laporan;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use App\Models\Kasir;
use App\Models\Produk;
use App\Models\HistoriProduk;

use App\Models\KasirDetail;
Use DB;
Use View;

class ProdukController extends Controller
{
    public function index(){
    	return view('admin.laporan.produk.all');
    }
    function allLaporanProduk(Request $request){
    	$input = $request->all();
    	$dt_kasir = Kasir::getAllLaporanProdukKasir($input,'data');
        $no = isset($input['start']) ? $input['start'] : 0;
        $data = array();
        foreach($dt_kasir as $kasir){
            $no++;
            $row = array();
        	$row[] = $no;
            $row[] = $kasir->nama;
            $row[] = $kasir->total_terbeli;
            $row[] = $kasir->total_harga_beli;
            $row[] = $kasir->total_harga_terjual;
            $row[] = $kasir->total_harga_terjual - $kasir->total_harga_beli;

            $data[] = $row;
        }
        $output = array(
		            "draw" => $input['draw'],
		            "recordsTotal" =>  Kasir::getAllLaporanProdukKasir($input,'total'),
		            "recordsFiltered" => Kasir::getAllLaporanProdukKasir($input,'raw'),
		            "data" => $data,
		            );
		//output to json format
		echo json_encode($output);
    }
    function getTotalKasirDetail(Request $request){
        $input = $request->all();
        $kasir = KasirDetail::select(DB::raw("sum(jumlah) as total_item"),DB::raw("sum(jumlah*harga_beli) as total_asal"),DB::raw("sum(jumlah*harga_terjual) as total_terbayar"),DB::raw("sum(jumlah*harga_jual) as total_bayar"))
            ->join('trans_kasir','trans_kasir.id','=','trans_kasir_detail.kasir_id')->where('trans_kasir.time','>=',strtotime($input['tgl_pembelian_a'].' 00:00:00'))->where('trans_kasir.time','<=',strtotime($input['tgl_pembelian_b'].' 23:59:59'))->first();
        return response()->json(['message' => 'success', 'data' => $kasir]);        
    }
}
