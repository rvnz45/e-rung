<?php

namespace App\Http\Controllers\Backend\Admin\Laporan;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use App\Models\Kasir;
use App\Models\Produk;
use App\Models\HistoriProduk;

use App\Models\KasirDetail;
Use DB;
Use View;

class KasirController extends Controller
{
    public function index(){
    	return view('admin.laporan.kasir.all');
    }
    function allLaporanKasir(Request $request){
    	$input = $request->all();
    	$dt_kasir = Kasir::getAllKasir($input,'data');
        $no = isset($input['start']) ? $input['start'] : 0;
        $data = array();
        foreach($dt_kasir as $kasir){
            $no++;
            $row = array();
        	$row[] = $no;
            $row[] = $kasir->pembeli;
            $row[] = $kasir->total_asal;
            $row[] = $kasir->total_nilai;
            $row[] = $kasir->diskon;
            $row[] = $kasir->total_bayar;
            $row[] = $kasir->total_terbayar;
            $row[] = $kasir->total_hutang;
            $row[] = "<a data-toggle='tooltip' class='col-md-5 btn btn-warning btn-md edit' id='".$kasir->id."' title='Edit' style='padding:20px'> <i class='fa fa-edit text-error'></i></a> <a data-toggle='tooltip' class='col-md-5 btn btn-danger btn-md  delete' id='".$kasir->id."' title='Delete'  style='padding:20px'> <i class='fa fa-trash-alt'></i></a>";

            $data[] = $row;
        }
        $output = array(
		            "draw" => $input['draw'],
		            "recordsTotal" =>  Kasir::getAllKasir($input,'total'),
		            "recordsFiltered" => Kasir::getAllKasir($input,'raw'),
		            "data" => $data,
		            );
		//output to json format
		echo json_encode($output);
    }
    function allLaporanKasirDetail(Request $request){
    	$input = $request->all();
    	$dt_keranjang = KasirDetail::getAllKeranjang($input,'data');
        $no = isset($input['start']) ? $input['start'] : 0;
        $data = array();
        foreach($dt_keranjang as $keranjang){
            $no++;
            $row = array();
            
            $row[] = $keranjang->kasir_id.'__'.$keranjang->id;
            $row[] = $keranjang->trans_produk_nama;
            $row[] = $keranjang->jumlah.' '.$keranjang->mst_satuan_nama;
            $row[] = number_format($keranjang->harga_terjual,2);
            $row[] = number_format($keranjang->jumlah * $keranjang->harga_terjual,2);

            $data[] = $row;
        }
        $output = array(
		            "draw" => $input['draw'],
		            "recordsTotal" =>  KasirDetail::getAllKeranjang($input,'total'),
		            "recordsFiltered" => KasirDetail::getAllKeranjang($input,'raw'),
		            "data" => $data,
		            );
		//output to json format
		echo json_encode($output);
    }
    function edit(Request $request, $id){
		$kasir = DB::table('trans_kasir')
            ->select('trans_kasir.*')
            ->where('trans_kasir.id', $id)
            ->first();
        // dd($pembelian);
    	return view('admin.laporan.kasir.edit',compact('kasir'))->with(['act'=>'edit']);
    }
    function keranjangProduk(Request $request,$id){
    	if($request->ajax()){
    		$id = explode("__", $id);
    		$produk = DB::table('trans_produk')
    		->join('trans_kasir_detail',  'trans_produk.id', '=', 'trans_kasir_detail.produk_id')
    		->join('mst_kategori',  'mst_kategori.id', '=', 'trans_produk.kategori_id')
    		->join('mst_tipe',  'mst_tipe.id', '=', 'trans_produk.tipe_id')
    		->join('mst_satuan',  'mst_satuan.id', '=', 'trans_produk.satuan_id')
            ->select('trans_produk.*', 'mst_kategori.nama as mst_kategori_nama', 'mst_tipe.nama as mst_tipe_nama', 'mst_satuan.nama as mst_satuan_nama','trans_kasir_detail.jumlah','trans_kasir_detail.harga_terjual','trans_kasir_detail.id as keranjang_id')
            ->where('trans_kasir_detail.id', $id[1])
            ->where('trans_kasir_detail.kasir_id', $id[0])
            ->first();
            $kasir = (object)[
			    'kasir_id'  => $id[0],
			];
    		$view = View::make('admin.laporan.kasir.editProduk',compact(['produk','kasir']))->render();
    		return response()->json(['html' => $view]);
    	} else {
			return response()->json(['status' => 'false', 'message' => "Access only ajax request"]);
		}
    }
    function updatePembelian(Request $request){
    	$dt_auth 	= Auth::user();
		if ($request->ajax()) {
		 // Setup the validator
			$rules = [
				'input_detail_action'		=> 'required',
			];
			$validator = Validator::make($request->all(), $rules);
			if ($validator->fails()) {
				return response()->json([
					'type' => 'error',
					'errors' => $validator->getMessageBag()->toArray()
				]);
			} else {
				DB::beginTransaction();

				try {
				    $input = $request->all();
					$diskonProduk	= $input['input_diskon_produk'];
					foreach ($input as $key => $value) {
						if (substr($key, 0,25)=='input_jumlah_beli_produk_') {
							$idProduk 	= explode('_', $key);
							$history = HistoriProduk::where('link','kasir/'.$input['kasir_id'].'/edit')->where('produk_id',$idProduk[4])->where('jenis','kasir');
							$history->delete();
							$keranjang = KasirDetail::where('id',sprintf('%05d', $input['keranjang_id']))->where('kasir_id',$input['kasir_id'])->where('produk_id',$idProduk[4])->first();

							$produk = Produk::where('id', $idProduk[4])->first();
							$produk->total_terbeli = $produk->total_terbeli - $keranjang->jumlah;
							$produk->stok = $produk->stok + $keranjang->jumlah;
							$produk->save();

							$updateParent = Produk::UpdateStok($produk->sub_id,'update');

							$keranjang->delete();

    						
						}
					}
				    DB::commit();
					return response()->json(['type' => 'success', 'message' => "Successfully Created"]);

				    // all good
				} catch (\Exception $e) {
				    DB::rollback();
				    dd($e);
					return response()->json(['type' => 'error', 'message' => "Silahkan cek koneksi Anda"]);
				    // something went wrong
				}
			}
		} else {
			return response()->json(['status' => 'false', 'message' => "Access only ajax request"]);
		}
    }

    function destroy(Request $request, $id){
    	if($request->ajax()){
    		DB::beginTransaction();

			try {
				$kasirDetail = KasirDetail::where('kasir_id', $id)->get();
				foreach ($kasirDetail as $key) {
					$produk = Produk::where('id',$key->produk_id)->first();
					$produk->stok = $produk->stok + $key->jumlah;
					$produk->total_terbeli = $produk->total_terbeli - $key->jumlah;
					$produk->save();

					$updateParent = Produk::UpdateStok($produk->sub_id,'update');

				}
	    		DB::table('trans_kasir_detail')
	    		->where('kasir_id', $id)
	    		->delete();

	    		DB::table('histori_produk')
	    		->where('link', 'kasir/'.$id.'/edit')
	    		->where('jenis', 'kasir')
	    		->delete();

	    		$judul = Kasir::find($id);
				$judul->delete();
			    DB::commit();
	        	return response()->json(['type' => 'success', 'message' => "Successfully Deleted"]);

			    // all good
			} catch (\Exception $e) {
				dd($e);
			    DB::rollback();
			    // something went wrong
				return response()->json(['type' => 'error', 'message' => "Silahkan cek koneksi Anda"]);
			}
    	} else {
			return response()->json(['status' => 'false', 'message' => "Access only ajax request"]);
		}
    }
    function getKeranjangUpdate($id){
    	$kasir = KasirDetail::select(DB::raw("sum(jumlah) as total_item"),DB::raw("sum(jumlah*harga_beli) as total_nilai_asal"),DB::raw("sum(jumlah*harga_jual) as total_nilai_jual"),DB::raw("sum(jumlah*harga_terjual) as total_nilai_terjual"))->where('kasir_id',$id)->first();
		return response()->json(['message' => 'success', 'data' => $kasir]);
    }
    function updateCheckOut(Request $request,$id){
    	$dt_auth 	= Auth::user();
		if ($request->ajax()) {
		 // Setup the validator
			$rules = [
				'input_subtotal'	=> 'required',
				'input_diskon'		=> 'required',
				'input_total_bayar'	=> 'required',
				'input_bayar'		=> 'required',
				'input_selisih'		=> 'required',
			];
			$validator = Validator::make($request->all(), $rules);
			if ($validator->fails()) {
				return response()->json([
					'type' => 'error',
					'errors' => $validator->getMessageBag()->toArray()
				]);
			} else {
				$input = $request->all();
				DB::beginTransaction();
				try {
				    $input = $request->all();
					$total 		= KasirDetail::select(DB::raw("sum(jumlah) as total_item"),DB::raw("sum(jumlah*harga_beli) as total_nilai_asal"),DB::raw("sum(jumlah*harga_jual) as total_nilai_jual"),DB::raw("sum(jumlah*harga_terjual) as total_nilai_terjual"))->first();
					$kasir 		= Kasir::where('id',$id)->first();
					DB::table('trans_kasir')
	                ->where('id', $id)
	                ->update([
	                	'tanggal'		=> date("Y-m-d H:i:s"),
						'time'			=> time(),
	                	'pembeli'		=> $input['input_pembeli'],
						'total_item'	=> $total->total_item,
						'total_asal'	=> $total->total_nilai_asal,
						'total_nilai'	=> $total->total_nilai_jual,
						'diskon'		=> $input['input_diskon'],
						'total_bayar'	=> $input['input_total_bayar'],
						'total_terbayar'=> $input['input_bayar'],
						'total_hutang'	=> $input['input_bayar'] - $input['input_total_bayar'],
						'keterangan'	=> '-',
						'user_id'		=> 	$dt_auth->id,
	                ]);

				    DB::commit();
					return response()->json(['type' => 'success', 'message' => "Successfully Created"]);

				    // all good
				} catch (\Exception $e) {
				    DB::rollback();
					return response()->json(['type' => 'error', 'message' => "Silahkan cek koneksi Anda"]);
				    // something went wrong
				}
			}
		} else {
			return response()->json(['status' => 'false', 'message' => "Access only ajax request"]);
		}
    }
    function getTotalKasir(Request $request){
    	$input = $request->all();
		$kasir = Kasir::select(DB::raw("sum(total_item) as total_item"),DB::raw("sum(total_asal) as total_asal"),DB::raw("sum(total_nilai) as total_nilai"),DB::raw("sum(total_bayar) as total_bayar"),DB::raw("sum(total_terbayar) as total_terbayar"))->where('time','>=',strtotime($input['tgl_pembelian_a'].' 00:00:00'))->where('time','<=',strtotime($input['tgl_pembelian_b'].' 23:59:59'))->first();
		return response()->json(['message' => 'success', 'data' => $kasir]);    	
    }
}
