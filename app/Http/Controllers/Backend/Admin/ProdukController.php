<?php

namespace App\Http\Controllers\Backend\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use App\Models\Produk;
use Illuminate\Support\Facades\Validator;
Use View;
Use DB;

class ProdukController extends Controller
{
    function index(){
    	return view('admin.produk.all');
    }
    public function allProduk(Request $request){
    	$input = $request->all();
    	$dt_produk = Produk::getAllProduk($input,'data');
        $no = isset($input['start']) ? $input['start'] : 0;
        $sumber = isset($input['sumber']) ? $input['sumber'] : '';
        $pembelian_id = isset($input['pembelian_id']) ? $input['pembelian_id'] : '';
        $pengeluaran_id = isset($input['pengeluaran_id']) ? $input['pengeluaran_id'] : '';
        $stok_opname_id = isset($input['stok_opname_id']) ? $input['stok_opname_id'] : '';
        $paket_menu_id = isset($input['paket_menu_id']) ? $input['paket_menu_id'] : '';
        $data = array();
        foreach($dt_produk as $produk){
        	$child = Produk::hasChildMaxMin($produk->id);
        	if (isset($child->harga_max)!='' && isset($child->harga_min)!='') {
        		$harga_max 		= ($child->harga_max > 0 ? number_format($child->harga_max,2) : '');
        		$harga_min 		= ($child->harga_min > 0 ? number_format($child->harga_min,2) : '');
        		$tampil_harga 	= $harga_min.' - '.$harga_max;

        		$tgl_kadaluarsa_max = ($child->tgl_kadaluarsa_max !='' ? date("d M Y",strtotime($child->tgl_kadaluarsa_max)) : '');
        		$tgl_kadaluarsa_min = ($child->tgl_kadaluarsa_min !='' ? date("d M Y",strtotime($child->tgl_kadaluarsa_min)) : '');
        		$tampil_expire 		= $tgl_kadaluarsa_min.' - '.$tgl_kadaluarsa_max;
        		$nama = $produk->nama.'<ul><li>'.$produk->stok.' '.$produk->mst_satuan_nama.'</li><li>Rp. '.$tampil_harga.'</li><li>ED : '.($produk->is_expire=='1' ? $tampil_expire : '-').'</li></ul>';
        	}else{
        		$nama = $produk->nama.'<ul><li>'.$produk->stok.' '.$produk->mst_satuan_nama.'</li><li>Rp. '.number_format($produk->harga_jual,2).'</li><li>ED : '.($produk->is_expire=='1' ? date("d M Y",strtotime($produk->tgl_kadaluarsa)) : '-').'</li></ul>';
        	}
        	$url= asset($produk->foto);
            $no++;
            $row = array();
        	$row[] = $no;
            $row[] = $produk->foto!='' && file_exists(public_path($produk->foto)) ?  '<img src="'.$url.'"   height="50" class="img-rounded" align="center" />' : "Kosong";
            $row[] = $nama;
            if ($sumber=='pembelian' || $sumber=='pengeluaran' || $sumber=='stokOpname'|| $sumber=='paketMenu') {
            	$id_luar = ($sumber=='pembelian' ? $pembelian_id : ($sumber=='pengeluaran' ? $pengeluaran_id : ($sumber=='stokOpname' ? $stok_opname_id : $paket_menu_id)));
            	$row[] = "<a data-toggle='tooltip' class='col-md-12 btn btn-warning btn-md edit' id='".$produk->id.'__'.$id_luar."' title='Beli' style='padding:20px'> <i class='fa fa-edit text-error'></i></a>";
            }else{
            	$row[] = "<a data-toggle='tooltip' class='col-md-5 btn btn-warning btn-md edit' id='".$produk->id."' title='Edit' style='padding:20px'> <i class='fa fa-edit text-error'></i></a> <a data-toggle='tooltip' class='col-md-5 btn btn-danger btn-md  delete' id='".$produk->id."' title='Delete'  style='padding:20px'> <i class='fa fa-trash-alt'></i></a>";
            }

            $data[] = $row;
        }
        $output = array(
		            "draw" => $input['draw'],
		            "recordsTotal" =>  Produk::getAllProduk($input,'total'),
		            "recordsFiltered" => Produk::getAllProduk($input,'raw'),
		            "data" => $data,
		            );
		//output to json format
		echo json_encode($output);
    }
    function create(Request $request){
    	if ($request->ajax()) {
    		$view = View::make('admin.produk.create')->render();
    		return response()->json(['html' => $view]);
    	}else{
         	return response()->json(['status' => 'false', 'message' => "Access only ajax request"]);
    	}
    }
    function store(Request $request){
    	$dt_auth 	= Auth::user();
		if ($request->ajax()) {
		 // Setup the validator
			$rules = [
				'input_nama' 		=> 'required',
				// 'input_foto' 		=> 'required',
				'input_tipe' 		=> 'required',
				'input_tipe_id'		=> 'required',
				// 'input_kategori'	=> 'required',
				// 'input_kategori_id'	=> 'required',
				'input_harga_beli'	=> 'required',
				'input_harga_jual'	=> 'required',
				'input_stok'		=> 'required',
				'input_satuan'		=> 'required',
				'input_satuan_id'	=> 'required',
				// 'input_status_confirm'=> 'required',
				// 'input_is_alert'	=> 'required',
				// 'input_is_tracking'	=> 'required',
			];
			if ($request->input('input_is_expire')=='on') {
				$addRules = [
					'input_tgl_kadaluarsa'	=> 'required'
				];
				$rules = array_merge($rules,$addRules);
			}
			$validator = Validator::make($request->all(), $rules);
			if ($validator->fails()) {
				return response()->json([
					'type' => 'error',
					'errors' => $validator->getMessageBag()->toArray()
				]);
			} else {
				$id = 'P'.time();

    			
					Produk::create([
						'id'			=> $id,
					    'nama' 			=> ucwords(strtolower($request->input('input_nama'))),
					    'stok' 			=> $request->input('input_stok'),
					    'is_alert'		=> 1,//$request->input('input_is_alert')=='on' ? 1 : 0,
					    'stok_min' 		=> $request->input('input_stok_min'),
					    'satuan_id'		=> $request->input('input_satuan_id'),
					    'is_tracking'	=> 1,//request->input('input_is_tracking')=='on' ? 1 : 0,
					    'harga_beli' 	=> $request->input('input_harga_beli'),
					    'harga_jual' 	=> $request->input('input_harga_jual'),
					    'diskon' 		=> $request->input('input_diskon'),
					    'total_terbeli'	=> 0,
					    'status'		=> $request->input('input_status_confirm')=='on' ? 1 : 0,
					    'user_id'		=> $dt_auth->id,
					    'kategori_id'	=> $request->input('input_kategori_id')!='' ? $request->input('input_kategori_id') : '01',
					    'tipe_id'		=> $request->input('input_tipe_id'),
					    'is_expire'		=> $request->input('input_is_expire')=='on' ? 1 : 0,
					    'tgl_kadaluarsa'=> $request->input('input_is_expire')=='on' ? date("Y-m-d",strtotime($request->input('input_tgl_kadaluarsa'))) : NULL,
					]);
				if ($request->input_foto!='') {
					$input['image'] = $id.'_'.time().'.'.$request->input_foto->extension();
	        		if ($request->input_foto->move(public_path('images/produk'), $input['image'])) {
	        			Produk::where('id',$id)
	        			->update(['foto' => 'images/produk/'.$input['image']]);
						return response()->json(['type' => 'success', 'message' => "Successfully Created"]);
	        		}else{
						return response()->json(['error'=>$validator->errors()->all()]);
	        		}
				}
				return response()->json(['type' => 'success', 'message' => "Successfully Created"]);
			}
		} else {
			return response()->json(['status' => 'false', 'message' => "Access only ajax request"]);
		}
    }
    function edit(Request $request, $id){
    	if($request->ajax()){
    		$produk = DB::table('trans_produk')
    		->join('mst_kategori',  'mst_kategori.id', '=', 'trans_produk.kategori_id')
    		->join('mst_tipe',  'mst_tipe.id', '=', 'trans_produk.tipe_id')
    		->join('mst_satuan',  'mst_satuan.id', '=', 'trans_produk.satuan_id')
            ->select('trans_produk.*', 'mst_kategori.nama as mst_kategori_nama', 'mst_tipe.nama as mst_tipe_nama', 'mst_satuan.nama as mst_satuan_nama')
            ->where('trans_produk.id', $id)
            ->first();
    		$view = View::make('admin.produk.edit',compact('produk'))->render();
    		return response()->json(['html' => $view]);
    	} else {
			return response()->json(['status' => 'false', 'message' => "Access only ajax request"]);
		}
    }
    function update(Request $request, $id){
    	$dt_auth 	= Auth::user();
		$child = Produk::hasChildMaxMin($id);
		if ($request->ajax()) {
		 // Setup the validator
			$rules = [
				'input_nama' 		=> 'required',
				'input_tipe' 		=> 'required',
				'input_tipe_id'		=> 'required',
				'input_kategori'	=> 'required',
				// 'input_harga_beli'	=> 'required',
				// 'input_harga_jual'	=> 'required',
				'input_satuan'		=> 'required',
				'input_satuan_id'	=> 'required',
			];
			if ($request->input('input_is_expire')=='on' && isset($child->harga_max)=='' && isset($child->harga_min)=='') {
				$addRules = [
					'input_tgl_kadaluarsa'	=> 'required'
				];
				$rules = array_merge($rules,$addRules);
			}
			$validator = Validator::make($request->all(), $rules);
			if ($validator->fails()) {
				return response()->json([
				'type' => 'error',
				'errors' => $validator->getMessageBag()->toArray()
				]);
			} else {
				if ($request->input_foto!='') {
					$gambar = Produk::where('id',$id)->first();
					if ($gambar->foto!='') {
						if(file_exists(public_path($gambar->foto))){
					    	unlink(public_path($gambar->foto));
					    }
					}
				    $input['image'] = $id.'_'.time().'.'.$request->input_foto->extension();
		    		$request->input_foto->move(public_path('images/produk'), $input['image']);
			    }
        			$produk = Produk::find($id);
					$produk->nama 		= strtoupper(strtolower($request->input_nama));
					$produk->is_alert 	= 1;//$request->input_is_alert=='on' ? 1 : 0;
					$produk->stok_min 	= $request->input_stok_min;
					$produk->satuan_id	= $request->input_satuan_id;
					$produk->is_tracking= 1;//$request->input_is_tracking=='on' ? 1 : 0;
					$produk->harga_beli	= $request->input_harga_beli;
					$produk->harga_jual	= $request->input_harga_jual;
					$produk->diskon		= $request->input_diskon;
					if ($request->input_foto!='') {
					$produk->foto		= 'images/produk/'.$input['image'];
					}
					$produk->status		= $request->input_status_confirm=='on' ? 1 : 0;
					$produk->user_id	= $dt_auth->id;
					$produk->kategori_id= $request->input_kategori_id!='' ? $request->input_kategori_id : '01';
					$produk->tipe_id	= $request->input_tipe_id;
					$produk->is_expire 	= $request->input_is_expire=='on' ? 1 : 0;
					$produk->tgl_kadaluarsa= $request->input_is_expire=='on' && isset($child->harga_max)=='' && isset($child->harga_min)=='' ? date("Y-m-d",strtotime($request->input_tgl_kadaluarsa)) : NULL;
					$produk->save();
	            	return response()->json(['type' => 'success', 'message' => "Successfully Updated"]);
			}
    	} else {
			return response()->json(['status' => 'false', 'message' => "Access only ajax request"]);
		}
    }
    function destroy(Request $request, $id){
    	if($request->ajax()){
    		DB::beginTransaction();

			try {
			    $gambar = DB::table('trans_produk')
	    		->where('sub_id', $id)
	    		->get();
	    		foreach ($gambar as $key) {
	    			if(file_exists(public_path($key->foto))){
				    	unlink(public_path($key->foto));
				    }
	    		}
	    		DB::table('trans_produk')
	    		->where('sub_id', $id)
	    		->delete();

	    		$gambar2 = Produk::find($id);
				if(file_exists(public_path($gambar2->foto))){
			    	unlink(public_path($gambar2->foto));
			    }

	    		$judul = Produk::find($id);
				$judul->delete();
			    DB::commit();
	        	return response()->json(['type' => 'success', 'message' => "Successfully Deleted"]);

			    // all good
			} catch (\Exception $e) {
			    DB::rollback();
			    // something went wrong
				return response()->json(['type' => 'error', 'message' => "Silahkan cek koneksi Anda"]);
			}
    	} else {
			return response()->json(['status' => 'false', 'message' => "Access only ajax request"]);
		}
    }
}
