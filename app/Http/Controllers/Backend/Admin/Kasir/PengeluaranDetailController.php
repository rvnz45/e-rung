<?php

namespace App\Http\Controllers\Backend\Admin\Kasir;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
Use View;
Use DB;

use App\Models\Kasir\PengeluaranDetail;
use App\Models\Kasir\Pengeluaran;

class PengeluaranDetailController extends Controller
{
    function destroy(Request $request, $id){
    	if($request->ajax()){
    		DB::beginTransaction();

			try {
			    $id = explode("__", $id);
	    		$pengeluaran = PengeluaranDetail::where('pengeluaran_id',$id[1])->where('id',$id[0]);
				$pengeluaran->delete();

	    		$pengeluaran = PengeluaranDetail::select(DB::raw('sum(jumlah) as total_item'),DB::raw('sum(jumlah*harga) as total_nilai'))
	    		->where('pengeluaran_id',$id[1])->first();
				 DB::table('trans_pengeluaran')
	                ->where('id', $id[1])
	                ->update([
	                	'total_nilai' => $pengeluaran->total_nilai > 0 ? $pengeluaran->total_nilai : 0,
	                	'total_item' => $pengeluaran->total_item > 0 ? $pengeluaran->total_item : 0,
	                ]);
			    DB::commit();
	        	return response()->json(['type' => 'success', 'message' => "Successfully Deleted", 'total_nilai' => number_format($pengeluaran->total_nilai), 'total_item' => $pengeluaran->total_item]);

			    // all good
			} catch (\Exception $e) {
			    DB::rollback();
			    // something went wrong
				return response()->json(['type' => 'error', 'message' => "Silahkan cek koneksi Anda"]);
			}
    	} else {
			return response()->json(['status' => 'false', 'message' => "Access only ajax request"]);
		}
    }
}
