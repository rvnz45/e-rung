<?php

namespace App\Http\Controllers\Backend\Admin\Kasir;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
Use View;
Use DB;

use App\Models\Kasir\Pembelian;
use App\Models\Produk;
use App\Models\HistoriProduk;
use App\Models\Kasir\PembelianDetail;

class PembelianController extends Controller
{
    function index(){
    	return view('admin.pembelian.all');
    }
    public function allPembelian(Request $request){
    	$input = $request->all();
    	$dt_pembelian = Pembelian::getAllPembelian($input,'data');
        $no = isset($input['start']) ? $input['start'] : 0;
        $data = array();
        foreach($dt_pembelian as $pembelian){
            $no++;
            $row = array();
        	$row[] = $no;
            $row[] = date("Y-m-d",strtotime($pembelian->tanggal));
            $row[] = $pembelian->total_nilai;
            $row[] = $pembelian->keterangan;
            $row[] = "<a data-toggle='tooltip' class='col-md-3 btn btn-warning btn-md edit' id='".$pembelian->id."' title='Edit'> <i class='fa fa-edit text-error'></i></a> <a data-toggle='tooltip' class='col-md-3 btn btn-danger btn-md  ".($pembelian->status=='1' ? '' : 'delete')."' id='".$pembelian->id."' title='Delete'> <i class='fa fa-trash-alt'></i></a>";

            $data[] = $row;
        }
        $output = array(
		            "draw" => $input['draw'],
		            "recordsTotal" =>  Pembelian::getAllPembelian($input,'total'),
		            "recordsFiltered" => Pembelian::getAllPembelian($input,'raw'),
		            "data" => $data,
		            );
		//output to json format
		echo json_encode($output);
    }
    function create(Request $request){
    	return view('admin.pembelian.create',['act' => 'add']);
    }
    function edit(Request $request, $id){
		$pembelian = DB::table('trans_pembelian')
        ->select('trans_pembelian.*')
        ->where('trans_pembelian.id', $id)
        ->first();
        // dd($pembelian);
    	return view('admin.pembelian.edit',compact('pembelian'))->with(['act'=>'edit']);
    }
    function update(Request $request,$id){
    	$dt_auth 	= Auth::user();
		if ($request->ajax()) {
		 // Setup the validator
			$rules = [
				'input_action'			=> 'required',
				'input_tgl_pembelian'	=> 'required',
				'input_keterangan' 		=> 'required',
			];
			$validator = Validator::make($request->all(), $rules);
			if ($validator->fails()) {
				return response()->json([
					'type' => 'error',
					'errors' => $validator->getMessageBag()->toArray()
				]);
			} else {
				if ($request->input_action=='add') {
					$id = time();
					Pembelian::create([
						'id'			=> $id,
					    'tanggal'		=> date("Y-m-d",strtotime($request->input('input_tgl_pembelian'))),
					    'total_nilai'	=> 0,
					    'total_item'	=> 0,
					    'keterangan'	=> $request->input('input_keterangan'),
					    'user_id'		=> $dt_auth->id,
					    'status'		=> 0,
					]);
					
					return response()->json(['type' => 'success', 'message' => "Successfully Created", 'id' => $id]);
				}else{
					$pembelian = Pembelian::find($id);
					$pembelian->tanggal 	= date("Y-m-d",strtotime($request->input('input_tgl_pembelian')));
					$pembelian->keterangan 	= $request->input('input_keterangan');
					$pembelian->user_id 		= $dt_auth->id;
					$pembelian->save();
	            	return response()->json(['type' => 'success', 'message' => "Successfully Updated"]);
				}
			}
		} else {
			return response()->json(['status' => 'false', 'message' => "Access only ajax request"]);
		}
    }
    function destroy(Request $request, $id){
    	if($request->ajax()){
    		$pembelian = PembelianDetail::where('pembelian_id',$id);
			$pembelian->delete();

    		$pembelian = Pembelian::find($id);
			$pembelian->delete();
        	return response()->json(['type' => 'success', 'message' => "Successfully Deleted"]);
    	} else {
			return response()->json(['status' => 'false', 'message' => "Access only ajax request"]);
		}
    }
    function editProduk(Request $request, $id){
    	if($request->ajax()){
    		$id = explode("__", $id);
    		$produk = DB::table('trans_produk')
    		->join('mst_kategori',  'mst_kategori.id', '=', 'trans_produk.kategori_id')
    		->join('mst_tipe',  'mst_tipe.id', '=', 'trans_produk.tipe_id')
    		->join('mst_satuan',  'mst_satuan.id', '=', 'trans_produk.satuan_id')
            ->select('trans_produk.*', 'mst_kategori.nama as mst_kategori_nama', 'mst_tipe.nama as mst_tipe_nama', 'mst_satuan.nama as mst_satuan_nama')
            ->where('trans_produk.id', $id[0])
            ->first();
            $pembelian = (object)[
			    'pembelian_id'  => $id[1],
			];
    		$view = View::make('admin.pembelian.editProduk',compact(['produk', 'pembelian']))->render();
    		return response()->json(['html' => $view]);
    	} else {
			return response()->json(['status' => 'false', 'message' => "Access only ajax request"]);
		}
    }

    function getValidateVarian(Request $request,$id){
    	if($request->ajax()){
    		$has_history = DB::table('histori_produk')->select('id')
            ->where('histori_produk.produk_id', $id)
            ->exists();
            $produk = Produk::find($id);
            $response['has_history'] 	= $has_history;
            $response['child'] 			= Produk::hasChild($id)->get();
            $response['stok'] 			= $produk->stok;
    		return json_encode($response);
    	} else {
			return response()->json(['status' => 'false', 'message' => "Access only ajax request"]);
		}
    }
    function createPemblianDetail(Request $request){
    	$dt_auth 	= Auth::user();
		if ($request->ajax()) {
		 // Setup the validator
			$rules = [
				'input_detail_action'		=> 'required',
				'pembelian_id'				=> 'required',
			];
			$validator = Validator::make($request->all(), $rules);
			if ($validator->fails()) {
				return response()->json([
					'type' => 'error',
					'errors' => $validator->getMessageBag()->toArray()
				]);
			} else {
				DB::beginTransaction();

				try {
				    $input = $request->all();
					$id = 'M'.time();
					foreach ($input as $key => $value) {
						if (substr($key, 0,25)=='input_jumlah_beli_produk_') {
							$idProduk 	= explode('_', $key);
							$produk 	= Produk::where('id',$idProduk[4])->first();
							$idDetail   = PembelianDetail::getID($input['pembelian_id']);
							$hasChild 	= Produk::hasChild($idProduk[4])->exists();
							$cekPembelianDuplicate = PembelianDetail::where('produk_id',$idProduk[4])->where('pembelian_id',$input['pembelian_id']);

							if (!$hasChild) {
								if (!$cekPembelianDuplicate->exists()) {
										PembelianDetail::create([
											'id'			=> $idDetail,
										    'pembelian_id'	=> $input['pembelian_id'],
										    'produk_id'		=> $idProduk[4],
										    'jumlah'		=> $value,
										    'harga'			=> $produk->harga_beli,
										    'keterangan'	=> '-',
										]);
								}else{
									self::UpdateNilai($input['pembelian_id']);
					    			DB::commit();
									return response()->json(['type' => 'error', 'message' => "Data Sudah pernah di masukan. Silahkan edit di Tab Data Pembelian"]);
								}
							}
						}
					}
					self::UpdateNilai($input['pembelian_id']);
				    DB::commit();
					return response()->json(['type' => 'success', 'message' => "Successfully Created", 'id' => $id]);

				    // all good
				} catch (\Exception $e) {
				    DB::rollback();
				    dd($e);
					return response()->json(['type' => 'error', 'message' => "Silahkan cek koneksi Anda"]);
				    // something went wrong
				}
			}
		} else {
			return response()->json(['status' => 'false', 'message' => "Access only ajax request"]);
		}
    }
    function updatePemblianDetail(Request $request,$id){
    	$dt_auth 	= Auth::user();
		if ($request->ajax()) {
		 // Setup the validator
			$rules = [
				'input_detail_action'		=> 'required',
				'pembelian_id'				=> 'required',
			];
			$validator = Validator::make($request->all(), $rules);
			if ($validator->fails()) {
				return response()->json([
					'type' => 'error',
					'errors' => $validator->getMessageBag()->toArray()
				]);
			} else {
				DB::beginTransaction();

				try {
				    $input = $request->all();
					$idBeli = explode("__", $id);
					foreach ($input as $key => $value) {
						if (substr($key, 0,25)=='input_jumlah_beli_produk_') {
							$idProduk 	= explode('_', $key);
							$produk 	= Produk::where('id',$idProduk[4])->first();
							$idDetail   = PembelianDetail::getID($input['pembelian_id']);
							$hasChild 	= Produk::hasChild($idProduk[4])->exists();

							if (!$hasChild) {
								DB::table('trans_pembelian_detail')
					            ->where('id', $idBeli[0])
					            ->where('pembelian_id', $idBeli[1])
					            ->update([
								    'jumlah'		=> $value,
								    'harga'			=> $produk->harga_beli,
								    'keterangan'	=> '-',
					            ]);
				        	}
						}
					}
					self::UpdateNilai($input['pembelian_id']);
				    DB::commit();
					return response()->json(['type' => 'success', 'message' => "Successfully Created", 'id' => $id]);

				    // all good
				} catch (\Exception $e) {
				    DB::rollback();
					return response()->json(['type' => 'error', 'message' => "Silahkan cek koneksi Anda"]);
				    // something went wrong
				}
			}
		} else {
			return response()->json(['status' => 'false', 'message' => "Access only ajax request"]);
		}
    }
    public function allPembelianDetail(Request $request){
    	$input = $request->all();
    	$dt_pembelianDetail = PembelianDetail::getAllPembelianDetail($input,'data');
        $no = isset($input['start']) ? $input['start'] : 0;
        $data = array();
        foreach($dt_pembelianDetail as $pembelianDetail){
            $no++;
            $row = array();
        	$row[] = $no;
            $row[] = $pembelianDetail->trans_produk_nama;
            $row[] = "<div class='row'><div class='col-6'>Jumlah</div><div class='col-6'>: ".$pembelianDetail->jumlah.' '.$pembelianDetail->mst_satuan_nama."</div><div class='col-6'>Harga</div><div class='col-6'>: Rp.".number_format($pembelianDetail->harga,2)."</div><div class='col-6'>SubTotal</div><div class='col-6'>: Rp.".number_format(($pembelianDetail->jumlah*$pembelianDetail->harga),2)."</div></div>";
            $row[] = "<a data-toggle='tooltip' class='col-md-3 btn btn-warning btn-md edit' id='".$pembelianDetail->id.'__'.$pembelianDetail->pembelian_id."' title='Edit'> <i class='fa fa-edit text-error'></i></a> <a data-toggle='tooltip' class='col-md-3 btn btn-danger btn-md  delete' id='".$pembelianDetail->id.'__'.$pembelianDetail->pembelian_id."' title='Delete'> <i class='fa fa-trash-alt'></i></a>";

            $data[] = $row;
        }
        $output = array(
		            "draw" => $input['draw'],
		            "recordsTotal" =>  PembelianDetail::getAllPembelianDetail($input,'total'),
		            "recordsFiltered" => PembelianDetail::getAllPembelianDetail($input,'raw'),
		            "data" => $data,
		            );
		//output to json format
		echo json_encode($output);
    }
    function editPembelian(Request $request, $id){
    	if($request->ajax()){
    		$id = explode("__", $id);
    		$produk = DB::table('trans_pembelian_detail')
    		->join('trans_produk',  'trans_produk.id', '=', 'trans_pembelian_detail.produk_id')
    		->join('mst_kategori',  'mst_kategori.id', '=', 'trans_produk.kategori_id')
    		->join('mst_tipe',  'mst_tipe.id', '=', 'trans_produk.tipe_id')
    		->join('mst_satuan',  'mst_satuan.id', '=', 'trans_produk.satuan_id')
            ->select('trans_pembelian_detail.*','trans_produk.nama','trans_produk.foto','trans_produk.stok','trans_produk.stok_min','trans_produk.is_expire','trans_produk.tgl_kadaluarsa','trans_produk.harga_beli','trans_produk.harga_jual','mst_kategori.nama as mst_kategori_nama', 'mst_tipe.nama as mst_tipe_nama', 'mst_satuan.nama as mst_satuan_nama')
            ->where('trans_pembelian_detail.pembelian_id', $id[1])
            ->where('trans_pembelian_detail.id', $id[0])
            ->first();
    		$view = View::make('admin.pembelian.editPembelian',compact(['produk']))->render();
    		return response()->json(['html' => $view]);
    	} else {
			return response()->json(['status' => 'false', 'message' => "Access only ajax request"]);
		}
    }
    function getNilai(Request $request,$id_pembelian){
		if ($request->ajax()) {
    		$pembelian = Pembelian::select('total_nilai','total_item')->where('id',$id_pembelian)->first();
			return response()->json(['total_nilai' =>number_format($pembelian->total_nilai,2),'total_item' => $pembelian->total_item]);
		} else {
			return response()->json(['status' => 'false', 'message' => "Access only ajax request"]);
		}
    }
    function UpdateNilai($pembelian_id){
			$pembelian = PembelianDetail::select(DB::raw('sum(jumlah) as total_item'),DB::raw('sum(jumlah*harga) as total_nilai'))->where('pembelian_id',$pembelian_id)->first();
			DB::table('trans_pembelian')
            ->where('id', $pembelian_id)
            ->update([
            	'total_nilai' => $pembelian->total_nilai > 0 ? $pembelian->total_nilai : 0,
            	'total_item' => $pembelian->total_item > 0 ? $pembelian->total_item : 0,
            ]);
    }
    function selesai(Request $request,$id_pembelian){
    	if ($request->ajax()) {
    		DB::beginTransaction();

			try {
    			$dt_auth 	= Auth::user();
			    $input = $request->all();
			    $pembelianDetail = PembelianDetail::where('pembelian_id',$id_pembelian)->get();
			    foreach ($pembelianDetail as $key) {
			    	$produk = Produk::where('id',$key->produk_id)->first();
			    	HistoriProduk::create([
			    		'link'			=> 'pembelian/'.$id_pembelian.'/edit',
			    		'jenis'			=> 'pembelian',
			    		'produk_id'		=> $key->produk_id,
			    		'stok_awal'		=> $produk->stok,
			    		'stok_akhir'	=> (int)$produk->stok + (int)$key->jumlah,
			    		'stok_selisih'	=> (int)$key->jumlah,
			    		'harga'			=> $key->harga,
			    		'keterangan' 	=> 'Pembelian oleh = '.$dt_auth->id
			    	]);

					$produk->stok = (int)$produk->stok + (int)$key->jumlah;
					$produk->save();
					if ($produk->sub_id!='') {
						Produk::updateStok($produk->sub_id);
					}
			    }
	    		DB::table('trans_pembelian')
	            ->where('id', $id_pembelian)
	            ->update([
	            	'status' => '1',
	            ]);
			    DB::commit();
				return response()->json(['type' => 'success', 'message' => "Successfully Updated"]);

			    // all good
			} catch (\Exception $e) {
			    DB::rollback();
			    dd($e);
			    // something went wrong
				return response()->json(['type' => 'error', 'message' => "Silahkan cek koneksi Anda"]);
			}
		} else {
			return response()->json(['status' => 'false', 'message' => "Access only ajax request"]);
		}
    }
}
