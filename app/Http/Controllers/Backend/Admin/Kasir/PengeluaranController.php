<?php

namespace App\Http\Controllers\Backend\Admin\Kasir;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
Use View;
Use DB;

use App\Models\Kasir\Pengeluaran;
use App\Models\Produk;
use App\Models\HistoriProduk;
use App\Models\Masterdata\Tipe;
use App\Models\Kasir\PengeluaranDetail;


class PengeluaranController extends Controller
{
    function index(){
    	return view('admin.pengeluaran.all');
    }
    public function allPengeluaran(Request $request){
    	$input = $request->all();
    	$dt_pengeluaran = Pengeluaran::getAllPengeluaran($input,'data');
        $no = isset($input['start']) ? $input['start'] : 0;
        $data = array();
        foreach($dt_pengeluaran as $pengeluaran){
            $no++;
            $row = array();
        	$row[] = $no;
            $row[] = date("Y-m-d",strtotime($pengeluaran->tanggal));
            $row[] = $pengeluaran->total_nilai;
            $row[] = $pengeluaran->keterangan;
            $row[] = "<a data-toggle='tooltip' class='col-md-3 btn btn-warning btn-md edit' id='".$pengeluaran->id."' title='Edit'> <i class='fa fa-edit text-error'></i></a> <a data-toggle='tooltip' class='col-md-3 btn btn-danger btn-md  ".($pengeluaran->status=='1' ? '' : 'delete')."' id='".$pengeluaran->id."' title='Delete'> <i class='fa fa-trash-alt'></i></a>";

            $data[] = $row;
        }
        $output = array(
		            "draw" => $input['draw'],
		            "recordsTotal" =>  Pengeluaran::getAllPengeluaran($input,'total'),
		            "recordsFiltered" => Pengeluaran::getAllPengeluaran($input,'raw'),
		            "data" => $data,
		            );
		//output to json format
		echo json_encode($output);
    }
    function create(Request $request){
    	return view('admin.pengeluaran.create',['act' => 'add']);
    }
    function edit(Request $request, $id){
		$pengeluaran = DB::table('trans_pengeluaran')
        ->select('trans_pengeluaran.*')
        ->where('trans_pengeluaran.id', $id)
        ->first();
        // dd($pengeluaran);
    	return view('admin.pengeluaran.edit',compact('pengeluaran'))->with(['act'=>'edit']);
    }
    function update(Request $request,$id){
    	$dt_auth 	= Auth::user();
		if ($request->ajax()) {
		 // Setup the validator
			$rules = [
				'input_action'			=> 'required',
				'input_tgl_pengeluaran'	=> 'required',
				'input_keterangan' 		=> 'required',
			];
			$validator = Validator::make($request->all(), $rules);
			if ($validator->fails()) {
				return response()->json([
					'type' => 'error',
					'errors' => $validator->getMessageBag()->toArray()
				]);
			} else {
				if ($request->input_action=='add') {
					$id = time();
					Pengeluaran::create([
						'id'			=> $id,
					    'tanggal'		=> date("Y-m-d",strtotime($request->input('input_tgl_pengeluaran'))),
					    'total_nilai'	=> 0,
					    'total_item'	=> 0,
					    'keterangan'	=> $request->input('input_keterangan'),
					    'user_id'		=> $dt_auth->id,
					    'status'		=> 0,
					]);
					
					return response()->json(['type' => 'success', 'message' => "Successfully Created", 'id' => $id]);
				}else{
					$pengeluaran = Pengeluaran::find($id);
					$pengeluaran->tanggal 	= date("Y-m-d",strtotime($request->input('input_tgl_pengeluaran')));
					$pengeluaran->keterangan 	= $request->input('input_keterangan');
					$pengeluaran->user_id 		= $dt_auth->id;
					$pengeluaran->save();
	            	return response()->json(['type' => 'success', 'message' => "Successfully Updated"]);
				}
			}
		} else {
			return response()->json(['status' => 'false', 'message' => "Access only ajax request"]);
		}
    }
    function destroy(Request $request, $id){
    	if($request->ajax()){
    		$pengeluaran = PengeluaranDetail::where('pengeluaran_id',$id);
			$pengeluaran->delete();

    		$pengeluaran = Pengeluaran::find($id);
			$pengeluaran->delete();
        	return response()->json(['type' => 'success', 'message' => "Successfully Deleted"]);
    	} else {
			return response()->json(['status' => 'false', 'message' => "Access only ajax request"]);
		}
    }
    function editProduk(Request $request, $id){
    	if($request->ajax()){
    		$id = explode("__", $id);
    		$produk = DB::table('trans_produk')
    		->join('mst_kategori',  'mst_kategori.id', '=', 'trans_produk.kategori_id')
    		->join('mst_tipe',  'mst_tipe.id', '=', 'trans_produk.tipe_id')
    		->join('mst_satuan',  'mst_satuan.id', '=', 'trans_produk.satuan_id')
            ->select('trans_produk.*', 'mst_kategori.nama as mst_kategori_nama', 'mst_tipe.nama as mst_tipe_nama', 'mst_satuan.nama as mst_satuan_nama')
            ->where('trans_produk.id', $id[0])
            ->first();
            $pengeluaran = (object)[
			    'pengeluaran_id'  => $id[1],
			];
    		$view = View::make('admin.pengeluaran.editProduk',compact(['produk', 'pengeluaran']))->render();
    		return response()->json(['html' => $view]);
    	} else {
			return response()->json(['status' => 'false', 'message' => "Access only ajax request"]);
		}
    }

    function getValidateVarian(Request $request,$id){
    	if($request->ajax()){
    		$has_history = DB::table('histori_produk')->select('id')
            ->where('histori_produk.produk_id', $id)
            ->exists();
            $produk = Produk::find($id);
            $response['has_history'] 	= $has_history;
            $response['child'] 			= Produk::hasChild($id)->get();
            $response['stok'] 			= $produk->stok;
    		return json_encode($response);
    	} else {
			return response()->json(['status' => 'false', 'message' => "Access only ajax request"]);
		}
    }
    function createPengeluaranDetail(Request $request){
    	$dt_auth 	= Auth::user();
		if ($request->ajax()) {
		 // Setup the validator
			$rules = [
				'input_detail_action'		=> 'required',
				'pengeluaran_id'				=> 'required',
			];
			$validator = Validator::make($request->all(), $rules);
			if ($validator->fails()) {
				return response()->json([
					'type' => 'error',
					'errors' => $validator->getMessageBag()->toArray()
				]);
			} else {
				DB::beginTransaction();

				try {
				    $input = $request->all();
					$id = 'M'.time();
					foreach ($input as $key => $value) {
						if (substr($key, 0,25)=='input_jumlah_beli_produk_') {
							$idProduk 	= explode('_', $key);
							$produk 	= Produk::where('id',$idProduk[4])->first();
							$idDetail   = PengeluaranDetail::getID($input['pengeluaran_id']);
							$hasChild 	= Produk::hasChild($idProduk[4])->exists();
							$cekPengeluaranDuplicate = PengeluaranDetail::where('produk_id',$idProduk[4])->where('pengeluaran_id',$input['pengeluaran_id']);
							if (!$hasChild) {
								if (!$cekPengeluaranDuplicate->exists()) {
										PengeluaranDetail::create([
											'id'			=> $idDetail,
										    'pengeluaran_id'	=> $input['pengeluaran_id'],
										    'produk_id'		=> $idProduk[4],
										    'jumlah'		=> $value,
										    'harga'			=> $produk->harga_jual,
										    'keterangan'	=> '-',
										]);
								}else{
									self::UpdateNilai($input['pengeluaran_id']);
					    			DB::commit();
									return response()->json(['type' => 'error', 'message' => "Data Sudah pernah di masukan. Silahkan edit di Tab Data Pengeluaran"]);
								}
							}
						}
					}
					self::UpdateNilai($input['pengeluaran_id']);
				    DB::commit();
					return response()->json(['type' => 'success', 'message' => "Successfully Created", 'id' => $id]);

				    // all good
				} catch (\Exception $e) {
				    DB::rollback();
				    dd($e);
					return response()->json(['type' => 'error', 'message' => "Silahkan cek koneksi Anda"]);
				    // something went wrong
				}
			}
		} else {
			return response()->json(['status' => 'false', 'message' => "Access only ajax request"]);
		}
    }
    function updatePengeluaranDetail(Request $request,$id){
    	$dt_auth 	= Auth::user();
		if ($request->ajax()) {
		 // Setup the validator
			$rules = [
				'input_detail_action'		=> 'required',
				'pengeluaran_id'				=> 'required',
			];
			$validator = Validator::make($request->all(), $rules);
			if ($validator->fails()) {
				return response()->json([
					'type' => 'error',
					'errors' => $validator->getMessageBag()->toArray()
				]);
			} else {
				DB::beginTransaction();

				try {
				    $input = $request->all();
					$idBeli = explode("__", $id);
					foreach ($input as $key => $value) {
						if (substr($key, 0,25)=='input_jumlah_beli_produk_') {
							$idProduk 	= explode('_', $key);
							$produk 	= Produk::where('id',$idProduk[4])->first();
							$idDetail   = PengeluaranDetail::getID($input['pengeluaran_id']);
							$hasChild 	= Produk::hasChild($idProduk[4])->exists();
							
							if (!$hasChild) {
								DB::table('trans_pengeluaran_detail')
					            ->where('id', $idBeli[0])
					            ->where('pengeluaran_id', $idBeli[1])
					            ->update([
								    'jumlah'		=> $value,
								    'harga'			=> $produk->harga_jual,
								    'keterangan'	=> '-',
					            ]);
				        	}
						}
					}
					self::UpdateNilai($input['pengeluaran_id']);
				    DB::commit();
					return response()->json(['type' => 'success', 'message' => "Successfully Created", 'id' => $id]);

				    // all good
				} catch (\Exception $e) {
				    DB::rollback();
					return response()->json(['type' => 'error', 'message' => "Silahkan cek koneksi Anda"]);
				    // something went wrong
				}
			}
		} else {
			return response()->json(['status' => 'false', 'message' => "Access only ajax request"]);
		}
    }
    public function allPengeluaranDetail(Request $request){
    	$input = $request->all();
    	$dt_pengeluaranDetail = PengeluaranDetail::getAllPengeluaranDetail($input,'data');
        $no = isset($input['start']) ? $input['start'] : 0;
        $data = array();
        foreach($dt_pengeluaranDetail as $pengeluaranDetail){
            $no++;
            $row = array();
        	$row[] = $no;
            $row[] = $pengeluaranDetail->trans_produk_nama;
            $row[] = "<div class='row'><div class='col-6'>Jumlah</div><div class='col-6'>: ".$pengeluaranDetail->jumlah.' '.$pengeluaranDetail->mst_satuan_nama."</div><div class='col-6'>Harga</div><div class='col-6'>: Rp.".number_format($pengeluaranDetail->harga,2)."</div><div class='col-6'>SubTotal</div><div class='col-6'>: Rp.".number_format(($pengeluaranDetail->jumlah*$pengeluaranDetail->harga),2)."</div></div>";
            $row[] = "<a data-toggle='tooltip' class='col-md-3 btn btn-warning btn-md edit' id='".$pengeluaranDetail->id.'__'.$pengeluaranDetail->pengeluaran_id."' title='Edit'> <i class='fa fa-edit text-error'></i></a> <a data-toggle='tooltip' class='col-md-3 btn btn-danger btn-md  delete' id='".$pengeluaranDetail->id.'__'.$pengeluaranDetail->pengeluaran_id."' title='Delete'> <i class='fa fa-trash-alt'></i></a>";

            $data[] = $row;
        }
        $output = array(
		            "draw" => $input['draw'],
		            "recordsTotal" =>  PengeluaranDetail::getAllPengeluaranDetail($input,'total'),
		            "recordsFiltered" => PengeluaranDetail::getAllPengeluaranDetail($input,'raw'),
		            "data" => $data,
		            );
		//output to json format
		echo json_encode($output);
    }
    function editPengeluaran(Request $request, $id){
    	if($request->ajax()){
    		$id = explode("__", $id);
    		$produk = DB::table('trans_pengeluaran_detail')
    		->join('trans_produk',  'trans_produk.id', '=', 'trans_pengeluaran_detail.produk_id')
    		->join('mst_kategori',  'mst_kategori.id', '=', 'trans_produk.kategori_id')
    		->join('mst_tipe',  'mst_tipe.id', '=', 'trans_produk.tipe_id')
    		->join('mst_satuan',  'mst_satuan.id', '=', 'trans_produk.satuan_id')
            ->select('trans_pengeluaran_detail.*','trans_produk.nama','trans_produk.foto','trans_produk.stok','trans_produk.stok_min','trans_produk.is_expire','trans_produk.tgl_kadaluarsa','trans_produk.harga_beli','trans_produk.harga_jual','mst_kategori.nama as mst_kategori_nama', 'mst_tipe.nama as mst_tipe_nama', 'mst_satuan.nama as mst_satuan_nama')
            ->where('trans_pengeluaran_detail.pengeluaran_id', $id[1])
            ->where('trans_pengeluaran_detail.id', $id[0])
            ->first();
    		$view = View::make('admin.pengeluaran.editPengeluaran',compact(['produk']))->render();
    		return response()->json(['html' => $view]);
    	} else {
			return response()->json(['status' => 'false', 'message' => "Access only ajax request"]);
		}
    }
    function getNilai(Request $request,$pengeluaran_id){
		if ($request->ajax()) {
    		$pengeluaran = Pengeluaran::select('total_nilai','total_item')->where('id',$pengeluaran_id)->first();
			return response()->json(['total_nilai' =>number_format($pengeluaran->total_nilai,2),'total_item' => $pengeluaran->total_item]);
		} else {
			return response()->json(['status' => 'false', 'message' => "Access only ajax request"]);
		}
    }
    function UpdateNilai($pengeluaran_id){
			$pengeluaran = PengeluaranDetail::select(DB::raw('sum(jumlah) as total_item'),DB::raw('sum(jumlah*harga) as total_nilai'))->where('pengeluaran_id',$pengeluaran_id)->first();
			DB::table('trans_pengeluaran')
            ->where('id', $pengeluaran_id)
            ->update([
            	'total_nilai' => $pengeluaran->total_nilai > 0 ? $pengeluaran->total_nilai : 0,
            	'total_item' => $pengeluaran->total_item > 0 ? $pengeluaran->total_item : 0,
            ]);
    }
    function selesai(Request $request,$pengeluaran_id){
    	if ($request->ajax()) {
    		DB::beginTransaction();

			try {
    			$dt_auth 	= Auth::user();
			    $input = $request->all();
			    $pengeluaranDetail = PengeluaranDetail::where('pengeluaran_id',$pengeluaran_id)->get();
			    foreach ($pengeluaranDetail as $key) {
			    	$produk = Produk::where('id',$key->produk_id)->first();
			    	if (((int)$produk->stok - (int)$key->jumlah) < 0) {
			    		DB::rollback();
						return response()->json(['type' => 'error', 'message' =>  $produk->nama." hanya memiliki stok ".$produk->stok]);
			    	}
			    	HistoriProduk::create([
			    		'link'			=> 'pengeluaran/'.$pengeluaran_id.'/edit',
			    		'jenis'			=> 'pengeluaran',
			    		'produk_id'		=> $key->produk_id,
			    		'stok_awal'		=> $produk->stok,
			    		'stok_akhir'	=> (int)$produk->stok - (int)$key->jumlah,
			    		'stok_selisih'	=> -1 * (int)$key->jumlah,
			    		'harga'			=> $key->harga,
			    		'keterangan' 	=> 'Pengeluaran oleh = '.$dt_auth->id
			    	]);

					$produk->stok = (int)$produk->stok - (int)$key->jumlah;
					$produk->save();
					if ($produk->sub_id!='') {
						Produk::updateStok($produk->sub_id);
					}
			    }
	    		DB::table('trans_pengeluaran')
	            ->where('id', $pengeluaran_id)
	            ->update([
	            	'status' => '1',
	            ]);
			    DB::commit();
				return response()->json(['type' => 'success', 'message' => "Successfully Updated"]);

			    // all good
			} catch (\Exception $e) {
			    DB::rollback();
			    // something went wrong
				return response()->json(['type' => 'error', 'message' => "Silahkan cek koneksi Anda"]);
			}
		} else {
			return response()->json(['status' => 'false', 'message' => "Access only ajax request"]);
		}
    }
    function pengaturanPengeluaran(Request $request,$id){
        $dt_tipe = Tipe::all();
        $id = (object)[
        	'tipe' => $id
        ];
        $view = View::make('admin.pengeluaran.pengaturan',compact(['dt_tipe','id']))->render();
    	return response()->json(['html' => $view]);
    }
    function savePengaturanPengeluaran(Request $request){
    	if ($request->ajax()) {
			$input = $request->all();
			$dtChecked = [];
			foreach ($input as $key => $val) {
				if ($val==1) {
					$id = explode("__", $key);
					$dtChecked[] = $id[1];
				}
			}
			DB::table('app_config')->where('id', $input['pengaturan'])
          		->update(['value' => json_encode($dtChecked)]);
			return response()->json(['type' => 'success', 'message' => "Successfully Created"]);
		} else {
			return response()->json(['status' => 'false', 'message' => "Access only ajax request"]);
		}
    }
    function getPengaturanPengeluaran(Request $request,$id){
    	$pengaturan = DB::table('app_config')
        ->where('id', $id)
        ->first();
		return response()->json(['type' => 'success', 'dt_pengeluaran' => $pengaturan]);
    }
}
