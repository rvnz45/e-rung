<?php

namespace App\Http\Controllers\Backend\Admin\Kasir;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
Use View;
Use DB;

use App\Models\Kasir\PembelianDetail;
use App\Models\Kasir\Pembelian;

class PembelianDetailController extends Controller
{
    function destroy(Request $request, $id){
    	if($request->ajax()){
    		DB::beginTransaction();

			try {
			    $id = explode("__", $id);
	    		$pembelian = PembelianDetail::where('pembelian_id',$id[1])->where('id',$id[0]);
				$pembelian->delete();

	    		$pembelian = PembelianDetail::select(DB::raw('sum(jumlah) as total_item'),DB::raw('sum(jumlah*harga) as total_nilai'))
	    		->where('pembelian_id',$id[1])->first();
				 DB::table('trans_pembelian')
	                ->where('id', $id[1])
	                ->update([
	                	'total_nilai' => $pembelian->total_nilai > 0 ? $pembelian->total_nilai : 0,
	                	'total_item' => $pembelian->total_item > 0 ? $pembelian->total_item : 0,
	                ]);
			    DB::commit();
	        	return response()->json(['type' => 'success', 'message' => "Successfully Deleted", 'total_nilai' => number_format($pembelian->total_nilai), 'total_item' => $pembelian->total_item]);

			    // all good
			} catch (\Exception $e) {
			    DB::rollback();
			    // something went wrong
				return response()->json(['type' => 'error', 'message' => "Silahkan cek koneksi Anda"]);
			}
    	} else {
			return response()->json(['status' => 'false', 'message' => "Access only ajax request"]);
		}
    }
}
