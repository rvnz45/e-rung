<?php

namespace App\Http\Controllers\Backend\Admin\Kasir;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
Use View;
Use DB;

use App\Models\Kasir\StokOpname;
use App\Models\Produk;
use App\Models\HistoriProduk;
use App\Models\Kasir\StokOpnameDetail;

class StokOpnameController extends Controller
{
    function index(){
    	return view('admin.stokOpname.all');
    }
    public function AllStokOpname(Request $request){
    	$input = $request->all();
    	$dt_stokOpname = StokOpname::getAllStokOpname($input,'data');
        $no = isset($input['start']) ? $input['start'] : 0;
        $data = array();
        foreach($dt_stokOpname as $stokOpname){
            $no++;
            $row = array();
        	$row[] = $no;
            $row[] = date("Y-m-d",strtotime($stokOpname->tanggal));
            $row[] = $stokOpname->total_nilai;
            $row[] = $stokOpname->keterangan;
            $row[] = "<a data-toggle='tooltip' class='col-md-3 btn btn-warning btn-md edit' id='".$stokOpname->id."' title='Edit'> <i class='fa fa-edit text-error'></i></a> <a data-toggle='tooltip' class='col-md-3 btn btn-danger btn-md  ".($stokOpname->status=='1' ? '' : 'delete')."' id='".$stokOpname->id."' title='Delete'> <i class='fa fa-trash-alt'></i></a>";

            $data[] = $row;
        }
        $output = array(
		            "draw" => $input['draw'],
		            "recordsTotal" =>  StokOpname::getAllStokOpname($input,'total'),
		            "recordsFiltered" => StokOpname::getAllStokOpname($input,'raw'),
		            "data" => $data,
		            );
		//output to json format
		echo json_encode($output);
    }
    function create(Request $request){
    	return view('admin.stokOpname.create',['act' => 'add']);
    }
    function edit(Request $request, $id){
		$stokOpname = DB::table('trans_stok_opname')
        ->select('trans_stok_opname.*')
        ->where('trans_stok_opname.id', $id)
        ->first();
        // dd($stokOpname);
    	return view('admin.stokOpname.edit',compact('stokOpname'))->with(['act'=>'edit']);
    }
    function update(Request $request,$id){
    	$dt_auth 	= Auth::user();
		if ($request->ajax()) {
		 // Setup the validator
			$rules = [
				'input_action'			=> 'required',
				'input_tgl_stokOpname'	=> 'required',
				'input_keterangan' 		=> 'required',
			];
			$validator = Validator::make($request->all(), $rules);
			if ($validator->fails()) {
				return response()->json([
					'type' => 'error',
					'errors' => $validator->getMessageBag()->toArray()
				]);
			} else {
				if ($request->input_action=='add') {
					$id = time();
					StokOpname::create([
						'id'			=> $id,
					    'tanggal'		=> date("Y-m-d",strtotime($request->input('input_tgl_stokOpname'))),
					    'total_nilai'	=> 0,
					    'total_item'	=> 0,
					    'keterangan'	=> $request->input('input_keterangan'),
					    'user_id'		=> $dt_auth->id,
					    'status'		=> 0,
					]);
					
					return response()->json(['type' => 'success', 'message' => "Successfully Created", 'id' => $id]);
				}else{
					$stokOpname = StokOpname::find($id);
					$stokOpname->tanggal 	= date("Y-m-d",strtotime($request->input('input_tgl_stokOpname')));
					$stokOpname->keterangan 	= $request->input('input_keterangan');
					$stokOpname->user_id 		= $dt_auth->id;
					$stokOpname->save();
	            	return response()->json(['type' => 'success', 'message' => "Successfully Updated"]);
				}
			}
		} else {
			return response()->json(['status' => 'false', 'message' => "Access only ajax request"]);
		}
    }
    function destroy(Request $request, $id){
    	if($request->ajax()){
    		$stokOpname = StokOpnameDetail::where('stok_opname_id',$id);
			$stokOpname->delete();

    		$stokOpname = StokOpname::find($id);
			$stokOpname->delete();
        	return response()->json(['type' => 'success', 'message' => "Successfully Deleted"]);
    	} else {
			return response()->json(['status' => 'false', 'message' => "Access only ajax request"]);
		}
    }
    function editProduk(Request $request, $id){
    	if($request->ajax()){
    		$id = explode("__", $id);
    		$produk = DB::table('trans_produk')
    		->join('mst_kategori',  'mst_kategori.id', '=', 'trans_produk.kategori_id')
    		->join('mst_tipe',  'mst_tipe.id', '=', 'trans_produk.tipe_id')
    		->join('mst_satuan',  'mst_satuan.id', '=', 'trans_produk.satuan_id')
            ->select('trans_produk.*', 'mst_kategori.nama as mst_kategori_nama', 'mst_tipe.nama as mst_tipe_nama', 'mst_satuan.nama as mst_satuan_nama')
            ->where('trans_produk.id', $id[0])
            ->first();
            $stokOpname = (object)[
			    'stok_opname_id'  => $id[1],
			];
    		$view = View::make('admin.stokOpname.editProduk',compact(['produk', 'stokOpname']))->render();
    		return response()->json(['html' => $view]);
    	} else {
			return response()->json(['status' => 'false', 'message' => "Access only ajax request"]);
		}
    }

    function getValidateVarian(Request $request,$id){
    	if($request->ajax()){
    		$has_history = DB::table('histori_produk')->select('id')
            ->where('histori_produk.produk_id', $id)
            ->exists();
            $produk = Produk::find($id);
            $response['has_history'] 	= $has_history;
            $response['child'] 			= Produk::hasChild($id)->get();
            $response['stok'] 			= $produk->stok;
    		return json_encode($response);
    	} else {
			return response()->json(['status' => 'false', 'message' => "Access only ajax request"]);
		}
    }
    function createStokOpnameDetail(Request $request){
    	$dt_auth 	= Auth::user();
		if ($request->ajax()) {
		 // Setup the validator
			$rules = [
				'input_detail_action'		=> 'required',
				'stok_opname_id'				=> 'required',
			];
			$validator = Validator::make($request->all(), $rules);
			if ($validator->fails()) {
				return response()->json([
					'type' => 'error',
					'errors' => $validator->getMessageBag()->toArray()
				]);
			} else {
				DB::beginTransaction();

				try {
				    $input = $request->all();
					$id = 'M'.time();
					foreach ($input as $key => $value) {
						if (substr($key, 0,25)=='input_jumlah_beli_produk_') {
							$idProduk 	= explode('_', $key);
							$produk 	= Produk::where('id',$idProduk[4])->first();
							$idDetail   = StokOpnameDetail::getID($input['stok_opname_id']);
							$hasChild 	= Produk::hasChild($idProduk[4])->exists();
							$cekStokOpnameDuplicate = StokOpnameDetail::where('produk_id',$idProduk[4])->where('stok_opname_id',$input['stok_opname_id']);
							if (!$hasChild) {
								if (!$cekStokOpnameDuplicate->exists()) {
										StokOpnameDetail::create([
											'id'			=> $idDetail,
										    'stok_opname_id'=> $input['stok_opname_id'],
										    'produk_id'		=> $idProduk[4],
										    'jumlah'		=> $produk->stok,
										    'jumlah_fisik'	=> $value,
										    'selisih'		=> $value - $produk->stok,
										    'harga'			=> $produk->harga_jual,
										    'keterangan'	=> '-',
										]);
								}else{
									self::UpdateNilai($input['stok_opname_id']);
					    			DB::commit();
									return response()->json(['type' => 'error', 'message' => "Data Sudah pernah di masukan. Silahkan edit di Tab Data Stok Opname"]);
								}
							}
						}
					}
					self::UpdateNilai($input['stok_opname_id']);
				    DB::commit();
					return response()->json(['type' => 'success', 'message' => "Successfully Created", 'id' => $id]);

				    // all good
				} catch (\Exception $e) {
				    DB::rollback();
				    dd($e);
					return response()->json(['type' => 'error', 'message' => "Silahkan cek koneksi Anda"]);
				    // something went wrong
				}
			}
		} else {
			return response()->json(['status' => 'false', 'message' => "Access only ajax request"]);
		}
    }
    function updateStokOpnameDetail(Request $request,$id){
    	$dt_auth 	= Auth::user();
		if ($request->ajax()) {
		 // Setup the validator
			$rules = [
				'input_detail_action'		=> 'required',
				'stok_opname_id'				=> 'required',
			];
			$validator = Validator::make($request->all(), $rules);
			if ($validator->fails()) {
				return response()->json([
					'type' => 'error',
					'errors' => $validator->getMessageBag()->toArray()
				]);
			} else {
				DB::beginTransaction();

				try {
				    $input = $request->all();
					$idBeli = explode("__", $id);
					foreach ($input as $key => $value) {
						if (substr($key, 0,25)=='input_jumlah_beli_produk_') {
							$idProduk 	= explode('_', $key);
							$produk 	= Produk::where('id',$idProduk[4])->first();
							$idDetail   = StokOpnameDetail::getID($input['stok_opname_id']);
							$hasChild 	= Produk::hasChild($idProduk[4])->exists();

							if (!$hasChild) {
								DB::table('trans_stok_opname_detail')
					            ->where('id', $idBeli[0])
					            ->where('stok_opname_id', $idBeli[1])
					            ->update([
								    'jumlah'		=> $produk->stok,
								    'jumlah_fisik'	=> $value,
								    'selisih'		=> $value - $produk->stok,
								    'harga'			=> $produk->harga_jual,
								    'keterangan'	=> '-',
					            ]);
				        	}
						}
					}
					self::UpdateNilai($input['stok_opname_id']);
				    DB::commit();
					return response()->json(['type' => 'success', 'message' => "Successfully Created", 'id' => $id]);

				    // all good
				} catch (\Exception $e) {
				    DB::rollback();
					return response()->json(['type' => 'error', 'message' => "Silahkan cek koneksi Anda"]);
				    // something went wrong
				}
			}
		} else {
			return response()->json(['status' => 'false', 'message' => "Access only ajax request"]);
		}
    }
    public function AllStokOpnameDetail(Request $request){
    	$input = $request->all();
    	$dt_stokOpnameDetail = StokOpnameDetail::getAllStokOpnameDetail($input,'data');
        $no = isset($input['start']) ? $input['start'] : 0;
        $data = array();
        foreach($dt_stokOpnameDetail as $stokOpnameDetail){
            $no++;
            $row = array();
        	$row[] = $no;
            $row[] = $stokOpnameDetail->trans_produk_nama;
            $row[] = "<div class='row'><div class='col-6'>Stok</div><div class='col-6'>: ".$stokOpnameDetail->jumlah.' '.$stokOpnameDetail->mst_satuan_nama."</div><div class='col-6'>Opname Fisik</div><div class='col-6'>: ".$stokOpnameDetail->jumlah_fisik.' '.$stokOpnameDetail->mst_satuan_nama."</div><div class='col-6'>Selisih</div><div class='col-6'>: ".$stokOpnameDetail->selisih.' '.$stokOpnameDetail->mst_satuan_nama."</div></div>";
            $row[] = "<a data-toggle='tooltip' class='col-md-3 btn btn-warning btn-md edit' id='".$stokOpnameDetail->id.'__'.$stokOpnameDetail->stok_opname_id."' title='Edit'> <i class='fa fa-edit text-error'></i></a> <a data-toggle='tooltip' class='col-md-3 btn btn-danger btn-md  delete' id='".$stokOpnameDetail->id.'__'.$stokOpnameDetail->stok_opname_id."' title='Delete'> <i class='fa fa-trash-alt'></i></a>";

            $data[] = $row;
        }
        $output = array(
		            "draw" => $input['draw'],
		            "recordsTotal" =>  StokOpnameDetail::getAllStokOpnameDetail($input,'total'),
		            "recordsFiltered" => StokOpnameDetail::getAllStokOpnameDetail($input,'raw'),
		            "data" => $data,
		            );
		//output to json format
		echo json_encode($output);
    }
    function editStokOpname(Request $request, $id){
    	if($request->ajax()){
    		$id = explode("__", $id);
    		$produk = DB::table('trans_stok_opname_detail')
    		->join('trans_produk',  'trans_produk.id', '=', 'trans_stok_opname_detail.produk_id')
    		->join('mst_kategori',  'mst_kategori.id', '=', 'trans_produk.kategori_id')
    		->join('mst_tipe',  'mst_tipe.id', '=', 'trans_produk.tipe_id')
    		->join('mst_satuan',  'mst_satuan.id', '=', 'trans_produk.satuan_id')
            ->select('trans_stok_opname_detail.*','trans_produk.nama','trans_produk.foto','trans_produk.stok','trans_produk.stok_min','trans_produk.is_expire','trans_produk.tgl_kadaluarsa','trans_produk.harga_beli','trans_produk.harga_jual','mst_kategori.nama as mst_kategori_nama', 'mst_tipe.nama as mst_tipe_nama', 'mst_satuan.nama as mst_satuan_nama')
            ->where('trans_stok_opname_detail.stok_opname_id', $id[1])
            ->where('trans_stok_opname_detail.id', $id[0])
            ->first();
    		$view = View::make('admin.stokOpname.editStokOpname',compact(['produk']))->render();
    		return response()->json(['html' => $view]);
    	} else {
			return response()->json(['status' => 'false', 'message' => "Access only ajax request"]);
		}
    }
    function getNilai(Request $request,$stokOpname_id){
		if ($request->ajax()) {
    		$stokOpname = StokOpname::select('total_nilai','total_item')->where('id',$stokOpname_id)->first();
			return response()->json(['total_nilai' =>number_format($stokOpname->total_nilai,2),'total_item' => $stokOpname->total_item]);
		} else {
			return response()->json(['status' => 'false', 'message' => "Access only ajax request"]);
		}
    }
    function UpdateNilai($stokOpname_id){
			$stokOpname = StokOpnameDetail::select(DB::raw('sum(jumlah) as total_item'),DB::raw('sum(jumlah*harga) as total_nilai'))->where('stok_opname_id',$stokOpname_id)->first();
			DB::table('trans_stok_opname')
            ->where('id', $stokOpname_id)
            ->update([
            	'total_nilai' => $stokOpname->total_nilai > 0 ? $stokOpname->total_nilai : 0,
            	'total_item' => $stokOpname->total_item > 0 ? $stokOpname->total_item : 0,
            ]);
    }
    function selesai(Request $request,$stokOpname_id){
    	if ($request->ajax()) {
    		DB::beginTransaction();

			try {
    			$dt_auth 	= Auth::user();
			    $input = $request->all();
			    $stokOpnameDetail = StokOpnameDetail::where('stok_opname_id',$stokOpname_id)->get();
			    foreach ($stokOpnameDetail as $key) {
			    	$produk = Produk::where('id',$key->produk_id)->first();
			    	HistoriProduk::create([
			    		'link'			=> 'stokOpname/'.$stokOpname_id.'/edit',
			    		'jenis'			=> 'stokOpname',
			    		'produk_id'		=> $key->produk_id,
			    		'stok_awal'		=> $key->jumlah,
			    		'stok_akhir'	=> (int)$key->jumlah_fisik,
			    		'stok_selisih'	=> (int)$key->selisih,
			    		'harga'			=> $key->harga,
			    		'keterangan' 	=> 'Stok Opname oleh = '.$dt_auth->id
			    	]);

					$produk->stok = (int)$key->jumlah_fisik;
					$produk->save();
					if ($produk->sub_id!='') {
						Produk::updateStok($produk->sub_id);
					}
			    }
	    		DB::table('trans_stok_opname')
	            ->where('id', $stokOpname_id)
	            ->update([
	            	'status' => '1',
	            ]);
			    DB::commit();
				return response()->json(['type' => 'success', 'message' => "Successfully Updated"]);

			    // all good
			} catch (\Exception $e) {
			    DB::rollback();
			    dd($e);
			    // something went wrong
				return response()->json(['type' => 'error', 'message' => "Silahkan cek koneksi Anda"]);
			}
		} else {
			return response()->json(['status' => 'false', 'message' => "Access only ajax request"]);
		}
    }
}
