<?php

namespace App\Http\Controllers\Backend\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
Use View;
Use DB;

use App\Models\PaketMenu;
use App\Models\PaketMenuDetail;
use App\Models\Produk;

class PaketMenuController extends Controller
{
    function index(){
    	return view('admin.paketmenu.all');
    }
    public function allPaketMenu(Request $request){
    	$input = $request->all();
    	$dt_paketMenu = PaketMenu::getAllPaketMenu($input,'data');
        $no = isset($input['start']) ? $input['start'] : 0;
        $data = array();
        foreach($dt_paketMenu as $paketMenu){
            $no++;
            $row = array();
        	$row[] = $no;
            $row[] = $paketMenu->keterangan;
            $row[] = "<a data-toggle='tooltip' class='col-md-3 btn btn-warning btn-md edit' id='".$paketMenu->id."' title='Edit'> <i class='fa fa-edit text-error'></i></a> <a data-toggle='tooltip' class='col-md-3 btn btn-danger btn-md  ".($paketMenu->status=='1' ? '' : 'delete')."' id='".$paketMenu->id."' title='Delete'> <i class='fa fa-trash-alt'></i></a>";

            $data[] = $row;
        }
        $output = array(
		            "draw" => $input['draw'],
		            "recordsTotal" =>  PaketMenu::getAllPaketMenu($input,'total'),
		            "recordsFiltered" => PaketMenu::getAllPaketMenu($input,'raw'),
		            "data" => $data,
		            );
		//output to json format
		echo json_encode($output);
    }
    function create(Request $request){
    	return view('admin.paketmenu.create',['act' => 'add']);
    }
    function edit(Request $request, $id){
		$paketMenu = DB::table('trans_paket_menu')
        ->select('trans_paket_menu.*')
        ->where('trans_paket_menu.id', $id)
        ->first();
        // dd($paketMenu);
    	return view('admin.paketmenu.edit',compact('paketMenu'))->with(['act'=>'edit']);
    }
    function update(Request $request,$id){
    	$dt_auth 	= Auth::user();
		if ($request->ajax()) {
		 // Setup the validator
			$rules = [
				'input_action'			=> 'required',
				'input_paketMenu' 		=> 'required',
			];
			$validator = Validator::make($request->all(), $rules);
			if ($validator->fails()) {
				return response()->json([
					'type' => 'error',
					'errors' => $validator->getMessageBag()->toArray()
				]);
			} else {
				if ($request->input_action=='add') {
					$id = time();
					PaketMenu::create([
						'id'			=> $id,
					    'tanggal'		=> date("Y-m-d"),
					    'total_nilai'	=> 0,
					    'total_item'	=> 0,
					    'keterangan'	=> $request->input('input_paketMenu'),
					    'user_id'		=> $dt_auth->id,
					    'status'		=> 0,
					]);
					
					return response()->json(['type' => 'success', 'message' => "Successfully Created", 'id' => $id]);
				}else{
					$paketMenu = PaketMenu::find($id);
					$paketMenu->tanggal 	= date("Y-m-d");
					$paketMenu->keterangan 	= $request->input('input_paketMenu');
					$paketMenu->user_id 		= $dt_auth->id;
					$paketMenu->save();
	            	return response()->json(['type' => 'success', 'message' => "Successfully Updated"]);
				}
			}
		} else {
			return response()->json(['status' => 'false', 'message' => "Access only ajax request"]);
		}
    }
    function destroy(Request $request, $id){
    	if($request->ajax()){
    		$paketMenu = PaketMenuDetail::where('paket_menu_id',$id);
			$paketMenu->delete();

    		$paketMenu = PaketMenu::find($id);
			$paketMenu->delete();
        	return response()->json(['type' => 'success', 'message' => "Successfully Deleted"]);
    	} else {
			return response()->json(['status' => 'false', 'message' => "Access only ajax request"]);
		}
    }
    function editProduk(Request $request, $id){
    	if($request->ajax()){
    		$id = explode("__", $id);
    		$produk = DB::table('trans_produk')
    		->join('mst_kategori',  'mst_kategori.id', '=', 'trans_produk.kategori_id')
    		->join('mst_tipe',  'mst_tipe.id', '=', 'trans_produk.tipe_id')
    		->join('mst_satuan',  'mst_satuan.id', '=', 'trans_produk.satuan_id')
            ->select('trans_produk.*', 'mst_kategori.nama as mst_kategori_nama', 'mst_tipe.nama as mst_tipe_nama', 'mst_satuan.nama as mst_satuan_nama')
            ->where('trans_produk.id', $id[0])
            ->first();
            $paketMenu = (object)[
			    'paket_menu_id'  => $id[1],
			];
    		$view = View::make('admin.paketmenu.editProduk',compact(['produk', 'paketMenu']))->render();
    		return response()->json(['html' => $view]);
    	} else {
			return response()->json(['status' => 'false', 'message' => "Access only ajax request"]);
		}
    }

    function getValidateVarian(Request $request,$id){
    	if($request->ajax()){
    		$has_history = DB::table('histori_produk')->select('id')
            ->where('histori_produk.produk_id', $id)
            ->exists();
            $produk = Produk::find($id);
            $response['has_history'] 	= $has_history;
            $response['child'] 			= Produk::hasChild($id)->get();
            $response['stok'] 			= $produk->stok;
    		return json_encode($response);
    	} else {
			return response()->json(['status' => 'false', 'message' => "Access only ajax request"]);
		}
    }
    function createPaketMenuDetail(Request $request){
    	$dt_auth 	= Auth::user();
		if ($request->ajax()) {
		 // Setup the validator
			$rules = [
				'input_detail_action'		=> 'required',
				'paket_menu_id'				=> 'required',
			];
			$validator = Validator::make($request->all(), $rules);
			if ($validator->fails()) {
				return response()->json([
					'type' => 'error',
					'errors' => $validator->getMessageBag()->toArray()
				]);
			} else {
				DB::beginTransaction();

				try {
				    $input = $request->all();
					$id = 'PKT'.time();
					foreach ($input as $key => $value) {
						if (substr($key, 0,20)=='input_jumlah_produk_') {
							$idProduk 	= explode('_', $key);
							$produk 	= Produk::where('id',$idProduk[3])->first();
							$idDetail   = PaketMenuDetail::getID($input['paket_menu_id']);
							$hasChild 	= Produk::hasChild($idProduk[3])->exists();
							$cekPaketMenuDuplicate = PaketMenuDetail::where('produk_id',$idProduk[3])->where('paket_menu_id',$input['paket_menu_id']);

							if (!$hasChild) {
								if (!$cekPaketMenuDuplicate->exists()) {
										PaketMenuDetail::create([
											'id'			=> $idDetail,
										    'paket_menu_id'	=> $input['paket_menu_id'],
										    'produk_id'		=> $idProduk[3],
										    'jumlah'		=> $value,
										    'harga'			=> $produk->harga_jual,
										    'keterangan'	=> '-',
										]);
								}else{
									self::UpdateNilai($input['paket_menu_id']);
					    			DB::commit();
									return response()->json(['type' => 'error', 'message' => "Data Sudah pernah di masukan. Silahkan edit di Tab Data Paket Menu"]);
								}
							}
						}
					}
					self::UpdateNilai($input['paket_menu_id']);
				    DB::commit();
					return response()->json(['type' => 'success', 'message' => "Successfully Created", 'id' => $id]);

				    // all good
				} catch (\Exception $e) {
				    DB::rollback();
				    dd($e);
					return response()->json(['type' => 'error', 'message' => "Silahkan cek koneksi Anda"]);
				    // something went wrong
				}
			}
		} else {
			return response()->json(['status' => 'false', 'message' => "Access only ajax request"]);
		}
    }
    function updatePaketMenuDetail(Request $request,$id){
    	$dt_auth 	= Auth::user();
		if ($request->ajax()) {
		 // Setup the validator
			$rules = [
				'input_detail_action'		=> 'required',
				'paket_menu_id'				=> 'required',
			];
			$validator = Validator::make($request->all(), $rules);
			if ($validator->fails()) {
				return response()->json([
					'type' => 'error',
					'errors' => $validator->getMessageBag()->toArray()
				]);
			} else {
				DB::beginTransaction();

				try {
				    $input = $request->all();
					$idBeli = explode("__", $id);
					foreach ($input as $key => $value) {
						if (substr($key, 0,20)=='input_jumlah_produk_') {
							$idProduk 	= explode('_', $key);
							$produk 	= Produk::where('id',$idProduk[3])->first();
							$idDetail   = PaketMenuDetail::getID($input['paket_menu_id']);
							$hasChild 	= Produk::hasChild($idProduk[3])->exists();

							if (!$hasChild) {
								DB::table('trans_paket_menu_detail')
					            ->where('id', $idBeli[0])
					            ->where('paket_menu_id', $idBeli[1])
					            ->update([
								    'jumlah'		=> $value,
								    'harga'			=> $produk->harga_jual,
								    'keterangan'	=> '-',
					            ]);
				        	}
						}
					}
					self::UpdateNilai($input['paket_menu_id']);
				    DB::commit();
					return response()->json(['type' => 'success', 'message' => "Successfully Created", 'id' => $id]);

				    // all good
				} catch (\Exception $e) {
				    DB::rollback();
					return response()->json(['type' => 'error', 'message' => "Silahkan cek koneksi Anda"]);
				    // something went wrong
				}
			}
		} else {
			return response()->json(['status' => 'false', 'message' => "Access only ajax request"]);
		}
    }
    public function allPaketMenuDetail(Request $request){
    	$input = $request->all();
    	$dt_paketMenuDetail = PaketMenuDetail::getAllPaketMenuDetail($input,'data');
        $no = isset($input['start']) ? $input['start'] : 0;
        $data = array();
        foreach($dt_paketMenuDetail as $paketMenuDetail){
            $no++;
            $row = array();
        	$row[] = $no;
            $row[] = $paketMenuDetail->trans_produk_nama;
            $row[] = "<div class='row'><div class='col-6'>Jumlah</div><div class='col-6'>: ".$paketMenuDetail->jumlah.' '.$paketMenuDetail->mst_satuan_nama."</div><div class='col-6'>Harga</div><div class='col-6'>: Rp.".number_format($paketMenuDetail->harga,2)."</div><div class='col-6'>SubTotal</div><div class='col-6'>: Rp.".number_format(($paketMenuDetail->jumlah*$paketMenuDetail->harga),2)."</div></div>";
            $row[] = "<a data-toggle='tooltip' class='col-md-3 btn btn-warning btn-md edit' id='".$paketMenuDetail->id.'__'.$paketMenuDetail->paket_menu_id."' title='Edit'> <i class='fa fa-edit text-error'></i></a> <a data-toggle='tooltip' class='col-md-3 btn btn-danger btn-md  delete' id='".$paketMenuDetail->id.'__'.$paketMenuDetail->paket_menu_id."' title='Delete'> <i class='fa fa-trash-alt'></i></a>";

            $data[] = $row;
        }
        $output = array(
		            "draw" => $input['draw'],
		            "recordsTotal" =>  PaketMenuDetail::getAllPaketMenuDetail($input,'total'),
		            "recordsFiltered" => PaketMenuDetail::getAllPaketMenuDetail($input,'raw'),
		            "data" => $data,
		            );
		//output to json format
		echo json_encode($output);
    }
    function editPaketMenu(Request $request, $id){
    	if($request->ajax()){
    		$id = explode("__", $id);
    		$produk = DB::table('trans_paket_menu_detail')
    		->join('trans_produk',  'trans_produk.id', '=', 'trans_paket_menu_detail.produk_id')
    		->join('mst_kategori',  'mst_kategori.id', '=', 'trans_produk.kategori_id')
    		->join('mst_tipe',  'mst_tipe.id', '=', 'trans_produk.tipe_id')
    		->join('mst_satuan',  'mst_satuan.id', '=', 'trans_produk.satuan_id')
            ->select('trans_paket_menu_detail.*','trans_produk.nama','trans_produk.foto','trans_produk.stok','trans_produk.stok_min','trans_produk.is_expire','trans_produk.tgl_kadaluarsa','trans_produk.harga_beli','trans_produk.harga_jual','mst_kategori.nama as mst_kategori_nama', 'mst_tipe.nama as mst_tipe_nama', 'mst_satuan.nama as mst_satuan_nama')
            ->where('trans_paket_menu_detail.paket_menu_id', $id[1])
            ->where('trans_paket_menu_detail.id', $id[0])
            ->first();
    		$view = View::make('admin.paketmenu.editPaketMenu',compact(['produk']))->render();
    		return response()->json(['html' => $view]);
    	} else {
			return response()->json(['status' => 'false', 'message' => "Access only ajax request"]);
		}
    }
    function UpdateNilai($paketMenu_id){
			$paketMenu= PaketMenuDetail::select(DB::raw('sum(jumlah) as total_item'),DB::raw('sum(jumlah*harga) as total_nilai'))->where('paket_menu_id',$paketMenu_id)->first();
			DB::table('trans_paket_menu')
            ->where('id', $paketMenu_id)
            ->update([
            	'total_nilai' => $paketMenu->total_nilai > 0 ? $paketMenu->total_nilai : 0,
            	'total_item' => $paketMenu->total_item > 0 ? $paketMenu->total_item : 0,
            ]);
    }
    function selesai(Request $request,$paket_menu_id){
    	if ($request->ajax()) {
    		DB::beginTransaction();

			try {
    			$dt_auth 	= Auth::user();
			    $input = $request->all();
			    $paketMenuDetail = PaketMenuDetail::where('paket_menu_id',$paket_menu_id)->get();
			    foreach ($paketMenuDetail as $key) {
			    	$produk = Produk::where('id',$key->produk_id)->first();

					$produk->stok = (int)$produk->stok + (int)$key->jumlah;
					$produk->save();
					if ($produk->sub_id!='') {
						Produk::updateStok($produk->sub_id);
					}
			    }
	    		DB::table('trans_paket_menu')
	            ->where('id', $paket_menu_id)
	            ->update([
	            	'status' => '1',
	            ]);
			    DB::commit();
				return response()->json(['type' => 'success', 'message' => "Successfully Updated"]);

			    // all good
			} catch (\Exception $e) {
			    DB::rollback();
			    dd($e);
			    // something went wrong
				return response()->json(['type' => 'error', 'message' => "Silahkan cek koneksi Anda"]);
			}
		} else {
			return response()->json(['status' => 'false', 'message' => "Access only ajax request"]);
		}
    }
}
