<?php

namespace App\Http\Controllers\Backend\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use App\Models\Produk;
use Illuminate\Support\Facades\Validator;
use Haruncpi\LaravelIdGenerator\IdGenerator;
Use View;
Use DB;

class ProdukDetailController extends Controller
{
    function store(Request $request){
		if ($request->ajax()) {
		 // Setup the validator
			$rules = [
				'input_detail_nama' 	=> 'required',
				'input_detail_action' 	=> 'required',
				'input_detail_stok' 	=> 'required',
				'input_detail_harga_jual' 	=> 'required',
				'input_detail_harga_beli' 	=> 'required',
				'input_detail_foto'		=> 'required',
				'input_detail_produk_id'=> 'required',
			];
			if ($request->input('input_is_expire')=='on') {
				$addRules = [
					'input_detail_tgl_kadaluarsa'	=> 'required'
				];
				$rules = array_merge($rules,$addRules);
			}
			$validator = Validator::make($request->all(), $rules);
			if ($validator->fails()) {
				return response()->json([
					'type' => 'error',
					'errors' => $validator->getMessageBag()->toArray()
				]);
			} else {
    			$dt_auth 	= Auth::user();
				$id = 'P'.time();
				$id_parent = $request->input('input_detail_produk_id');
				$produk = Produk::find($id_parent);

    			$input['image'] = $id.'_'.time().'.'.$request->input_detail_foto->extension();
        		if ($request->input_detail_foto->move(public_path('images/produk'), $input['image'])) {
        			DB::beginTransaction();
					try {
					    Produk::create([
							'id'			=> $id,
							'sub_id'		=> $produk->id,
						    'nama' 			=> ucwords(strtolower($produk->nama.' '.$request->input('input_detail_nama'))),
						    'stok' 			=> $request->input('input_detail_stok'),
						    'is_alert'		=> 1,//$request->input('input_is_alert')=='on' ? 1 : 0,
						    'stok_min' 		=> $produk->stok_min,
						    'satuan_id'		=> $produk->satuan_id,
						    'is_tracking'	=> 1,//$request->input('input_is_tracking')=='on' ? 1 : 0,
						    'harga_beli' 	=> $request->input('input_detail_harga_beli'),
						    'harga_jual' 	=> $request->input('input_detail_harga_jual'),
						    'foto' 			=> 'images/produk/'.$input['image'],
						    'diskon' 		=> $produk->diskon,
						    'total_terbeli'	=> 0,
						    'status'		=> $request->input('input_status_detail')=='on' ? 1 : 0,
						    'user_id'		=> $dt_auth->id,
						    'kategori_id'	=> $produk->kategori_id,
						    'tipe_id'		=> $produk->tipe_id,
						    'is_expire'		=> $request->input('input_is_expire')=='on' ? 1 : 0,
						    'tgl_kadaluarsa'=> $request->input('input_is_expire')=='on' ? date("Y-m-d",strtotime($request->input('input_detail_tgl_kadaluarsa'))) : NULL,
						]);
						Produk::UpdateStok($produk->id);
					    DB::commit();
						return response()->json(['type' => 'success', 'message' => "Successfully Created"]);

					    // all good
					} catch (\Exception $e) {
					    DB::rollback();
					    // something went wrong
					    return response()->json(['type' => 'error', 'message' => "Silahkan cek koneksi Anda"]);
					}
        		}else{
					return response()->json(['error'=>$validator->errors()->all()]);
        		}
			}
		} else {
			return response()->json(['status' => 'false', 'message' => "Access only ajax request"]);
		}
    }
    function edit(Request $request, $id){
    	$id = explode("__", $id);
    	if($request->ajax() && count($id) > 1){
    		$produk = DB::table('trans_produk')
    		->join('mst_kategori',  'mst_kategori.id', '=', 'trans_produk.kategori_id')
    		->join('mst_tipe',  'mst_tipe.id', '=', 'trans_produk.tipe_id')
    		->join('mst_satuan',  'mst_satuan.id', '=', 'trans_produk.satuan_id')
            ->select('trans_produk.*', 'mst_kategori.nama as mst_kategori_nama', 'mst_tipe.nama as mst_tipe_nama', 'mst_satuan.nama as mst_satuan_nama')
            ->where('trans_produk.id', $id)
            ->first();
    		return response()->json($produk);
    	} else {
			return response()->json(['status' => 'false', 'message' => "Access only ajax request"]);
		}
    }
    function update(Request $request, $id){
		if ($request->ajax()) {
		 // Setup the validator
				$rules = [
				'input_detail_nama' 	=> 'required',
				'input_detail_stok' 	=> 'required',
				'input_detail_harga_jual' 	=> 'required',
				'input_detail_harga_beli' 	=> 'required',
				'input_detail_action' 	=> 'required',
				'input_detail_produk_id'=> 'required',
				'input_detail_id'=> 'required',
			];
			if ($request->input('input_is_expire')=='on') {
				$addRules = [
					'input_detail_tgl_kadaluarsa'	=> 'required'
				];
				$rules = array_merge($rules,$addRules);
			}
			$validator = Validator::make($request->all(), $rules);
			if ($validator->fails()) {
				return response()->json([
				'type' => 'error',
				'errors' => $validator->getMessageBag()->toArray()
				]);
			} else {
				$input = $request->all();
				DB::beginTransaction();

				try {
				    if ($request->input_detail_foto!='') {
						$gambar = DB::table('trans_produk_detail')
						->where('id',$input['input_detail_id'])
						->where('produk_id',$input['input_detail_produk_id'])
						->first();
						if(file_exists(public_path($gambar->foto))){
					    	unlink(public_path($gambar->foto));
					    }
	    				$input['image'] = $input['input_detail_produk_id'].'_'.$id.'_'.time().'.'.$request->input_detail_foto->extension();
			    		$request->input_detail_foto->move(public_path('images/produk'), $input['image']);
			    		DB::table('trans_produk_detail')
		              	->where('id',$input['input_detail_id'])
						->where('produk_id',$input['input_detail_produk_id'])
		              	->update(
						    ['foto' 			=> 'images/produk/'.$input['image']],
		              	);
				    }
	    			DB::table('trans_produk')
		              	->where('id',$input['input_detail_id'])
						->where('sub_id',$input['input_detail_produk_id'])
		              	->update(
						    ['nama' 		=> $request->input('input_detail_nama'),
						    'harga_jual' 	=> $request->input('input_detail_harga_jual'),
						    'harga_beli' 	=> $request->input('input_detail_harga_beli'),
						    // 'stok' 			=> $request->input('input_detail_stok'),
						    'is_expire'		=> $request->input('input_is_expire')=='on' ? 1 : 0,
						    'tgl_kadaluarsa'=> $request->input('input_is_expire')=='on' ? date("Y-m-d",strtotime($request->input_detail_tgl_kadaluarsa)) : NULL,
						    'status'		=> $request->input('input_status_detail')=='on' ? 1 : 0,
							],
		              	);
		            Produk::UpdateStok($input['input_detail_produk_id']);
				    DB::commit();
	            	return response()->json(['type' => 'success', 'message' => "Successfully Updated"]);
				    // all good
				} catch (\Exception $e) {
				    DB::rollback();
				    // something went wrong
					return response()->json(['type' => 'error', 'message' => "Silahkan cek koneksi Anda"]);
				}
			}
    	} else {
			return response()->json(['status' => 'false', 'message' => "Access only ajax request"]);
		}
    }
    function destroy(Request $request, $id){
    	if($request->ajax()){
    		DB::beginTransaction();
			try {
			    $input = $request->all();
				$gambar = DB::table('trans_produk')
				->where('id', $id)
	    		->where('sub_id', $input['input_detail_produk_id'])
	    		->first();
	    		if(file_exists(public_path($gambar->foto))){
			    	unlink(public_path($gambar->foto));
			    }

	    		DB::table('trans_produk')
	    		->where('id', $id)
	    		->where('sub_id', $input['input_detail_produk_id'])
	    		->delete();
	    		Produk::UpdateStok($input['input_detail_produk_id'],'delete');
				DB::commit();
	        	return response()->json(['type' => 'success', 'message' => "Successfully Deleted"]);
			} catch (\Exception $e) {
			    DB::rollback();
			    dd($e);
				return response()->json(['type' => 'error', 'message' => "Silahkan cek koneksi Anda"]);
			}
    	} else {
			return response()->json(['status' => 'false', 'message' => "Access only ajax request"]);
		}
    }
    
}
