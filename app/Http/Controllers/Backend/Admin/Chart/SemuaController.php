<?php

namespace App\Http\Controllers\Backend\Admin\Chart;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Kasir;
use App\Models\Kasir\Pembelian;
use App\Models\Kasir\Pengeluaran;
use DB;

class SemuaController extends Controller
{
    public function index(){
    	$bulan = [1=>"Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember"];
    	return view('admin.chart.semua.all',compact(['bulan']));
    }
    function getChartSemuaTrans(Request $request){
    	$input = $request->all();
    	$bulan = [1=>"Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember"];
    	if ($input['filter_jenis_chart']=='tahunan') {
    		$kasir = Kasir::select(DB::raw('sum(total_asal) as total_asal'),DB::raw('sum(total_bayar) as total_bayar'),DB::raw('MONTH(tanggal) as bulan'))->where(DB::raw('YEAR(tanggal)'),$input['filter_tahun'])->groupBy(DB::raw('MONTH(tanggal)'));

    		$pembelian = Pembelian::select(DB::raw('sum(total_nilai) as total'),DB::raw('MONTH(tanggal) as bulan'))->where(DB::raw('YEAR(tanggal)'),$input['filter_tahun'])->groupBy(DB::raw('MONTH(tanggal)'));

    		$pengeluaran = Pengeluaran::select(DB::raw('sum(total_nilai) as total'),DB::raw('MONTH(tanggal) as bulan'))->where(DB::raw('YEAR(tanggal)'),$input['filter_tahun'])->groupBy(DB::raw('MONTH(tanggal)'));

    		$nilai_kasir = array();
    		$nilai_pembelian = array();
    		$nilai_pengeluaran = array();
    		foreach ($kasir->get() as $key) {
    			$nilai_kasir[(int)$key->bulan] = $key->total_bayar;
    		}
    		foreach ($pembelian->get() as $key) {
    			$nilai_pembelian[(int)$key->bulan] = $key->total;
    		}
    		foreach ($pengeluaran->get() as $key) {
    			$nilai_pengeluaran[(int)$key->bulan] = $key->total;
    		}
    		$dt_bulan = array();
    		$dt_nilai_kasir = array();
    		$dt_nilai_pembelian = array();
    		$dt_nilai_pengeluaran = array();
    		foreach ($bulan as $key => $val) {
    			$dt_bulan[] = $val;
    			$nlKasir = isset($nilai_kasir[$key]) ? $nilai_kasir[$key] : 0;
    			$nlPembelian = isset($nilai_pembelian[$key]) ? $nilai_pembelian[$key] : 0;
    			$nlPengeluaran = isset($nilai_pengeluaran[$key]) ? $nilai_pengeluaran[$key] : 0;
    			$dt_nilai_kasir[] = $nlKasir;
    			$dt_nilai_pembelian[] = $nlPembelian;
    			$dt_nilai_pengeluaran[] = $nlPengeluaran;
    		}
			return response()->json(['status' => true, 'nilai_kasir' => $dt_nilai_kasir , 'nilai_pembelian' => $dt_nilai_pembelian , 'nilai_pengeluaran' => $dt_nilai_pengeluaran , 'x_column' => $dt_bulan]);
    	}else if ($input['filter_jenis_chart']=='bulanan') {
    		$kasir = Kasir::select(DB::raw('sum(total_bayar) as total_bayar'),DB::raw('SUBSTRING(tanggal,9,2) as tanggal'))->where(DB::raw('YEAR(tanggal)'),$input['filter_tahun'])->where(DB::raw('MONTH(tanggal)'),$input['filter_bulan'])->groupBy(DB::raw('SUBSTRING(tanggal,9,2)'));

    		$pembelian = Pembelian::select(DB::raw('sum(total_nilai) as total'),DB::raw('SUBSTRING(tanggal,9,2) as tanggal'))->where(DB::raw('YEAR(tanggal)'),$input['filter_tahun'])->where(DB::raw('MONTH(tanggal)'),$input['filter_bulan'])->groupBy(DB::raw('SUBSTRING(tanggal,9,2)'));

    		$pengeluaran = Pengeluaran::select(DB::raw('sum(total_nilai) as total'),DB::raw('SUBSTRING(tanggal,9,2) as tanggal'))->where(DB::raw('YEAR(tanggal)'),$input['filter_tahun'])->where(DB::raw('MONTH(tanggal)'),$input['filter_bulan'])->groupBy(DB::raw('SUBSTRING(tanggal,9,2)'));
    		$nilai_kasir = array();
    		$nilai_pembelian = array();
    		$nilai_pengeluaran = array();
    		foreach ($kasir->get() as $key) {
    			$nilai_kasir[(int)$key->tanggal] = $key->total_bayar;
    		}
    		foreach ($pembelian->get() as $key) {
    			$nilai_pembelian[(int)$key->tanggal] = $key->total;
    		}
    		foreach ($pengeluaran->get() as $key) {
    			$nilai_pengeluaran[(int)$key->tanggal] = $key->total;
    		}
    		$dt_tanggal = array();
    		$dt_nilai_kasir = array();
    		$dt_nilai_pembelian = array();
    		$dt_nilai_pengeluaran = array();
    		$tgl = $input['filter_tahun'].'-'.sprintf('%02d', $input['filter_bulan']).'-01';
    		for($i=1;$i<=date("t",strtotime($tgl));$i++){
    			$dt_tanggal[] = sprintf('%02d', $i);
    			$nlKasir = isset($nilai_kasir[$i]) ? $nilai_kasir[$i] : 0;
    			$nlPembelian = isset($nilai_pembelian[$i]) ? $nilai_pembelian[$i] : 0;
    			$nlPengeluaran = isset($nilai_pengeluaran[$i]) ? $nilai_pengeluaran[$i] : 0;
    			$dt_nilai_kasir[] = $nlKasir;
    			$dt_nilai_pembelian[] = $nlPembelian;
    			$dt_nilai_pengeluaran[] = $nlPengeluaran;
    		}
			return response()->json(['status' => true, 'nilai_kasir' => $dt_nilai_kasir , 'nilai_pembelian' => $dt_nilai_pembelian , 'nilai_pengeluaran' => $dt_nilai_pengeluaran , 'x_column' => $dt_tanggal]);
    	}else if ($input['filter_jenis_chart']=='harian') {
    		$kasir = Kasir::select(DB::raw('sum(total_bayar) as total_bayar'),DB::raw('SUBSTRING(tanggal,15,2) as jam'))->where('tanggal','>=',date("Y-m-d",strtotime($input['filter_tanggal_a'])))->where('tanggal','<=',date("Y-m-d",strtotime($input['filter_tanggal_b'])))->groupBy(DB::raw('SUBSTRING(tanggal,15,2)'));
    		$nilai = array();
    		foreach ($kasir->get() as $key) {
    			$nilai[(int)$key->jam] = $key->total_bayar;
    		}
    		$dt_jam = array();
    		$dt_nilai = array();
    		for($i=1;$i<=24;$i++){
    			$dt_jam[] = sprintf('%02d', $i);
    			$nl = isset($nilai[$i]) ? $nilai[$i] : 0;
    			$dt_nilai[] = $nl;
    		}
			return response()->json(['status' => true, 'nilai_kasir' => $dt_nilai , 'nilai_pembelian' => [] , 'nilai_pengeluaran' => [] , 'x_column' => $dt_jam]);
    	}else{
			return response()->json(['status' => true, 'nilai' => [] , 'bulan' => []]);
    	}
    }
    function getTotalTransaksi(Request $request){
    	$input = $request->all();
    	$jenis = $input['filter_jenis_chart'];
    	if ($jenis!='') {
    		$pembelian = Pembelian::select(DB::raw('sum(total_nilai) as total'));
    		$pengeluaran = Pengeluaran::select(DB::raw('sum(total_nilai) as total'));
    		$kasir = Kasir::select(DB::raw('sum(total_bayar) as total'));
    		if ($jenis=='tahunan') {
    			$pembelian = $pembelian->where(DB::raw('YEAR(tanggal)'),$input['filter_tahun'])->first();

    			$pengeluaran = $pengeluaran->where(DB::raw('YEAR(tanggal)'),$input['filter_tahun'])->first();

    			$kasir = $kasir->where(DB::raw('YEAR(tanggal)'),$input['filter_tahun'])->first();
    		}else if ($jenis=='bulanan') {
    			$pembelian = $pembelian->where(DB::raw('YEAR(tanggal)'),$input['filter_tahun']);
    			$pembelian = $pembelian->where(DB::raw('MONTH(tanggal)'),$input['filter_bulan'])->first();

    			$pengeluaran = $pengeluaran->where(DB::raw('YEAR(tanggal)'),$input['filter_tahun']);
    			$pengeluaran = $pengeluaran->where(DB::raw('MONTH(tanggal)'),$input['filter_bulan'])->first();

    			$kasir = $kasir->where(DB::raw('YEAR(tanggal)'),$input['filter_tahun']);
    			$kasir = $kasir->where(DB::raw('MONTH(tanggal)'),$input['filter_bulan'])->first();
    		}else{
    			$pembelian = $pembelian->where('tanggal','>=',date("Y-m-d",strtotime($input['filter_tanggal_a'])));
    			$pembelian = $pembelian->where('tanggal','<=',date("Y-m-d",strtotime($input['filter_tanggal_b'])))->first();

				$pengeluaran = $pengeluaran->where('tanggal','>=',date("Y-m-d",strtotime($input['filter_tanggal_a'])));
    			$pengeluaran = $pengeluaran->where('tanggal','<=',date("Y-m-d",strtotime($input['filter_tanggal_b'])))->first();

				$kasir = $kasir->where('tanggal','>=',date("Y-m-d",strtotime($input['filter_tanggal_a'])));
    			$kasir = $kasir->where('tanggal','<=',date("Y-m-d",strtotime($input['filter_tanggal_b'])))->first();
    		}
    		$total_pembelian = $pembelian->total > 0 ? number_format($pembelian->total,2) : 0;
    		$total_pengeluaran = $pengeluaran->total > 0 ? number_format($pengeluaran->total,2) : 0;
    		$total_kasir = $kasir->total > 0 ? number_format($kasir->total,2) : 0;
			return response()->json(['status' => true, 'total_pembelian' =>  $total_pembelian, 'total_pengeluaran' => $total_pengeluaran, 'total_kasir' => $total_kasir]);
    	}
		return response()->json(['status' => true, 'total_pembelian' => 0 , 'total_pengeluaran' => 0, 'total_kasir' => 0]);
    }
}
