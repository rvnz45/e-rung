<?php

namespace App\Http\Controllers\Backend\Admin\Chart;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use App\Models\Kasir;
use App\Models\Produk;
use App\Models\HistoriProduk;

use App\Models\KasirDetail;
Use DB;
Use View;

class KasirController extends Controller
{
    public function index(){
    	$bulan = [1=>"Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember"];
    	return view('admin.chart.kasir.all',compact(['bulan']));
    }
    function getChartKasir(Request $request){
    	$input = $request->all();
    	$bulan = [1=>"Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember"];
    	$colorArray = ['#FF6633', '#FFB399', '#FF33FF', '#FFFF99', '#00B3E6', 
		  '#E6B333', '#3366E6', '#999966', '#99FF99', '#B34D4D',
		  '#80B300', '#809900', '#E6B3B3', '#6680B3', '#66991A', 
		  '#FF99E6', '#CCFF1A', '#FF1A66', '#E6331A', '#33FFCC',
		  '#66994D', '#B366CC', '#4D8000', '#B33300', '#CC80CC', 
		  '#66664D', '#991AFF', '#E666FF', '#4DB3FF', '#1AB399',
		  '#E666B3', '#33991A', '#CC9999', '#B3B31A', '#00E680', 
		  '#4D8066', '#809980', '#E6FF80', '#1AFF33', '#999933',
		  '#FF3380', '#CCCC00', '#66E64D', '#4D80CC', '#9900B3', 
		  '#E64D66', '#4DB380', '#FF4D4D', '#99E6E6', '#6666FF'];
    	if ($input['filter_jenis_chart']=='tahunan') {
    		$kasir = Kasir::select(DB::raw('sum(total_asal) as total_asal'),DB::raw('sum(total_bayar) as total_bayar'),DB::raw('MONTH(tanggal) as bulan'))->where(DB::raw('YEAR(tanggal)'),$input['filter_tahun'])->groupBy(DB::raw('MONTH(tanggal)'));
    		$nilai = array();
    		foreach ($kasir->get() as $key) {
    			$nilai[(int)$key->bulan] = $key->total_bayar - $key->total_asal;
    		}
    		$dt_bulan = array();
    		$dt_nilai = array();
    		foreach ($bulan as $key => $val) {
    			$dt_bulan[] = $val;
    			$nl = isset($nilai[$key]) ? $nilai[$key] : 0;
    			$dt_nilai[] = $nl;
    			$dt_color[] = $colorArray[$key];
    		}
			return response()->json(['status' => true, 'nilai' => $dt_nilai , 'bulan' => $dt_bulan, 'color' => $dt_color]);
    	}else if ($input['filter_jenis_chart']=='bulanan') {
    		$kasir = Kasir::select(DB::raw('sum(total_asal) as total_asal'),DB::raw('sum(total_bayar) as total_bayar'),DB::raw('SUBSTRING(tanggal,9,10) as tanggal'))->where(DB::raw('YEAR(tanggal)'),$input['filter_tahun'])->where(DB::raw('MONTH(tanggal)'),$input['filter_bulan'])->groupBy(DB::raw('SUBSTRING(tanggal,9,10)'));
    		$nilai = array();
    		foreach ($kasir->get() as $key) {
    			$nilai[(int)$key->tanggal] = $key->total_bayar - $key->total_asal;
    		}
    		$dt_tanggal = array();
    		$dt_nilai = array();
    		$tgl = $input['filter_tahun'].'-'.sprintf('%02d', $input['filter_bulan']).'-01';
    		for($i=1;$i<=date("t",strtotime($tgl));$i++){
    			$dt_tanggal[] = sprintf('%02d', $i);
    			$nl = isset($nilai[$i]) ? $nilai[$i] : 0;
    			$dt_nilai[] = $nl;
    			$dt_color[] = $colorArray[$i];
    		}
			return response()->json(['status' => true, 'nilai' => $dt_nilai , 'bulan' => $dt_tanggal, 'color' => $dt_color]);
    	}else{
			return response()->json(['status' => true, 'nilai' => [] , 'bulan' => []]);
    	}
    }
}
