<?php

namespace App\Http\Controllers\Backend\Admin\Masterdata;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Yajra\DataTables\DataTables;
use App\Models\Masterdata\Provinsi;
use Illuminate\Support\Facades\Validator;
use Haruncpi\LaravelIdGenerator\IdGenerator;
Use View;
Use DB;

class ProvinsiController extends Controller
{
    function index(){
    	return view('admin.masterdata.provinsi.all');
    }
    public function allprovinsi(Request $request){
    	$input = $request->all();
    	$dt_provinsi = Provinsi::getAllProvinsi($input,'data');
        $no = isset($input['start']) ? $input['start'] : 0;
        $data = array();
        foreach($dt_provinsi as $provinsi){
            $no++;
            $row = array();
        	$row[] = $no;
            $row[] = $provinsi->nama;
            $row[] = "<a data-toggle='tooltip' class='col-md-3 btn btn-warning btn-md edit' id='".sprintf("%02d",$provinsi->id)."' title='Edit'> <i class='fa fa-edit text-error'></i></a> <a data-toggle='tooltip' class='col-md-3 btn btn-danger btn-md  delete' id='".sprintf("%02d",$provinsi->id)."' title='Delete'> <i class='fa fa-trash-alt'></i></a>";

            $data[] = $row;
        }
        $output = array(
		            "draw" => $input['draw'],
		            "recordsTotal" =>  Provinsi::getAllProvinsi($input,'total'),
		            "recordsFiltered" => Provinsi::getAllProvinsi($input,'raw'),
		            "data" => $data,
		            );
		//output to json format
		echo json_encode($output);
    }
    function create(Request $request){
    	if ($request->ajax()) {
    		$view = View::make('admin.masterdata.provinsi.create',)->render();
    		return response()->json(['html' => $view]);
    	}else{
         	return response()->json(['status' => 'false', 'message' => "Access only ajax request"]);
    	}
    }
    function store(Request $request){
		if ($request->ajax()) {
		 // Setup the validator
			$rules = [
				'input_provinsi' 	=> 'required',
				'input_latitude' 	=> 'required',
				'input_longitude' 	=> 'required',
			];

			$validator = Validator::make($request->all(), $rules);
			if ($validator->fails()) {
				return response()->json([
				'type' => 'error',
				'errors' => $validator->getMessageBag()->toArray()
				]);
			} else {
    			$id = IdGenerator::generate(['table' => 'mst_provinsi', 'length' => 2, 'prefix' =>'0']);
				Provinsi::create([
					'id'		=> $id,
				    'nama'		=> strtoupper(strtolower($request->input('input_provinsi'))),
				    'latitude' 	=> $request->input('input_latitude'),
				    'longitude'	=> $request->input('input_longitude'),
				]);
				return response()->json(['type' => 'success', 'message' => "Successfully Created"]);
			}
		} else {
			return response()->json(['status' => 'false', 'message' => "Access only ajax request"]);
		}
    }
    function edit(Request $request, $id){
    	if($request->ajax()){
    		$provinsi = DB::table('mst_provinsi')
            ->select('mst_provinsi.*')
            ->where('id', $id)
            ->first();
            // die($provinsi);
    		$view = View::make('admin.masterdata.provinsi.edit',compact('provinsi'))->render();
    		return response()->json(['html' => $view]);
    	} else {
			return response()->json(['status' => 'false', 'message' => "Access only ajax request"]);
		}
    }

    function update(Request $request, $id){
    	if($request->ajax()){
    		$rules = [
				'input_provinsi' 	=> 'required',
				'input_latitude' 	=> 'required',
				'input_longitude' 	=> 'required',
			];

			$validator = Validator::make($request->all(), $rules);
			if ($validator->fails()) {
				return response()->json([
				'type' => 'error',
				'errors' => $validator->getMessageBag()->toArray()
				]);
			} else {
				$provinsi = Provinsi::find(sprintf("%02d",$id));
				$provinsi->nama = strtoupper(strtolower($request->input_provinsi));
				$provinsi->latitude = $request->input_latitude;
				$provinsi->longitude = $request->input_longitude;
				$provinsi->save();
            	return response()->json(['type' => 'success', 'message' => "Successfully Updated"]);
			}
    	} else {
			return response()->json(['status' => 'false', 'message' => "Access only ajax request"]);
		}
    }
    function destroy(Request $request, $id){
    	if($request->ajax()){
    		$provinsi = Provinsi::find(sprintf("%02d",$id));
			$provinsi->delete();
        	return response()->json(['type' => 'success', 'message' => "Successfully Deleted"]);
    	} else {
			return response()->json(['status' => 'false', 'message' => "Access only ajax request"]);
		}
    }
    public function autocomplete(Request $request)
    {
        $search = $request->input('search');

		if($search == ''){
		 	$dt_provinsi = Provinsi::orderby('nama','asc')->select('id','nama','latitude','longitude')->limit(5)->get();
		}else{
		 	$dt_provinsi = Provinsi::orderby('nama','asc')->select('id','nama','latitude','longitude')->where('nama', 'like', '%' .$search . '%')->limit(5)->get();
		}

		$response = array();
		foreach($dt_provinsi as $provinsi){
			$response[] = array("value"=>$provinsi->id,"label"=>$provinsi->nama,"latitude"=>$provinsi->latitude,"longitude"=>$provinsi->longitude);
		}

		return response()->json($response);
    }
}
