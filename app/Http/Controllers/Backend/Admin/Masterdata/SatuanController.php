<?php

namespace App\Http\Controllers\Backend\Admin\Masterdata;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Yajra\DataTables\DataTables;
use App\Models\Masterdata\Satuan;
use Illuminate\Support\Facades\Validator;
Use View;
Use DB;

class SatuanController extends Controller
{
    function index(){
    	return view('admin.masterdata.satuan.all');
    }
    public function allSatuan(Request $request){
    	$input = $request->all();
        $dt_satuan = Satuan::getAllSatuan($input,'data');
        $no = isset($input['start']) ? $input['start'] : 0;
        $data = array();
        foreach($dt_satuan as $satuan){
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $satuan->nama;
            $row[] = $satuan->jenis;
            $row[] = "<a data-toggle='tooltip' class='col-md-3 btn btn-warning btn-md edit' id='".$satuan->id."' title='Edit'> <i class='fa fa-edit text-error'></i></a> <a data-toggle='tooltip' class='col-md-3 btn btn-danger btn-md  delete' id='".$satuan->id."' title='Delete'> <i class='fa fa-trash-alt'></i></a>";

            $data[] = $row;
        }
        $output = array(
                    "draw" => $input['draw'],
                    "recordsTotal" =>  Satuan::getAllSatuan($input,'total'),
                    "recordsFiltered" => Satuan::getAllSatuan($input,'raw'),
                    "data" => $data,
                    );
        //output to json format
        echo json_encode($output);
    }
    function create(Request $request){
    	if ($request->ajax()) {
    		$view = View::make('admin.masterdata.satuan.create',)->render();
    		return response()->json(['html' => $view]);
    	}else{
         	return response()->json(['status' => 'false', 'message' => "Access only ajax request"]);
    	}
    }
    function store(Request $request){
		if ($request->ajax()) {
		 // Setup the validator
			$rules = [
				'input_tipe' => 'required',
				'input_satuan' => 'required'
			];

			$validator = Validator::make($request->all(), $rules);
			if ($validator->fails()) {
				return response()->json([
				'type' => 'error',
				'errors' => $validator->getMessageBag()->toArray()
				]);
			} else {
				Satuan::create([
				    'nama' => strtoupper(strtolower($request->input('input_satuan'))),
				    'jenis' => $request->input('input_tipe')
				]);
				return response()->json(['type' => 'success', 'message' => "Successfully Created"]);
			}
		} else {
			return response()->json(['status' => 'false', 'message' => "Access only ajax request"]);
		}
    }
    function edit(Request $request, $id){
    	if($request->ajax()){
    		$satuan = Satuan::findOrFail($id);
    		$view = View::make('admin.masterdata.satuan.edit',compact('satuan'))->render();
    		return response()->json(['html' => $view]);
    	} else {
			return response()->json(['status' => 'false', 'message' => "Access only ajax request"]);
		}
    }

    function update(Request $request, Satuan $satuan){
    	if($request->ajax()){
    		$rules = [
				'input_tipe' => 'required',
				'input_satuan' => 'required'
			];

			$validator = Validator::make($request->all(), $rules);
			if ($validator->fails()) {
				return response()->json([
				'type' => 'error',
				'errors' => $validator->getMessageBag()->toArray()
				]);
			} else {
				$satuan->nama = strtoupper(strtolower($request->input_satuan));
				$satuan->jenis = $request->input_tipe;
				$satuan->save();
            	return response()->json(['type' => 'success', 'message' => "Successfully Updated"]);
			}
    	} else {
			return response()->json(['status' => 'false', 'message' => "Access only ajax request"]);
		}
    }
    function destroy(Request $request, Satuan $satuan){
    	if($request->ajax()){
			$satuan->delete();
        	return response()->json(['type' => 'success', 'message' => "Successfully Deleted"]);
    	} else {
			return response()->json(['status' => 'false', 'message' => "Access only ajax request"]);
		}
    }
    public function autocomplete(Request $request)
    {
        $search = $request->input('search');

        if($search == ''){
            $temas = Satuan::orderby('nama','asc')->select('id','nama')->limit(5)->get();
        }else{
            $temas = Satuan::orderby('nama','asc')->select('id','nama')->where('nama', 'like', '%' .$search . '%')->limit(5)->get();
        }

        $response = array();
        foreach($temas as $tema){
            $response[] = array("value"=>$tema->id,"label"=>$tema->nama);
        }

        return response()->json($response);
    }
}
