<?php

namespace App\Http\Controllers\Backend\Admin\Masterdata;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Yajra\DataTables\DataTables;
use App\Models\Masterdata\Kota;
use Illuminate\Support\Facades\Validator;
use Haruncpi\LaravelIdGenerator\IdGenerator;
Use View;
Use DB;

class KotaController extends Controller
{
    function index(){
    	return view('admin.masterdata.kota.all');
    }
    public function allkota(Request $request){
    	$input = $request->all();
    	$dt_kota = Kota::getAllKota($input,'data');
        $no = isset($input['start']) ? $input['start'] : 0;
        $data = array();
        foreach($dt_kota as $kota){
            $no++;
            $row = array();
        	$row[] = $no;
            $row[] = $kota->mst_provinsi_nama;
            $row[] = $kota->nama;
            $row[] = "<a data-toggle='tooltip' class='col-md-3 btn btn-warning btn-md edit' id='".sprintf("%04d",$kota->id)."' title='Edit'> <i class='fa fa-edit text-error'></i></a> <a data-toggle='tooltip' class='col-md-3 btn btn-danger btn-md  delete' id='".sprintf("%04d",$kota->id)."' title='Delete'> <i class='fa fa-trash-alt'></i></a>";

            $data[] = $row;
        }
        $output = array(
		            "draw" => $input['draw'],
		            "recordsTotal" =>  Kota::getAllKota($input,'total'),
		            "recordsFiltered" => Kota::getAllKota($input,'raw'),
		            "data" => $data,
		            );
		//output to json format
		echo json_encode($output);
    }
    function create(Request $request){
    	if ($request->ajax()) {
    		$view = View::make('admin.masterdata.kota.create',)->render();
    		return response()->json(['html' => $view]);
    	}else{
         	return response()->json(['status' => 'false', 'message' => "Access only ajax request"]);
    	}
    }
    function store(Request $request){
		if ($request->ajax()) {
		 // Setup the validator
			$rules = [
				'input_provinsi' 	=> 'required',
				'input_provinsi_id' => 'required',
				'input_kota' 		=> 'required',
				'input_latitude' 	=> 'required',
				'input_longitude' 	=> 'required',
			];

			$validator = Validator::make($request->all(), $rules);
			if ($validator->fails()) {
				return response()->json([
				'type' => 'error',
				'errors' => $validator->getMessageBag()->toArray()
				]);
			} else {
    			$id = IdGenerator::generate(['table' => 'mst_kota', 'length' => 4, 'prefix' =>sprintf("%02d",$request->input('input_provinsi_id'))]);
				Kota::create([
					'id'		=> $id,
				    'nama'		=> strtoupper(strtolower($request->input('input_kota'))),
				    'latitude' 	=> $request->input('input_latitude'),
				    'longitude'	=> $request->input('input_longitude'),
				    'provinsi_id'=> sprintf("%02d",$request->input('input_provinsi_id')),
				]);
				return response()->json(['type' => 'success', 'message' => "Successfully Created"]);
			}
		} else {
			return response()->json(['status' => 'false', 'message' => "Access only ajax request"]);
		}
    }
    function edit(Request $request, $id){
    	if($request->ajax()){
    		$kota = DB::table('mst_kota')
            ->join('mst_provinsi',  'mst_provinsi.id', '=', 'mst_kota.provinsi_id')
            ->select('mst_kota.*', 'mst_provinsi.nama as mst_provinsi_nama')
            ->where('mst_kota.id', sprintf("%04d",$id))
            ->first();
    		$view = View::make('admin.masterdata.kota.edit',compact('kota'))->render();
    		return response()->json(['html' => $view]);
    	} else {
			return response()->json(['status' => 'false', 'message' => "Access only ajax request"]);
		}
    }

    function update(Request $request, $id){
    	if($request->ajax()){
    		$rules = [
				'input_provinsi' 	=> 'required',
				'input_provinsi_id' => 'required',
				'input_kota' 		=> 'required',
				'input_latitude' 	=> 'required',
				'input_longitude' 	=> 'required',
			];

			$validator = Validator::make($request->all(), $rules);
			if ($validator->fails()) {
				return response()->json([
				'type' => 'error',
				'errors' => $validator->getMessageBag()->toArray()
				]);
			} else {
				$kota = Kota::find(sprintf("%04d",$id));
				$kota->nama 	= strtoupper(strtolower($request->input_kota));
				$kota->latitude = $request->input_latitude;
				$kota->longitude= $request->input_longitude;
				$kota->provinsi_id = sprintf("%02d",$request->input_provinsi_id);
				$kota->save();
            	return response()->json(['type' => 'success', 'message' => "Successfully Updated"]);
			}
    	} else {
			return response()->json(['status' => 'false', 'message' => "Access only ajax request"]);
		}
    }
    function destroy(Request $request, $id){
    	if($request->ajax()){
    		$kota = Kota::find(sprintf("%04d",$id));
			$kota->delete();
        	return response()->json(['type' => 'success', 'message' => "Successfully Deleted"]);
    	} else {
			return response()->json(['status' => 'false', 'message' => "Access only ajax request"]);
		}
    }
    public function autocomplete(Request $request)
    {
        $search = $request->input('search');
        $provinsi_id = sprintf("%02d",$request->input('provinsi_id'));

		if($search == ''){
		 	$dt_kota = Kota::orderby('nama','asc')->select('id','nama','latitude','longitude')->limit(5)->get();
		}else{
		 	$dt_kota = Kota::orderby('nama','asc')->select('id','nama','latitude','longitude')->where('nama', 'like', '%' .$search . '%')->where('provinsi_id', '=', $provinsi_id )->limit(5)->get();
		}

		$response = array();
		foreach($dt_kota as $kota){
			$response[] = array("value"=>$kota->id,"label"=>$kota->nama,"longitude"=>$kota->longitude,"latitude"=>$kota->latitude);
		}

		return response()->json($response);
    }
}
