<?php

namespace App\Http\Controllers\Backend\Admin\Masterdata;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Yajra\DataTables\DataTables;
use App\Models\Masterdata\Desa;
use Illuminate\Support\Facades\Validator;
use Haruncpi\LaravelIdGenerator\IdGenerator;
Use View;
Use DB;

class DesaController extends Controller
{
    function index(){
    	return view('admin.masterdata.desa.all');
    }
    public function alldesa(Request $request){
    	$input = $request->all();
    	$dt_desa = Desa::getAllDesa($input,'data');
        $no = isset($input['start']) ? $input['start'] : 0;
        $data = array();
        foreach($dt_desa as $desa){
            $no++;
            $row = array();
        	$row[] = $no;
            $row[] = $desa->mst_kecamatan_nama;
            $row[] = $desa->nama;
            $row[] = "<a data-toggle='tooltip' class='col-md-3 btn btn-warning btn-md edit' id='".sprintf("%010d",$desa->id)."' title='Edit'> <i class='fa fa-edit text-error'></i></a> <a data-toggle='tooltip' class='col-md-3 btn btn-danger btn-md  delete' id='".sprintf("%010d",$desa->id)."' title='Delete'> <i class='fa fa-trash-alt'></i></a>";

            $data[] = $row;
        }
        $output = array(
		            "draw" => $input['draw'],
		            "recordsTotal" =>  Desa::getAllDesa($input,'total'),
		            "recordsFiltered" => Desa::getAllDesa($input,'raw'),
		            "data" => $data,
		            );
		//output to json format
		echo json_encode($output);
    }
    function create(Request $request){
    	if ($request->ajax()) {
    		$view = View::make('admin.masterdata.desa.create',)->render();
    		return response()->json(['html' => $view]);
    	}else{
         	return response()->json(['status' => 'false', 'message' => "Access only ajax request"]);
    	}
    }
    function store(Request $request){
		if ($request->ajax()) {
		 // Setup the validator
			$rules = [
				'input_provinsi' 	=> 'required',
				'input_provinsi_id' => 'required',
				'input_kota' 		=> 'required',
				'input_kota_id' 	=> 'required',
				'input_kecamatan' 	=> 'required',
				'input_kecamatan_id'=> 'required',
				'input_desa'		=> 'required',
				'input_latitude' 	=> 'required',
				'input_longitude' 	=> 'required',
			];

			$validator = Validator::make($request->all(), $rules);
			if ($validator->fails()) {
				return response()->json([
				'type' => 'error',
				'errors' => $validator->getMessageBag()->toArray()
				]);
			} else {
    			$id = IdGenerator::generate(['table' => 'mst_desa', 'length' => 10, 'prefix' =>sprintf("%07d",$request->input('input_kecamatan_id'))]);
				Desa::create([
					'id'		=> $id,
				    'nama'		=> strtoupper(strtolower($request->input('input_desa'))),
				    'latitude' 	=> $request->input('input_latitude'),
				    'longitude'	=> $request->input('input_longitude'),
				    'kecamatan_id'=> sprintf("%07d",$request->input('input_kecamatan_id')),
				]);
				return response()->json(['type' => 'success', 'message' => "Successfully Created"]);
			}
		} else {
			return response()->json(['status' => 'false', 'message' => "Access only ajax request"]);
		}
    }
    function edit(Request $request, $id){
    	if($request->ajax()){
    		$desa = DB::table('mst_desa')
            ->join('mst_kecamatan',  'mst_kecamatan.id', '=', 'mst_desa.kecamatan_id')
            ->join('mst_kota',  'mst_kota.id', '=', 'mst_kecamatan.kota_id')
            ->join('mst_provinsi',  'mst_provinsi.id', '=', 'mst_kota.provinsi_id')
            ->select('mst_desa.*', 'mst_provinsi.nama as mst_provinsi_nama','mst_provinsi.id as mst_provinsi_id', 'mst_kota.nama as mst_kota_nama','mst_kota.id as mst_kota_id', 'mst_kecamatan.nama as mst_kecamatan_nama','mst_kecamatan.id as mst_kecamatan_id')
            ->where('mst_desa.id', sprintf("%010d",$id))
            ->first();
    		$view = View::make('admin.masterdata.desa.edit',compact('desa'))->render();
    		return response()->json(['html' => $view]);
    	} else {
			return response()->json(['status' => 'false', 'message' => "Access only ajax request"]);
		}
    }

    function update(Request $request, $id){
    	if($request->ajax()){
    		$rules = [
    			'input_provinsi' 	=> 'required',
				'input_provinsi_id' => 'required',
				'input_kota' 		=> 'required',
				'input_kota_id' 	=> 'required',
				'input_kecamatan' 	=> 'required',
				'input_kecamatan_id'=> 'required',
				'input_desa'		=> 'required',
				'input_latitude' 	=> 'required',
				'input_longitude' 	=> 'required',
			];

			$validator = Validator::make($request->all(), $rules);
			if ($validator->fails()) {
				return response()->json([
				'type' => 'error',
				'errors' => $validator->getMessageBag()->toArray()
				]);
			} else {
				$desa = Desa::find(sprintf("%010d",$id));
				$desa->nama 	= strtoupper(strtolower($request->input_desa));
				$desa->latitude = $request->input_latitude;
				$desa->longitude= $request->input_longitude;
				$desa->kecamatan_id = sprintf("%07d",$request->input_kecamatan_id);
				$desa->save();
            	return response()->json(['type' => 'success', 'message' => "Successfully Updated"]);
			}
    	} else {
			return response()->json(['status' => 'false', 'message' => "Access only ajax request"]);
		}
    }
    function destroy(Request $request, $id){
    	if($request->ajax()){
    		$desa = Desa::find(sprintf("%010d",$id));
			$desa->delete();
        	return response()->json(['type' => 'success', 'message' => "Successfully Deleted"]);
    	} else {
			return response()->json(['status' => 'false', 'message' => "Access only ajax request"]);
		}
    }
    public function autocomplete(Request $request)
    {
        $search = $request->input('search');
        $kecamatan_id = sprintf("%07d",$request->input('kecamatan_id'));

		if($search == ''){
		 	$dt_desa = Desa::orderby('nama','asc')->select('id','nama','latitude','longitude')->limit(5)->get();
		}else{
		 	$dt_desa = Desa::orderby('nama','asc')->select('id','nama','latitude','longitude')->where('nama', 'like', '%' .$search . '%')->where('kecamatan_id', '=', $kecamatan_id )->limit(5)->get();
		}

		$response = array();
		foreach($dt_desa as $desa){
			$response[] = array("value"=>$desa->id,"label"=>$desa->nama,"latitude"=>$desa->latitude,"longitude"=>$desa->longitude);
		}

		return response()->json($response);
	}
}
