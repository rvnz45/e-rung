<?php

namespace App\Http\Controllers\Backend\Admin\Masterdata;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Masterdata\Kategori;
use Illuminate\Support\Facades\Validator;
Use View;
Use DB;

class KategoriController extends Controller
{
    function index(){
    	return view('admin.masterdata.kategori.all');
    }
    public function allKategori(Request $request){
    	$input = $request->all();
        $dt_kategori = Kategori::getAllKategori($input,'data');
        $no = isset($input['start']) ? $input['start'] : 0;
        $data = array();
        foreach($dt_kategori as $kategori){
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $kategori->nama;
            $row[] = "<a data-toggle='tooltip' class='col-md-3 btn btn-warning btn-md edit' id='".$kategori->id."' title='Edit'> <i class='fa fa-edit text-error'></i></a> <a data-toggle='tooltip' class='col-md-3 btn btn-danger btn-md  delete' id='".$kategori->id."' title='Delete'> <i class='fa fa-trash-alt'></i></a>";

            $data[] = $row;
        }
        $output = array(
                    "draw" => $input['draw'],
                    "recordsTotal" =>  Kategori::getAllKategori($input,'total'),
                    "recordsFiltered" => Kategori::getAllKategori($input,'raw'),
                    "data" => $data,
                    );
        //output to json format
        echo json_encode($output);
    }
    function create(Request $request){
    	if ($request->ajax()) {
    		$view = View::make('admin.masterdata.kategori.create',)->render();
    		return response()->json(['html' => $view]);
    	}else{
         	return response()->json(['status' => 'false', 'message' => "Access only ajax request"]);
    	}
    }
    function store(Request $request){
		if ($request->ajax()) {
		 // Setup the validator
			$rules = [
				'input_kategori' => 'required',
				'input_status' => 'required'
			];

			$validator = Validator::make($request->all(), $rules);
			if ($validator->fails()) {
				return response()->json([
				'type' => 'error',
				'errors' => $validator->getMessageBag()->toArray()
				]);
			} else {
                $id = Kategori::getID();
				Kategori::create([
				    'id' => $id,
				    'nama' => strtoupper(strtolower($request->input('input_kategori'))),
				    'status' => $request->input('input_status')
				]);
				return response()->json(['type' => 'success', 'message' => "Successfully Created"]);
			}
		} else {
			return response()->json(['status' => 'false', 'message' => "Access only ajax request"]);
		}
    }
    function edit(Request $request, $id){
    	if($request->ajax()){
    		$kategori = Kategori::findOrFail($id);
    		$view = View::make('admin.masterdata.kategori.edit',compact('kategori'))->render();
    		return response()->json(['html' => $view]);
    	} else {
			return response()->json(['status' => 'false', 'message' => "Access only ajax request"]);
		}
    }

    function update(Request $request, $id){
    	if($request->ajax()){
    		$rules = [
				'input_kategori' => 'required',
				'input_status' => 'required'
			];

			$validator = Validator::make($request->all(), $rules);
			if ($validator->fails()) {
				return response()->json([
				'type' => 'error',
				'errors' => $validator->getMessageBag()->toArray()
				]);
			} else {
    			$kategori = Kategori::find($id);
				$kategori->nama = strtoupper(strtolower($request->input_kategori));
				$kategori->status = $request->input_status;
				$kategori->save();
            	return response()->json(['type' => 'success', 'message' => "Successfully Updated"]);
			}
    	} else {
			return response()->json(['status' => 'false', 'message' => "Access only ajax request"]);
		}
    }
    function destroy(Request $request, $id){
    	if($request->ajax()){
    		$kategori = Kategori::find($id);
			$kategori->delete();
        	return response()->json(['type' => 'success', 'message' => "Successfully Deleted"]);
    	} else {
			return response()->json(['status' => 'false', 'message' => "Access only ajax request"]);
		}
    }
    public function autocomplete(Request $request)
    {
        $search = $request->input('search');

        if($search == ''){
            $temas = Kategori::orderby('nama','asc')->select('id','nama')->limit(5)->get();
        }else{
            $temas = Kategori::orderby('nama','asc')->select('id','nama')->where('nama', 'like', '%' .$search . '%')->limit(5)->get();
        }

        $response = array();
        foreach($temas as $tema){
            $response[] = array("value"=>$tema->id,"label"=>$tema->nama);
        }

        return response()->json($response);
    }
}
