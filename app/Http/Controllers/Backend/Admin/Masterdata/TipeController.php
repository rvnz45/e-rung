<?php

namespace App\Http\Controllers\Backend\Admin\Masterdata;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Models\Masterdata\Tipe;
Use View;
Use DB;

class TipeController extends Controller
{
    function index(){
    	return view('admin.masterdata.tipe.all');
    }
    public function allTipe(Request $request){
    	$input = $request->all();
        $dt_tipe = Tipe::getAllTipe($input,'data');
        $no = isset($input['start']) ? $input['start'] : 0;
        $data = array();
        foreach($dt_tipe as $tipe){
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $tipe->nama;
            $row[] = "<a data-toggle='tooltip' class='col-md-3 btn btn-warning btn-md edit' id='".$tipe->id."' title='Edit'> <i class='fa fa-edit text-error'></i></a> <a data-toggle='tooltip' class='col-md-3 btn btn-danger btn-md  delete' id='".$tipe->id."' title='Delete'> <i class='fa fa-trash-alt'></i></a>";

            $data[] = $row;
        }
        $output = array(
                    "draw" => $input['draw'],
                    "recordsTotal" =>  Tipe::getAllTipe($input,'total'),
                    "recordsFiltered" => Tipe::getAllTipe($input,'raw'),
                    "data" => $data,
                    );
        //output to json format
        echo json_encode($output);
    }
    function create(Request $request){
    	if ($request->ajax()) {
    		$view = View::make('admin.masterdata.tipe.create',)->render();
    		return response()->json(['html' => $view]);
    	}else{
         	return response()->json(['status' => 'false', 'message' => "Access only ajax request"]);
    	}
    }
    function store(Request $request){
		if ($request->ajax()) {
		 // Setup the validator
			$rules = [
				'input_tipe' => 'required',
				'input_status' => 'required'
			];

			$validator = Validator::make($request->all(), $rules);
			if ($validator->fails()) {
				return response()->json([
				'type' => 'error',
				'errors' => $validator->getMessageBag()->toArray()
				]);
			} else {
    			$id = Tipe::getID();
				Tipe::create([
				    'id' => $id,
				    'nama' => strtoupper(strtolower($request->input('input_tipe'))),
				    'status' => $request->input('input_status')
				]);
				return response()->json(['type' => 'success', 'message' => "Successfully Created"]);
			}
		} else {
			return response()->json(['status' => 'false', 'message' => "Access only ajax request"]);
		}
    }
    function edit(Request $request, $id){
    	if($request->ajax()){
    		$tipe = Tipe::findOrFail($id);
    		$view = View::make('admin.masterdata.tipe.edit',compact('tipe'))->render();
    		return response()->json(['html' => $view]);
    	} else {
			return response()->json(['status' => 'false', 'message' => "Access only ajax request"]);
		}
    }

    function update(Request $request, $id){
    	if($request->ajax()){
    		$rules = [
				'input_tipe' => 'required',
				'input_status' => 'required'
			];

			$validator = Validator::make($request->all(), $rules);
			if ($validator->fails()) {
				return response()->json([
				'type' => 'error',
				'errors' => $validator->getMessageBag()->toArray()
				]);
			} else {
    			$tipe = Tipe::find($id);
				$tipe->nama = strtoupper(strtolower($request->input_tipe));
				$tipe->status = $request->input_status;
				$tipe->save();
            	return response()->json(['type' => 'success', 'message' => "Successfully Updated"]);
			}
    	} else {
			return response()->json(['status' => 'false', 'message' => "Access only ajax request"]);
		}
    }
    function destroy(Request $request, $id){
    	if($request->ajax()){
    		$tipe = Tipe::find($id);
			$tipe->delete();
        	return response()->json(['type' => 'success', 'message' => "Successfully Deleted"]);
    	} else {
			return response()->json(['status' => 'false', 'message' => "Access only ajax request"]);
		}
    }
    public function autocomplete(Request $request)
    {
        $search = $request->input('search');

        if($search == ''){
            $temas = Tipe::orderby('nama','asc')->select('id','nama')->limit(5)->get();
        }else{
            $temas = Tipe::orderby('nama','asc')->select('id','nama')->where('nama', 'like', '%' .$search . '%')->limit(5)->get();
        }

        $response = array();
        foreach($temas as $tema){
            $response[] = array("value"=>$tema->id,"label"=>$tema->nama);
        }

        return response()->json($response);
    }
}
