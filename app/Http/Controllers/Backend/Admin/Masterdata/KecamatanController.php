<?php

namespace App\Http\Controllers\Backend\Admin\Masterdata;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Yajra\DataTables\DataTables;
use App\Models\Masterdata\Kecamatan;
use Illuminate\Support\Facades\Validator;
use Haruncpi\LaravelIdGenerator\IdGenerator;
Use View;
Use DB;

class KecamatanController extends Controller
{
    function index(){
    	return view('admin.masterdata.kecamatan.all');
    }
    public function allkecamatan(Request $request){
    	$input = $request->all();
    	$dt_kecamatan = Kecamatan::getAllKecamatan($input,'data');
        $no = isset($input['start']) ? $input['start'] : 0;
        $data = array();
        foreach($dt_kecamatan as $kecamatan){
            $no++;
            $row = array();
        	$row[] = $no;
            $row[] = $kecamatan->mst_kota_nama;
            $row[] = $kecamatan->nama;
            $row[] = "<a data-toggle='tooltip' class='col-md-3 btn btn-warning btn-md edit' id='".sprintf("%07d",$kecamatan->id)."' title='Edit'> <i class='fa fa-edit text-error'></i></a> <a data-toggle='tooltip' class='col-md-3 btn btn-danger btn-md  delete' id='".sprintf("%07d",$kecamatan->id)."' title='Delete'> <i class='fa fa-trash-alt'></i></a>";

            $data[] = $row;
        }
        $output = array(
		            "draw" => $input['draw'],
		            "recordsTotal" =>  Kecamatan::getAllKecamatan($input,'total'),
		            "recordsFiltered" => Kecamatan::getAllKecamatan($input,'raw'),
		            "data" => $data,
		            );
		//output to json format
		echo json_encode($output);
    }
    function create(Request $request){
    	if ($request->ajax()) {
    		$view = View::make('admin.masterdata.kecamatan.create',)->render();
    		return response()->json(['html' => $view]);
    	}else{
         	return response()->json(['status' => 'false', 'message' => "Access only ajax request"]);
    	}
    }
    function store(Request $request){
		if ($request->ajax()) {
		 // Setup the validator
			$rules = [
				'input_provinsi' 	=> 'required',
				'input_provinsi_id' => 'required',
				'input_kota' 		=> 'required',
				'input_kota_id' 	=> 'required',
				'input_kecamatan'	=> 'required',
				'input_latitude' 	=> 'required',
				'input_longitude' 	=> 'required',
			];

			$validator = Validator::make($request->all(), $rules);
			if ($validator->fails()) {
				return response()->json([
				'type' => 'error',
				'errors' => $validator->getMessageBag()->toArray()
				]);
			} else {
    			$id = IdGenerator::generate(['table' => 'mst_kecamatan', 'length' => 7, 'prefix' =>sprintf("%04d",$request->input('input_kota_id'))]);
				Kecamatan::create([
					'id'		=> $id,
				    'nama'		=> strtoupper(strtolower($request->input('input_kecamatan'))),
				    'latitude' 	=> $request->input('input_latitude'),
				    'longitude'	=> $request->input('input_longitude'),
				    'kota_id'=> sprintf("%04d",$request->input('input_kota_id')),
				]);
				return response()->json(['type' => 'success', 'message' => "Successfully Created"]);
			}
		} else {
			return response()->json(['status' => 'false', 'message' => "Access only ajax request"]);
		}
    }
    function edit(Request $request, $id){
    	if($request->ajax()){
    		$kecamatan = DB::table('mst_kecamatan')
            ->join('mst_kota',  'mst_kota.id', '=', 'mst_kecamatan.kota_id')
            ->join('mst_provinsi',  'mst_provinsi.id', '=', 'mst_kota.provinsi_id')
            ->select('mst_kecamatan.*', 'mst_provinsi.nama as mst_provinsi_nama','mst_provinsi.id as mst_provinsi_id','mst_kota.nama as mst_kota_nama')
            ->where('mst_kecamatan.id', sprintf("%07d",$id))
            ->first();
    		$view = View::make('admin.masterdata.kecamatan.edit',compact('kecamatan'))->render();
    		return response()->json(['html' => $view]);
    	} else {
			return response()->json(['status' => 'false', 'message' => "Access only ajax request"]);
		}
    }

    function update(Request $request, $id){
    	if($request->ajax()){
    		$rules = [
    			'input_provinsi' 	=> 'required',
				'input_provinsi_id' => 'required',
				'input_kota' 		=> 'required',
				'input_kota_id' 	=> 'required',
				'input_kecamatan' 	=> 'required',
				'input_latitude' 	=> 'required',
				'input_longitude' 	=> 'required',
			];

			$validator = Validator::make($request->all(), $rules);
			if ($validator->fails()) {
				return response()->json([
				'type' => 'error',
				'errors' => $validator->getMessageBag()->toArray()
				]);
			} else {
				$kecamatan = Kecamatan::find(sprintf("%07d",$id));
				$kecamatan->nama 	= strtoupper(strtolower($request->input_kecamatan));
				$kecamatan->latitude = $request->input_latitude;
				$kecamatan->longitude= $request->input_longitude;
				$kecamatan->kota_id = sprintf("%04d",$request->input_kota_id);
				$kecamatan->save();
            	return response()->json(['type' => 'success', 'message' => "Successfully Updated"]);
			}
    	} else {
			return response()->json(['status' => 'false', 'message' => "Access only ajax request"]);
		}
    }
    function destroy(Request $request, $id){
    	if($request->ajax()){
    		$kecamatan = Kecamatan::find(sprintf("%07d",$id));
			$kecamatan->delete();
        	return response()->json(['type' => 'success', 'message' => "Successfully Deleted"]);
    	} else {
			return response()->json(['status' => 'false', 'message' => "Access only ajax request"]);
		}
    }
    public function autocomplete(Request $request)
    {
        $search = $request->input('search');
        $kota_id = sprintf("%04d",$request->input('kota_id'));

		if($search == ''){
		 	$dt_kecamatan = Kecamatan::orderby('nama','asc')->select('id','nama','latitude','longitude')->limit(5)->get();
		}else{
		 	$dt_kecamatan = Kecamatan::orderby('nama','asc')->select('id','nama','latitude','longitude')->where('nama', 'like', '%' .$search . '%')->where('kota_id', '=', $kota_id )->limit(5)->get();
		}

		$response = array();
		foreach($dt_kecamatan as $kecamatan){
			$response[] = array("value"=>$kecamatan->id,"label"=>$kecamatan->nama,"latitude"=>$kecamatan->latitude,"longitude"=>$kecamatan->longitude);
		}

		return response()->json($response);
    }
}
