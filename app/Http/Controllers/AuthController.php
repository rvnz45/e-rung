<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Validator,Redirect,Response;
Use App\User;
use App\Models\App_config;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Session;

class AuthController extends Controller
{
    public function index()
    {
        $data = array();
        foreach (App_config::all() as $setting){
            $data[$setting->id] = $setting->value;
        }
        return view('auth.login')->with('app_config', $data);
    }  
    public function postLogin(Request $request)
    {
    	$input = $request->all();
        request()->validate([
        'username' => 'required',
        'password' => 'required',
        ]);
 		$fieldType = filter_var($request->username, FILTER_VALIDATE_EMAIL) ? 'email' : 'phone_number';
        if(auth()->attempt(array(''.$fieldType => $input['username'], 'password' => $input['password'])))
        {
            return redirect()->intended('/admin_bqs/dashboard');
        }
        $data = array();
        foreach (App_config::all() as $setting){
            $data[$setting->id] = $setting->value;
        }
         return redirect('/admin_bqs/loginxxx')->withSuccess('Oppes! You have entered invalid credentials');
    }
    public function logout() {
        Session::flush();
        Auth::logout();

        return Redirect('admin_bqs/loginxxx');
    }
}
