<?php

use Illuminate\Support\Facades\Route;
use App\Models\App_config;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
	$data = array();
    foreach (App_config::all() as $setting){
        $data[$setting->id] = $setting->value;
    }
    return view('welcome')->with('app_config', $data);
});
Auth::routes([
	'login' => false,
	'register' => false,
	'reset' => false,
	'logout' => false,
	'confirm' => false
]);
Route::group([
	'namespace'	=> 'Backend\Admin',
	'prefix'	=> 'admin_bqs',
	'as'		=> 'admin.',
	'middleware'=>'auth'],
	function(){
		require base_path('routes/backend/admin.php');
	}
);
Route::get('/admin_bqs/loginxxx', 'AuthController@index')->name('login');
Route::post('post-login', 'AuthController@postLogin'); 
Route::get('/logout', 'AuthController@logout')->name('logout');

Route::get('/home', 'HomeController@index')->name('home');
