<?php
Route::get('/dashboard', 'DashboardController@index')->name('dashboard');

Route::resource('users','UserController');
Route::get('/allUser','UserController@allUser')->name('allUser.users');
Route::post('autocompleteUsers', 'UserController@autocomplete')->name('autocompleteUsers.users');
Route::get('/export','UserController@export')->name('export');

Route::resource('level','LevelController');
Route::get('/allLevel','LevelController@allLevel')->name('allLevel.level');
Route::post('autocompleteLevel', 'LevelController@autocomplete')->name('autocompleteLevel.level');

Route::resource('/satuan','Masterdata\SatuanController');
Route::get('/allSatuan','Masterdata\SatuanController@allSatuan')->name('allSatuan.satuan');
Route::post('autocompleteSatuan', 'Masterdata\SatuanController@autocomplete')->name('autocompleteSatuan.satuan');

Route::resource('/kategori','Masterdata\KategoriController');
Route::get('/allKategori','Masterdata\KategoriController@allKategori')->name('allKategori.kategori');
Route::post('autocompleteKategori', 'Masterdata\KategoriController@autocomplete')->name('autocompleteKategori.kategori');

Route::resource('/tipe','Masterdata\TipeController');
Route::get('/allTipe','Masterdata\TipeController@allTipe')->name('allTipe.tipe');
Route::post('autocompleteTipe', 'Masterdata\TipeController@autocomplete')->name('autocompleteTipe.tipe');

Route::resource('/provinsi','Masterdata\ProvinsiController');
Route::get('/allProvinsi','Masterdata\ProvinsiController@allProvinsi')->name('allProvinsi.provinsi');
Route::post('autocompleteProvinsi', 'Masterdata\ProvinsiController@autocomplete')->name('autocompleteProvinsi.provinsi');

Route::resource('/kota','Masterdata\KotaController');
Route::get('/allKota','Masterdata\KotaController@allKota')->name('allKota.kota');
Route::post('autocompleteKota', 'Masterdata\KotaController@autocomplete')->name('autocompleteKota.kota');

Route::resource('/kecamatan','Masterdata\KecamatanController');
Route::get('/allKecamatan','Masterdata\KecamatanController@allKecamatan')->name('allKecamatan.kecamatan');
Route::post('autocompleteKecamatan', 'Masterdata\KecamatanController@autocomplete')->name('autocompleteKecamatan.kecamatan');

Route::resource('/desa','Masterdata\DesaController');
Route::get('/allDesa','Masterdata\DesaController@allDesa')->name('allDesa.desa');
Route::post('autocompleteDesa', 'Masterdata\DesaController@autocomplete')->name('autocompleteDesa.desa');

Route::resource('/produk','ProdukController');
Route::get('/allProduk','ProdukController@allProduk')->name('allProduk.produk');
Route::resource('/produkDetail','ProdukDetailController');

Route::resource('/pembelian','Kasir\PembelianController');
Route::get('/allPembelian','Kasir\PembelianController@allPembelian')->name('allPembelian.pembelian');
Route::get('/pembelian/editProduk/{id}','Kasir\PembelianController@editProduk')->name('editProduk.pembelian');
Route::get('/pembelian/editPembelian/{id}','Kasir\PembelianController@editPembelian')->name('editPembelian.pembelian');
Route::get('/pembelian/getValidateVarian/{id}','Kasir\PembelianController@getValidateVarian')->name('getValidateVarian.pembelian');
Route::get('/pembelian/getNilai/{id}','Kasir\PembelianController@getNilai')->name('getNilai.pembelian');
Route::post('/pembelian/createPemblianDetail','Kasir\PembelianController@createPemblianDetail')->name('createPemblianDetail.pembelian');
Route::post('/pembelian/updatePemblianDetail/{id}','Kasir\PembelianController@updatePemblianDetail')->name('updatePemblianDetail.pembelian');
Route::post('/pembelian/selesai/{id}','Kasir\PembelianController@selesai')->name('selesai.pembelian');

Route::resource('/pembelian/pembelianDetail','Kasir\PembelianDetailController');
Route::get('/allPembelianDetail','Kasir\PembelianController@allPembelianDetail')->name('allPembelianDetail.pembelianDetail');


Route::resource('/pengeluaran','Kasir\PengeluaranController');
Route::get('/allPengeluaran','Kasir\PengeluaranController@allPengeluaran')->name('allPengeluaran.pengeluaran');
Route::get('/pengeluaran/editProduk/{id}','Kasir\PengeluaranController@editProduk')->name('editProduk.pengeluaran');
Route::get('/pengeluaran/editPengeluaran/{id}','Kasir\PengeluaranController@editPengeluaran')->name('editPengeluaran.pengeluaran');
Route::get('/pengeluaran/getValidateVarian/{id}','Kasir\PengeluaranController@getValidateVarian')->name('getValidateVarian.pengeluaran');
Route::get('/pengeluaran/getNilai/{id}','Kasir\PengeluaranController@getNilai')->name('getNilai.pengeluaran');
Route::post('/pengeluaran/createPengeluaranDetail','Kasir\PengeluaranController@createPengeluaranDetail')->name('createPengeluaranDetail.pengeluaran');
Route::post('/pengeluaran/updatePengeluaranDetail/{id}','Kasir\PengeluaranController@updatePengeluaranDetail')->name('updatePengeluaranDetail.pengeluaran');
Route::post('/pengeluaran/selesai/{id}','Kasir\PengeluaranController@selesai')->name('selesai.pengeluaran');
Route::get('/pengaturanPengeluaran/{id}','Kasir\PengeluaranController@pengaturanPengeluaran')->name('pengaturanPengeluaran.pengeluaran');
Route::post('/savePengaturanPengeluaran','Kasir\PengeluaranController@savePengaturanPengeluaran')->name('savePengaturanPengeluaran.pengeluaran');
Route::get('getPengaturanPengeluaran/{id}','Kasir\PengeluaranController@getPengaturanPengeluaran')->name('getPengaturanPengeluaran.pengeluaran');

Route::resource('/pengeluaran/pengeluaranDetail','Kasir\PengeluaranDetailController');
Route::get('/allPengeluaranDetail','Kasir\PengeluaranController@allPengeluaranDetail')->name('allPengeluaranDetail.pengeluaranDetail');

Route::resource('/paketMenu','PaketMenuController');
Route::get('/allPaketMenu','PaketMenuController@allPaketMenu')->name('allPaketMenu.paketMenu');
Route::get('/paketMenu/editProduk/{id}','PaketMenuController@editProduk')->name('editProduk.paketMenu');
Route::get('/paketMenu/editPaketMenu/{id}','PaketMenuController@editPaketMenu')->name('editPaketMenu.paketMenu');
Route::get('/paketMenu/getValidateVarian/{id}','PaketMenuController@getValidateVarian')->name('getValidateVarian.paketMenu');
Route::post('/paketMenu/createPaketMenuDetail','PaketMenuController@createPaketMenuDetail')->name('createPaketMenuDetail.paketMenu');
Route::post('/paketMenu/updatePaketMenuDetail/{id}','PaketMenuController@updatePaketMenuDetail')->name('updatePaketMenuDetail.paketMenu');
Route::post('/paketMenu/selesai/{id}','PaketMenuController@selesai')->name('selesai.paketMenu');

Route::resource('/paketMenu/paketMenuDetail','PaketMenuDetailController');
Route::get('/allPaketMenuDetail','PaketMenuController@allPaketMenuDetail')->name('allPaketMenuDetail.paketMenuDetail');




Route::resource('/stokOpname','Kasir\StokOpnameController');
Route::get('/allStokOpname','Kasir\StokOpnameController@allStokOpname')->name('allStokOpname.stokOpname');
Route::get('/stokOpname/editProduk/{id}','Kasir\StokOpnameController@editProduk')->name('editProduk.stokOpname');
Route::get('/stokOpname/editStokOpname/{id}','Kasir\StokOpnameController@editStokOpname')->name('editStokOpname.stokOpname');
Route::get('/stokOpname/getValidateVarian/{id}','Kasir\StokOpnameController@getValidateVarian')->name('getValidateVarian.stokOpname');
Route::get('/stokOpname/getNilai/{id}','Kasir\StokOpnameController@getNilai')->name('getNilai.stokOpname');
Route::post('/stokOpname/createStokOpnameDetail','Kasir\StokOpnameController@createStokOpnameDetail')->name('createStokOpnameDetail.stokOpname');
Route::post('/stokOpname/updateStokOpnameDetail/{id}','Kasir\StokOpnameController@updateStokOpnameDetail')->name('updateStokOpnameDetail.stokOpname');
Route::post('/stokOpname/selesai/{id}','Kasir\StokOpnameController@selesai')->name('selesai.stokOpname');

Route::resource('/stokOpname/stokOpnameDetail','Kasir\StokOpnameDetailController');
Route::get('/allStokOpnameDetail','Kasir\StokOpnameController@allStokOpnameDetail')->name('allStokOpnameDetail.stokOpnameDetail');

Route::resource('/kasir','KasirController');
Route::get('/allKasir','KasirController@allKasir')->name('allKasir.kasir');
Route::get('/allKeranjang','KasirController@allKeranjang')->name('allKeranjang.kasir');
Route::get('/allProdukKasir','KasirController@allProduk')->name('allProdukKasir.kasir');
Route::post('/cekKasirAbsen','KasirController@cekKasirAbsen')->name('cekKasirAbsen.kasir');
Route::post('/bukaKasir','KasirController@bukaKasir')->name('bukaKasir.kasir');
Route::get('/pembelianProduk/{id}','KasirController@pembelianProduk')->name('pembelianProduk.kasir');
Route::get('/keranjangProduk/{id}','KasirController@keranjangProduk')->name('keranjangProduk.kasir');
Route::post('/createPembelian','KasirController@createPembelian')->name('createPembelian.kasir');
Route::get('/getKeranjang','KasirController@getKeranjang')->name('getKeranjang.kasir');
Route::post('/checkOut','KasirController@checkOut')->name('checkOut.kasir');
Route::get('/hitungKasir','KasirController@hitungKasir')->name('hitungKasir.kasir');
Route::post('/tutupKasir','KasirController@tutupKasir')->name('tutupKasir.kasir');
Route::get('/checkOutHp','KasirController@checkOutHp')->name('checkOutHp.kasir');
Route::get('/allPaket','KasirController@allPaket')->name('allPaket.kasir');
Route::get('/pembelianPaket/{id}','KasirController@pembelianPaket')->name('pembelianPaket.kasir');
Route::post('/createPembelianPaket','KasirController@createPembelianPaket')->name('createPembelianPaket.kasir');
Route::post('/print_pembelian','KasirController@print_pembelian')->name('print_pembelian.kasir');



Route::resource('laporan/kasir','Laporan\KasirController');
Route::get('/allLaporanKasir','Laporan\KasirController@allLaporanKasir')->name('allLaporanKasir.laporan');
Route::get('/allLaporanKasirDetail','Laporan\KasirController@allLaporanKasirDetail')->name('allLaporanKasirDetail.laporan');
Route::get('/laporan/kasir/keranjangProduk/{id}','Laporan\KasirController@keranjangProduk')->name('keranjangProduk.laporan');
Route::post('updatePembelian','Laporan\KasirController@updatePembelian')->name('updatePembelian.laporan');
Route::get('/laporan/kasir/getKeranjangUpdate/{id}','Laporan\KasirController@getKeranjangUpdate')->name('getKeranjangUpdate.laporan');
Route::post('laporan/kasir/updateCheckOut/{id}','Laporan\KasirController@updateCheckOut')->name('updateCheckOut.laporan');
Route::post('getTotalKasir','Laporan\KasirController@getTotalKasir')->name('getTotalKasir.laporan');


Route::resource('laporan/produk','Laporan\ProdukController');
Route::get('/allLaporanProduk','Laporan\ProdukController@allLaporanProduk')->name('allLaporanProduk.laporan');
Route::post('getTotalKasirDetail','Laporan\ProdukController@getTotalKasirDetail')->name('getTotalKasirDetail.laporan');

Route::resource('chart/kasir','Chart\KasirController');
Route::post('/getChartKasir','Chart\KasirController@getChartKasir')->name('getChartKasir.chart');
Route::resource('chart/semua_trans','Chart\SemuaController');
Route::post('/getChartSemuaTrans','Chart\SemuaController@getChartSemuaTrans')->name('getChartSemuaTrans.chart');
Route::post('/getTotalTransaksi','Chart\SemuaController@getTotalTransaksi')->name('getTotalTransaksi.chart');

Route::resource('landing_page','LandingPageController');
Route::get('getChartData','LandingPageController@getChartData');
