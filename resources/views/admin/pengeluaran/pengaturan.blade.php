<form role="form" id="edit"   enctype="multipart/form-data" method="post" accept-charset="utf-8">
  {{method_field('PATCH')}}
  <div class="card-body">
    <div class="row" style="padding-top: 10px">
      @foreach($dt_tipe as $tipe)
      <div class="col-md-12">
        <div class="form-check">
          <input class="form-check-input" type="checkbox"  id="input_tipe__{{$tipe->id}}" class="form-control" name="input_tipe__{{$tipe->id}}">
          <label class="form-check-label" for="input_tipe__{{$tipe->id}}">{{$tipe->nama}}</label>
        </div>
      </div>
      @endforeach
    </div>
  </div>
  <div class="card-footer" id="action_form_pengeluaran">
      <button type="button" class="btn btn-success btn-block col-md-12" id="btn_add_pengeluaran_save"><span style="font-size: 20px">Simpan</span></button>
      <button type="button" class="btn btn-default btn-block col-md-12" id="btn_add_pengeluaran_cancel" data-dismiss="modal"><span style="font-size: 20px">Cancel</span></button>
  </div>
  <!-- /.card-body -->
</form>

<style type="text/css">
.modal-content {
  max-height: 90%;
  border-radius: 0;
}
.modal-body{
  overflow-y: auto;
}
</style>
<script type="text/javascript">
$(function(){
  getCheckedPengaturan();
})
$("#btn_add_pengeluaran_save").click(function(){
  var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
  var myDataDetail = new FormData();
  myDataDetail.append('_token', CSRF_TOKEN);
  let number = 0;
  $("input[name^='input_tipe__']" ).each(function(){
      myDataDetail.append($(this).attr('id'), ($(this).is(':checked') ? 1 : 0));
  });
  myDataDetail.append('pengaturan', "{{$id->tipe}}");
  $.ajax({
    url: 'savePengaturanPengeluaran',
    type: 'POST',
    data: myDataDetail,
    dataType: 'json',
    cache: false,
    processData: false,
    contentType: false,
    beforeSend: function () {
        $("#btn_add_pengeluaran_save").prop('disabled', true); // disable button
    },
    success: function (data) {
        if (data.type === 'success') {
            notify_view(data.type, data.message);
            $("#btn_add_pengeluaran_save").prop('disabled', false); // disable button
            $('#myModal').modal('hide'); // hide bootstrap modal
        } else if (data.type === 'error') {
            if (data.errors) {
                $.each(data.errors, function (key, val) {
                    $('#error_' + key).html(val);
                    console.log("#"+key);
                    $("#"+key).addClass('is-invalid');
                });
            }else{
              Swal.fire({
                icon: 'error',
                title: data.message
              })
            }
            $("#btn_add_pengeluaran_save").prop('disabled', false); // disable button
        }
    },
    error: function (result) {
      $("#btn_add_pengeluaran_save").prop('disabled', false); // disable button
      Swal.fire({
        icon: 'error',
        title: 'Terjadi kesalahan pada koneksi! <br>Pastikan koneksi Anda stabil'
      })
    }
  });
});
function getCheckedPengaturan(){
  $.ajax({
    url: 'getPengaturanPengeluaran/{{$id->tipe}}',
    type: 'GET',
    dataType: 'json',
    cache: false,
    processData: false,
    contentType: false,
    success: function (data) {
      var pengaturan = JSON.parse(data.dt_pengeluaran.value);
      $.each(pengaturan, function (i,val) {
        $("#input_tipe__"+val).prop('checked',true);
      });
    },
    error: function (result) {
      Swal.fire({
        icon: 'error',
        title: 'Terjadi kesalahan pada koneksi! <br>Pastikan koneksi Anda stabil'
      })
    }
  });
}
</script>