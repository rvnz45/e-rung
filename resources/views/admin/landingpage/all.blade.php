@extends('admin.layouts.admin')
@section('title','Landing Page')
@section('breadcumb')
  <li class="breadcrumb-item"><a href="#">Access Managemen</a></li>
  <li class="breadcrumb-item active">Operators</li>
@endsection
@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card card-outline card-info">
      <div class="card-header">
        <h3 class="card-title">
          LANDING PAGE
        </h3>
        <!-- tools box -->
        <div class="card-tools">
          <button type="button" class="btn btn-tool btn-sm" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
            <i class="fas fa-minus"></i></button>
          <button type="button" class="btn btn-tool btn-sm" data-card-widget="remove" data-toggle="tooltip" title="Remove">
            <i class="fas fa-times"></i></button>
        </div>
        <!-- /. tools -->
      </div>
    <div class="box-header with-border">
    </div>
      <!-- /.card-header -->
      <form role="form" id="create"   enctype="multipart/form-data" method="post" accept-charset="utf-8">
      <div class="card-body pad">
          <div class="panel-body">
            <button type="button" id="btn_create" class="btn btn-success">Simpan</button><br><br>

            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="background_login">Background Login</label><br>
                  <input type="file" id="background_login" name="background_login">
                  {{$background_login}}
                </div>  
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="title_login">Judul Login</label><br>
                  <input type="text" id="title_login" class="form-control" name="title_login" autocomplete="off" placeholder="Judul Login" value="{{$title_login}}" >
                </div>  
              </div>


              <div class="col-md-6">
                <div class="form-group">
                  <label for="logo">Logo</label><br>
                  <input type="file" id="logo" name="logo">
                  {{$logo}}
                </div>  
              </div>
              <div class="col-md-6">
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label for="pembukaan_toko">Pembukaan</label>
                  <input type="text" id="pembukaan_toko" class="form-control" name="pembukaan_toko" autocomplete="off" placeholder="Pembukaan" value="{{$pembukaan_toko}}" >
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="pembukaan_toko_tambahan">Pembukaan Detail</label>
                  <textarea id="pembukaan_toko_tambahan" class="form-control" name="pembukaan_toko_tambahan" autocomplete="off" placeholder="Pembukaan Detail">{{$pembukaan_toko_tambahan}}</textarea>
                </div>
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label for="title_aboutUs">About</label>
                  <input type="text" id="title_aboutUs" class="form-control" name="title_aboutUs" autocomplete="off" placeholder="About" value="{{$title_aboutUs}}" >
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="desc_title_aboutUs">About Detail</label>
                  <textarea id="desc_title_aboutUs" class="form-control" name="desc_title_aboutUs" autocomplete="off" placeholder="About Detail">{{$desc_title_aboutUs}}</textarea>
                </div>
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label for="detail_aboutUs1">Detail About 1</label>
                  <input type="text" id="detail_aboutUs1" class="form-control" name="detail_aboutUs1" autocomplete="off" placeholder="Detail About 1" value="{{$detail_aboutUs1}}" >
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="desc_detail_aboutUs1">Deskripsi Detail About 1</label>
                  <textarea id="desc_detail_aboutUs1" class="form-control" name="desc_detail_aboutUs1" autocomplete="off" placeholder="Deskripsi Detail About 1">{{$desc_detail_aboutUs1}}</textarea>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="detail_aboutUs2">Detail About 2</label>
                  <input type="text" id="detail_aboutUs2" class="form-control" name="detail_aboutUs2" autocomplete="off" placeholder="Detail About 2" value="{{$detail_aboutUs2}}" >
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="desc_detail_aboutUs2">Deskripsi Detail About 2</label>
                  <textarea id="desc_detail_aboutUs2" class="form-control" name="desc_detail_aboutUs2" autocomplete="off" placeholder="Deskripsi Detail About 2">{{$desc_detail_aboutUs2}}</textarea>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="detail_aboutUs3">Detail About 3</label>
                  <input type="text" id="detail_aboutUs3" class="form-control" name="detail_aboutUs3" autocomplete="off" placeholder="Detail About 3" value="{{$detail_aboutUs3}}">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="desc_detail_aboutUs3">Deskripsi Detail About 3</label>
                  <textarea id="desc_detail_aboutUs3" class="form-control" name="desc_detail_aboutUs3" autocomplete="off" placeholder="Deskripsi Detail About 3">{{$desc_detail_aboutUs3}}</textarea>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="img_aboutUs">Gambar About</label><br>
                  <input type="file" id="img_aboutUs" name="img_aboutUs"> {{$img_aboutUs}}
                </div>  
              </div>
              <div class="col-md-6">
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label for="title_fact">Fact</label>
                  <input type="text" id="title_fact" class="form-control" name="title_fact" autocomplete="off" placeholder="Facts"  value="{{$title_fact}}">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="desc_title_fact">Detial Fact</label>
                  <textarea id="desc_title_fact" class="form-control" name="desc_title_fact" autocomplete="off" placeholder="Detial Fact">{{$desc_title_fact}}</textarea>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="detail_fact_angka1">Jumlah Detail Fact 1</label>
                  <input type="number" id="detail_fact_angka1" class="form-control" name="detail_fact_angka1" autocomplete="off" placeholder="Jumlah Detail Fact 1" step="any" value="{{$detail_fact_angka1}}">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="detail_fact_title1">Judul Detail Fact 1</label>
                  <input type="text" id="detail_fact_title1" class="form-control" name="detail_fact_title1" autocomplete="off" placeholder="Judul Detail Fact 1" value="{{$detail_fact_title1}}">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="detail_fact_angka2">Jumlah Detail Fact 2</label>
                  <input type="number" id="detail_fact_angka2" class="form-control" name="detail_fact_angka2" autocomplete="off" placeholder="Jumlah Detail Fact 2" step="any" value="{{$detail_fact_angka2}}">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="detail_fact_title2">Judul Detail Fact 2</label>
                  <input type="text" id="detail_fact_title2" class="form-control" name="detail_fact_title2" autocomplete="off" placeholder="Judul Detail Fact 2" value="{{$detail_fact_title2}}">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="detail_fact_angka3">Jumlah Detail Fact 3</label>{{$detail_fact_angka3}}
                  <input type="number" id="detail_fact_angka3" class="form-control" name="detail_fact_angka3" autocomplete="off" placeholder="Jumlah Detail Fact 3" step="any" value="{{$detail_fact_angka3}}">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="detail_fact_title3">Judul Detail Fact 3</label>
                  <input type="text" id="detail_fact_title3" class="form-control" name="detail_fact_title3" autocomplete="off" placeholder="Judul Detail Fact 3"  value="{{$detail_fact_title3}}">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="detail_fact_angka4">Jumlah Detail Fact 4</label>
                  <input type="number" id="detail_fact_angka4" class="form-control" name="detail_fact_angka4" autocomplete="off" placeholder="Jumlah Detail Fact 4" step="any"  value="{{$detail_fact_angka4}}">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="detail_fact_title4">Judul Detail Fact 4</label>
                  <input type="text" id="detail_fact_title4" class="form-control" name="detail_fact_title4" autocomplete="off" placeholder="Judul Detail Fact 4"  value="{{$detail_fact_title4}}">
                </div>
              </div>


              <div class="col-md-6">
                <div class="form-group">
                  <label for="title_service">Service</label>
                  <input type="text" id="title_service" class="form-control" name="title_service" autocomplete="off" placeholder="Service" value="{{$title_service}}">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="desc_service">Deskripsi Service</label>
                  <textarea id="desc_service" class="form-control" name="desc_service" autocomplete="off" placeholder="Deskripsi Service">{{$desc_service}}</textarea>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="detail_service1">Detail Service 1</label>
                  <input type="text" id="detail_service1" class="form-control" name="detail_service1" autocomplete="off" placeholder="Detail Service 1" value="{{$detail_service1}}">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="desc_detail_service1">Deskripsi Detail Service 1</label>
                  <textarea id="desc_detail_service1" class="form-control" name="desc_detail_service1" autocomplete="off" placeholder="Deskripsi Detail Service 1">{{$desc_detail_service1}}</textarea>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="detail_service2">Detail Service 2</label>
                  <input type="text" id="detail_service2" class="form-control" name="detail_service2" autocomplete="off" placeholder="Detail Service 2" value="{{$detail_service1}}">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="desc_detail_service2">Deskripsi Detail Service 2</label>
                  <textarea id="desc_detail_service2" class="form-control" name="desc_detail_service2" autocomplete="off" placeholder="Deskripsi Detail Service 2">{{$desc_detail_service2}}</textarea>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="detail_service3">Detail Service 3</label>
                  <input type="text" id="detail_service3" class="form-control" name="detail_service3" autocomplete="off" placeholder="Detail Service 3" value="{{$detail_service3}}">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="desc_detail_service3">Deskripsi Detail Service 3</label>
                  <textarea id="desc_detail_service3" class="form-control" name="desc_detail_service3" autocomplete="off" placeholder="Deskripsi Detail Service 3">{{$desc_detail_service3}}</textarea>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="detail_service4">Detail Service 4</label>
                  <input type="text" id="detail_service4" class="form-control" name="detail_service4" autocomplete="off" placeholder="Detail Service 4" value="{{$detail_service4}}">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="desc_detail_service4">Deskripsi Detail Service 4</label>
                  <textarea id="desc_detail_service4" class="form-control" name="desc_detail_service4" autocomplete="off" placeholder="Deskripsi Detail Service 4">{{$desc_detail_service4}}</textarea>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="detail_service5">Detail Service 5</label>
                  <input type="text" id="detail_service5" class="form-control" name="detail_service5" autocomplete="off" placeholder="Detail Service 5" value="{{$detail_service5}}">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="desc_detail_service5">Deskripsi Detail Service 5</label>
                  <textarea id="desc_detail_service5" class="form-control" name="desc_detail_service5" autocomplete="off" placeholder="Deskripsi Detail Service 5">{{$desc_detail_service5}}</textarea>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="detail_service6">Detail Service 6</label>
                  <input type="text" id="detail_service6" class="form-control" name="detail_service6" autocomplete="off" placeholder="Detail Service 6"  value="{{$detail_service6}}">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="desc_detail_service6">Deskripsi Detail Service 6</label>
                  <textarea id="desc_detail_service6" class="form-control" name="desc_detail_service6" autocomplete="off" placeholder="Deskripsi Detail Service 6">{{$desc_detail_service6}}</textarea>
                </div>
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label for="title_action">Judul Action</label>
                  <input type="text" id="title_action" class="form-control" name="title_action" autocomplete="off" placeholder="Judul Action"  value="{{$title_action}}">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="desc_action">Deskripsi Action</label>
                  <input type="text" id="desc_action" class="form-control" name="desc_action" autocomplete="off" placeholder="Deskripsi Action"  value="{{$desc_action}}">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="btn_action">Label Button Action</label>
                  <input type="text" id="btn_action" class="form-control" name="btn_action" autocomplete="off" placeholder="Label Button Action"  value="{{$btn_action}}">
                </div>
              </div>
              <div class="col-md-6">
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label for="title_tim">Judul Anggota</label>
                  <input type="text" id="title_tim" class="form-control" name="title_tim" autocomplete="off" placeholder="Judul Anggota"  value="{{$title_tim}}">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="desc_tim">Deskripsi Judul Anggota</label>
                  <textarea id="desc_tim" class="form-control" name="desc_tim" autocomplete="off" placeholder="Deskripsi Judul Anggota">{{$desc_tim}}</textarea>
                </div>
              </div>



              <div class="col-md-6">
                <div class="form-group">
                  <label for="img_tim1">Foto Anggota 1</label><br>
                  <input type="file" id="img_tim1" name="img_tim1">{{$img_tim1}}
                </div>  
              </div>
              <div class="col-md-6">
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="nama_img_tim1">Nama Anggota 1</label>
                  <input type="text" id="nama_img_tim1" class="form-control" name="nama_img_tim1" autocomplete="off" placeholder="Nama Anggota 1" value="{{$nama_img_tim1}}">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="jabatan_img_tim1">Jabatan Anggota 1</label>
                  <input type="text" id="jabatan_img_tim1" class="form-control" name="jabatan_img_tim1" autocomplete="off" placeholder="Jabatan Anggota 1" value="{{$jabatan_img_tim1}}">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="img_tim2">Foto Anggota 2</label><br>
                  <input type="file" id="img_tim2" name="img_tim2">{{$img_tim2}}
                </div>  
              </div>
              <div class="col-md-6">
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="nama_img_tim2">Nama Anggota 2</label>
                  <input type="text" id="nama_img_tim2" class="form-control" name="nama_img_tim2" autocomplete="off" placeholder="Nama Anggota 2" value="{{$nama_img_tim2}}">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="jabatan_img_tim2">Jabatan Anggota 2</label>
                  <input type="text" id="jabatan_img_tim2" class="form-control" name="jabatan_img_tim2" autocomplete="off" placeholder="Jabatan Anggota 2" value="{{$jabatan_img_tim2}}">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="img_tim3">Foto Anggota 3</label><br>
                  <input type="file" id="img_tim3" name="img_tim3">{{$img_tim3}}
                </div>  
              </div>
              <div class="col-md-6">
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="nama_img_tim3">Nama Anggota 3</label>
                  <input type="text" id="nama_img_tim3" class="form-control" name="nama_img_tim3" autocomplete="off" placeholder="Nama Anggota 3" value="{{$nama_img_tim3}}">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="jabatan_img_tim3">Jabatan Anggota 3</label>
                  <input type="text" id="jabatan_img_tim3" class="form-control" name="jabatan_img_tim3" autocomplete="off" placeholder="Jabatan Anggota 3" value="{{$jabatan_img_tim3}}">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="img_tim4">Foto Anggota 4</label><br>
                  <input type="file" id="img_tim4" name="img_tim4">{{$img_tim4}}
                </div>  
              </div>
              <div class="col-md-6">
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="nama_img_tim4">Nama Anggota 4</label>
                  <input type="text" id="nama_img_tim4" class="form-control" name="nama_img_tim4" autocomplete="off" placeholder="Nama Anggota 4" value="{{$nama_img_tim4}}">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="jabatan_img_tim4">Jabatan Anggota 4</label>
                  <input type="text" id="jabatan_img_tim4" class="form-control" name="jabatan_img_tim4" autocomplete="off" placeholder="Jabatan Anggota 4" value="{{$jabatan_img_tim4}}">
                </div>
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label for="title_contact">Kontak</label>
                  <input type="text" id="title_contact" class="form-control" name="title_contact" autocomplete="off" placeholder="Kontak" value="{{$title_contact}}">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="desc_contact">Deskripsi Kontak</label>
                  <input type="text" id="desc_contact" class="form-control" name="desc_contact" autocomplete="off" placeholder="Deskripsi Kontak" value="{{$desc_contact}}">
                </div>
              </div>
              <div class="col-md-12">
                <div class="form-group">
                  <label for="peta_contact">Peta</label>
                  <textarea id="peta_contact" class="form-control" name="peta_contact" autocomplete="off" placeholder="Peta">{{$peta_contact}}</textarea>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="web_alamat">Alamat</label>
                  <textarea id="web_alamat" class="form-control" name="web_alamat" autocomplete="off" placeholder="Alamat">{{$web_alamat}}</textarea>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="web_email">Email</label>
                  <input type="text" id="web_email" class="form-control" name="web_email" autocomplete="off" placeholder="Email" value="{{$web_email}}">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="web_nohp">No HP</label>
                  <input type="text" id="web_nohp" class="form-control" name="web_nohp" autocomplete="off" placeholder="No HP" value="{{$web_nohp}}">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="web_twitter">Twitter</label>
                  <input type="text" id="web_twitter" class="form-control" name="web_twitter" autocomplete="off" placeholder="Twitter" value="{{$web_twitter}}">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="web_facebook">Facebook</label>
                  <input type="text" id="web_facebook" class="form-control" name="web_facebook" autocomplete="off" placeholder="Facebook" value="{{$web_facebook}}">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="web_instagram">Instagram</label>
                  <input type="text" id="web_instagram" class="form-control" name="web_instagram" autocomplete="off" placeholder="Instagram" value="{{$web_instagram}}">
                </div>
              </div>

            </div>
          </div>
      </div>
      </form>
    </div>
  </div>
  <!-- /.col-->
</div>
<style>
    @media screen and (min-width: 768px) {
        #myModal .modal-dialog {
            width: 75%;
            border-radius: 5px;
        }
    }
</style>
<script type="text/javascript">
$(document).ready(function () {
  $("#menu_landing_page").addClass('menu-open');
  $("#menu_landing_page").addClass('active');
  $('#btn_create').click(function(){
    var myData = new FormData($("#create")[0]);
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    myData.append('_token', CSRF_TOKEN);

    $.ajax({
        url: 'landing_page',
        type: 'POST',
        data: myData,
        dataType: 'json',
        cache: false,
        processData: false,
        contentType: false,
        beforeSend: function () {
            $("#submit").prop('disabled', true); // disable button
        },
        success: function (data) {
            if (data.type === 'success') {
                notify_view(data.type, data.message);
                $("#submit").prop('disabled', false); // disable button

            } else if (data.type === 'error') {
                if (data.errors) {
                    $.each(data.errors, function (key, val) {
                        $('#error_' + key).html(val);
                        $("#"+key).addClass('is-invalid');
                    });
                }
                $("#status").html(data.message);
                $("#submit").prop('disabled', false); // disable button
            }
        },
        error: function (result) {
          Swal.fire({
            icon: 'error',
            title: 'Terjadi kesalahan pada koneksi! <br>Pastikan koneksi Anda stabil'
          })
        }
    });
  });
});



</script>
@endsection