@extends('admin.layouts.admin')
@section('title','Semua level')
@section('breadcumb')
	<li class="breadcrumb-item"><a href="#">Access Managemen</a></li>
	<li class="breadcrumb-item active">Level</li>
@endsection
@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card card-outline card-info">
      <div class="card-header">
        <h3 class="card-title">
          TABEL LEVEL
        </h3>
        <!-- tools box -->
        <div class="card-tools">
          <button type="button" class="btn btn-tool btn-sm" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
            <i class="fas fa-minus"></i></button>
          <button type="button" class="btn btn-tool btn-sm" data-card-widget="remove" data-toggle="tooltip" title="Remove">
            <i class="fas fa-times"></i></button>
        </div>
        <!-- /. tools -->
      </div>
    <div class="box-header with-border">
      
    </div>
      <!-- /.card-header -->
      <div class="card-body pad">
        <div class="row">
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-info">
              <div class="inner">
                <h3><span id="total_produk">0</span></h3>

                <p>PRODUK</p>
              </div>
              <div class="icon">
                <i class="fas fa-shekel-sign"></i>
              </div>
              <a href="{{ URL :: to('/admin_bqs/produk') }}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-success">
              <div class="inner">
                <h3><span id="total_pembelian">0</span></h3>

                <p>PEMBELIAN</p>
              </div>
              <div class="icon">
                <i class="fas fa-folder"></i>
              </div>
              <a href="{{ URL :: to('/admin_bqs/pembelian') }}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-warning">
              <div class="inner">
                <h3><span id="total_pengeluaran">0</span></h3>

                <p>PENGELUARAN</p>
              </div>
              <div class="icon">
                <i class="fas fa-folder"></i>
              </div>
              <a href="{{ URL :: to('/admin_bqs/pengeluaran') }}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-danger">
              <div class="inner">
                <h3><span id="total_kasir">0</span></h3>

                <p>KASIR</p>
              </div>
              <div class="icon">
                <i class="fas fa-cash-register"></i>
              </div>
              <a href="{{ URL :: to('/admin_bqs/kasir') }}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
        </div>
        <div class="card" style="background-color: #5fc977">
          <div class="card-header border-0 ui-sortable-handle" style="cursor: move;">
            <h3 class="card-title">
              <i class="fas fa-th mr-1"></i>
              GRAFIK PENDAPATAN KASIR BULAN INI
            </h3>

            <div class="card-tools">
              <button type="button" class="btn bg-info btn-sm" data-card-widget="collapse">
                <i class="fas fa-minus"></i>
              </button>
              <button type="button" class="btn bg-info btn-sm" data-card-widget="remove">
                <i class="fas fa-times"></i>
              </button>
            </div>
          </div>
          <div class="card-body">
            <canvas class="chart chartjs-render-monitor" id="line-chart" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%; display: block; width: 600px;" width="600" height="250"></canvas>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- /.col-->
</div>
<script type="text/javascript">
$(function(){
  getChartData();
})
function getChartData(){
  $.ajax({
    url: 'getChartData',
    type: 'GET',
    dataType: 'json',
    cache: false,
    processData: false,
    contentType: false,
    success: function (data) {
      console.log(data.kasir);
      $("#total_produk").html(data.produk);
      $("#total_pembelian").html(data.pembelian);
      $("#total_pengeluaran").html(data.pengeluaran);
      $("#total_kasir").html(data.kasir);
      var speedCanvas = document.getElementById("line-chart");

      Chart.defaults.global.defaultFontFamily = "Lato";
      Chart.defaults.global.defaultFontSize = 18;

      var speedData = {
        labels: data.dt_tanggal,
        datasets: [{
          label: "GRAFIK PENDAPATAN KASIR",
          data: data.dt_pembayaran,
        }]
      };

      var chartOptions = {
        legend: {
          display: true,
          position: 'top',
          labels: {
            boxWidth: 80,
            fontColor: 'black'
          },
        },
         backgroundColor: [ 'rgba(255, 99, 132, 0.2)'],
        responsive              : true,
        maintainAspectRatio     : false,
        datasetFill             : false
      };

      var lineChart = new Chart(speedCanvas, {
        type: 'line',
        data: speedData,
        options: chartOptions
      })
    },
    error: function (result) {
      Swal.fire({
        icon: 'error',
        title: 'Terjadi kesalahan pada koneksi! <br>Pastikan koneksi Anda stabil'
      })
    }
  });
}
</script>
@endsection