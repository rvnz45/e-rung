<aside class="main-sidebar sidebar-dark-primary elevation-4">
  <!-- Brand Logo -->
  <a href="index3.html" class="brand-link">
    <img src="{{ url('admin/dist/img/AdminLTELogo.png') }}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
         style="opacity: .8">
    <span class="brand-text font-weight-light">AdminLTE 3</span>
  </a>

  <!-- Sidebar -->
  <div class="sidebar">

    <!-- Sidebar Menu -->
    <nav class="mt-2">
      <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <!-- Add icons to the links using the .nav-icon class
             with font-awesome or any other icon font library -->
        @if($dt_user->app_user_level_id!='kasir')
        <li class="nav-item has-treeview menu-open">
          <a href="{{ URL :: to('/admin_bqs/dashboard') }}" class="nav-link active">
            <i class="nav-icon fas fa-tachometer-alt"></i>
            <p>
              Dashboard
            </p>
          </a>
        </li>
        
        <li class="nav-item has-treeview" id="menu_landing_page">
          <a href="{{ URL :: to('/admin_bqs/landing_page') }}" class="nav-link" >
            <i class="nav-icon fas fa-solar-panel"></i>
            <p>
              Landing Page
            </p>
          </a>
        </li>
        @endif
        <li class="nav-item has-treeview" id="menu_kasir">
          <a href="{{ URL :: to('/admin_bqs/kasir') }}" class="nav-link" id="menu_kasir_detail">
            <i class="nav-icon fas fa-cash-register"></i>
            <p>
              Kasir
            </p>
          </a>
        </li>
        @if($dt_user->app_user_level_id!='kasir')
        <li class="nav-item has-treeview" id="menu_stok">
          <a href="#" class="nav-link">
            <i class="nav-icon fas fa-folder"></i>
            <p>
              Stok
              <i class="fas fa-angle-left right"></i>
            </p>
          </a>
          <ul class="nav nav-treeview">
            <li class="nav-item">
              <a href="{{ URL :: to('/admin_bqs/pembelian') }}" class="nav-link"  id="menu_stok_pembelian">
                <i class="far fa-circle nav-icon"></i>
                <p>Pembelian</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="{{ URL :: to('/admin_bqs/pengeluaran') }}" class="nav-link"  id="menu_stok_pengeluaran">
                <i class="far fa-circle nav-icon"></i>
                <p>Pengeluaran</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="{{ URL :: to('/admin_bqs/stokOpname') }}" class="nav-link"  id="menu_stok_opname">
                <i class="far fa-circle nav-icon"></i>
                <p>Stok Opname</p>
              </a>
            </li>
          </ul>
        </li>
        <li class="nav-item has-treeview" id="menu_produk">
          <a href="#" class="nav-link">
            <i class="nav-icon fas fa-shekel-sign"></i>
            <p>
              Produk
              <i class="fas fa-angle-left right"></i>
            </p>
          </a>
          <ul class="nav nav-treeview">
            <li class="nav-item">
              <a href="{{ URL :: to('/admin_bqs/produk') }}" class="nav-link" id="menu_produk_master_produk">
                <i class="far fa-circle nav-icon"></i>
                <p>Master Produk</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="{{ URL :: to('/admin_bqs/paketMenu') }}" class="nav-link"  id="menu_produk_paket_menu">
                <i class="far fa-circle nav-icon"></i>
                <p>Paket Menu</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="{{ URL :: to('/admin_bqs/kategori') }}" class="nav-link" id="menu_produk_master_kategori">
                <i class="far fa-circle nav-icon"></i>
                <p>Kategori</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="{{ URL :: to('/admin_bqs/tipe') }}" class="nav-link" id="menu_produk_master_tipe">
                <i class="far fa-circle nav-icon"></i>
                <p>Tipe</p>
              </a>
            </li>
          </ul>
        </li>
        <li class="nav-item has-treeview"  id="menu_laporan">
          <a href="#" class="nav-link">
            <i class="nav-icon fas fa-edit"></i>
            <p>
              Laporan 
              <i class="fas fa-angle-left right"></i>
            </p>
          </a>
          <ul class="nav nav-treeview">
            <li class="nav-item">
              <a href="{{ URL :: to('/admin_bqs/laporan/kasir') }}" class="nav-link" id="menu_laporan_kasir">
                <i class="far fa-circle nav-icon"></i>
                <p>Kasir</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="{{ URL :: to('/admin_bqs/laporan/produk') }}" class="nav-link" id="menu_laporan_produk">
                <i class="far fa-circle nav-icon"></i>
                <p>Produk</p>
              </a>
            </li>
          </ul>
        </li>
        <li class="nav-item has-treeview" id="menu_chart">
          <a href="#" class="nav-link">
            <i class="nav-icon fas fa-chart-pie"></i>
            <p>
              Charts
              <i class="right fas fa-angle-left"></i>
            </p>
          </a>
          <ul class="nav nav-treeview">
            <li class="nav-item">
              <a href="{{ URL :: to('/admin_bqs/chart/kasir') }}" class="nav-link" id="menu_chart_kasir">
                <i class="far fa-circle nav-icon"></i>
                <p>Kasir</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="{{ URL :: to('/admin_bqs/chart/semua_trans') }}" class="nav-link" id="menu_chart_semua_trans">
                <i class="far fa-circle nav-icon"></i>
                <p>Semua Transaksi</p>
              </a>
            </li>
          </ul>
        </li>
        <li class="nav-item has-treeview" id="menu_master_data">
          <a href="#" class="nav-link">
            <i class="nav-icon fas fa-database"></i>
            <p>
              Master Data
              <i class="fas fa-angle-left right"></i>
            </p>
          </a>
          <ul class="nav nav-treeview">
            <li class="nav-item">
              <a href="{{ URL :: to('/admin_bqs/satuan') }}" class="nav-link" id="menu_master_data_satuan">
                <i class="far fa-circle nav-icon"></i>
                <p>Satuan</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="{{ URL :: to('/admin_bqs/provinsi') }}" class="nav-link" id="menu_master_data_provinsi">
                <i class="far fa-circle nav-icon"></i>
                <p>Provinsi</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="{{ URL :: to('/admin_bqs/kota') }}" class="nav-link" id="menu_master_data_kota">
                <i class="far fa-circle nav-icon"></i>
                <p>Kota</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="{{ URL :: to('/admin_bqs/kecamatan') }}" class="nav-link" id="menu_master_data_kecamatan">
                <i class="far fa-circle nav-icon"></i>
                <p>Kecamatan</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="{{ URL :: to('/admin_bqs/desa') }}" class="nav-link" id="menu_master_data_desa">
                <i class="far fa-circle nav-icon"></i>
                <p>Desa</p>
              </a>
            </li>
          </ul>
        </li>
        <li class="nav-item has-treeview" id="menu_access_managemen">
          <a href="#" class="nav-link">
            <i class="nav-icon fas fa-cog"></i>
            <p>
              Access Managemen
              <i class="fas fa-angle-left right"></i>
            </p>
          </a>
          <ul class="nav nav-treeview">
            <li class="nav-item">
              <a href="{{ URL :: to('/admin_bqs/users') }}" class="nav-link"  id="menu_access_managemen_operators">
                <i class="far fa-circle nav-icon"></i>
                <p>Operators</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="{{ URL :: to('/admin_bqs/level') }}" class="nav-link"  id="menu_access_managemen_level">
                <i class="far fa-circle nav-icon"></i>
                <p>Level</p>
              </a>
            </li>
            <!-- <li class="nav-item">
              <a href="{{ URL :: to('/admin_bqs/permissions') }}" class="nav-link"  id="menu_access_managemen_permissions">
                <i class="far fa-circle nav-icon"></i>
                <p>Permissions</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="{{ URL :: to('/admin_bqs/roles') }}" class="nav-link"  id="menu_access_managemen_roles">
                <i class="far fa-circle nav-icon"></i>
                <p>Roles</p>
              </a>
            </li> -->
          </ul>
        </li>
        @endif
        
      </ul>
    </nav>
    <!-- /.sidebar-menu -->
  </div>
  <!-- /.sidebar -->
</aside>