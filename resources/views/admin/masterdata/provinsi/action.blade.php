<a data-toggle='tooltip' class='col-md-3 btn btn-warning btn-md edit' id='{{sprintf("%02d",$id)}}' title='Edit'> <i class='fa fa-edit text-error'></i></a>
<a data-toggle='tooltip' class='col-md-3 btn btn-danger btn-md  delete' id='{{sprintf("%02d",$id)}}' title='Delete'> <i class='fa fa-trash-alt'></i></a>
