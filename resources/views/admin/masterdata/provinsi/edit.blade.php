<form role="form" id="edit"   enctype="multipart/form-data" method="post" accept-charset="utf-8">
  {{method_field('PATCH')}}
  <div class="card-body">
    <div class="form-group">
      <label for="input_provinsi">Provinsi</label>
      <input type="text" class="form-control" id="input_provinsi" placeholder="Provinsi" name="input_provinsi" value="{{$provinsi->nama}}">
      <span id="error_input_provinsi" class="error invalid-feedback"></span>
    </div>
    <div class="form-group">
      <label for="input_latitude">Latitude</label>
      <input type="text" class="form-control" id="input_latitude" placeholder="Latitude" name="input_latitude" value="{{$provinsi->latitude}}">
      <span id="error_input_latitude" class="error invalid-feedback"></span>
    </div>
    <div class="form-group">
      <label for="input_longitude">Longitude</label>
      <input type="text" class="form-control" id="input_longitude" placeholder="Longitude" name="input_longitude" value="{{$provinsi->longitude}}">
      <span id="error_input_longitude" class="error invalid-feedback"></span>
    </div>
  </div>
  <!-- /.card-body -->

  <div class="card-footer">
    <button type="submit" class="btn btn-success">Simpan</button>
    <button type="button" class="btn btn-default"
            data-dismiss="modal">
        Close
    </button>
  </div>
</form>
<script type="text/javascript">
$(document).ready(function () {
  $('#edit').validate({
    submitHandler: function (form) {

        var myData = new FormData($("#edit")[0]);
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        myData.append('_token', CSRF_TOKEN);

        $.ajax({
            url: 'provinsi/{{sprintf("%02d",$provinsi->id)}}',
            type: 'POST',
            data: myData,
            dataType: 'json',
            cache: false,
            processData: false,
            contentType: false,
            beforeSend: function () {
                $("#submit").prop('disabled', true); // disable button
                $("#modal-overlay").addClass('overlay d-flex justify-content-center align-items-center');
                $("#modal-overlay-content").addClass('fas fa-2x fa-sync fa-spin');
            },
            success: function (data) {
                if (data.type === 'success') {
                    reload_table();
                    notify_view(data.type, data.message);
                    $("#modal-overlay").removeClass();
                    $("#modal-overlay-content").removeClass();
                    $("#submit").prop('disabled', false); // disable button
                    $('#myModal').modal('hide'); // hide bootstrap modal

                } else if (data.type === 'error') {
                    if (data.errors) {
                        $.each(data.errors, function (key, val) {
                            $('#error_' + key).html(val);
                            $("#"+key).addClass('is-invalid');
                        });
                    }
                    $("#status").html(data.message);
                    $("#modal-overlay").removeClass();
                    $("#modal-overlay-content").removeClass();
                    $("#submit").prop('disabled', false); // disable button

                }

            },
            error: function (result) {
              $("#modal-overlay").removeClass();
              $("#modal-overlay-content").removeClass();
              Swal.fire({
                icon: 'error',
                title: 'Terjadi kesalahan pada koneksi! <br>Pastikan koneksi Anda stabil'
              })
            }
        });
    }
  });
});

</script>