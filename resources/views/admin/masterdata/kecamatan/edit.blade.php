<form role="form" id="edit"   enctype="multipart/form-data" method="post" accept-charset="utf-8">
  {{method_field('PATCH')}}
  <div class="card-body">
    <div class="form-group">
      <label for="input_kota">Provinsi</label>
      <input type="text" class="form-control" id="input_provinsi" placeholder="Provinsi" name="input_provinsi" value="{{$kecamatan->mst_provinsi_nama}}">
      <span id="error_input_provinsi" class="error invalid-feedback"></span>
      <input type="hidden" id='input_provinsi_id' name="input_provinsi_id" readonly class="form-control" value="{{$kecamatan->mst_provinsi_id}}">
    </div>
    <div class="form-group">
      <label for="input_kota">Kota</label>
      <input type="text" class="form-control" id="input_kota" placeholder="Kota" name="input_kota"  value="{{$kecamatan->mst_kota_nama}}">
      <span id="error_input_kota" class="error invalid-feedback"></span>
      <input type="hidden" id='input_kota_id' name="input_kota_id" readonly class="form-control"  value="{{$kecamatan->kota_id}}">
    </div>
    <div class="form-group">
      <label for="input_kecamatan">Kecamatan</label>
      <input type="text" class="form-control" id="input_kecamatan" placeholder="Kecamatan" name="input_kecamatan" value="{{$kecamatan->nama}}">
      <span id="error_input_kecamatan" class="error invalid-feedback"></span>
    </div>
    <div class="form-group">
      <label for="input_latitude">Latitude</label>
      <input type="text" class="form-control" id="input_latitude" placeholder="Latitude" name="input_latitude" value="{{$kecamatan->latitude}}">
      <span id="error_input_latitude" class="error invalid-feedback"></span>
    </div>
    <div class="form-group">
      <label for="input_longitude">Longitude</label>
      <input type="text" class="form-control" id="input_longitude" placeholder="Longitude" name="input_longitude" value="{{$kecamatan->longitude}}">
      <span id="error_input_longitude" class="error invalid-feedback"></span>
    </div>
  </div>
  <!-- /.card-body -->

  <div class="card-footer">
    <button type="submit" class="btn btn-success">Simpan</button>
    <button type="button" class="btn btn-default"
            data-dismiss="modal">
        Close
    </button>
  </div>
</form>
<script type="text/javascript">
$(document).ready(function () {
  $('#edit').validate({
    submitHandler: function (form) {

        var myData = new FormData($("#edit")[0]);
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        myData.append('_token', CSRF_TOKEN);

        $.ajax({
            url: 'kecamatan/{{sprintf("%07d",$kecamatan->id)}}',
            type: 'POST',
            data: myData,
            dataType: 'json',
            cache: false,
            processData: false,
            contentType: false,
            beforeSend: function () {
                $("#submit").prop('disabled', true); // disable button
                $("#modal-overlay").addClass('overlay d-flex justify-content-center align-items-center');
                $("#modal-overlay-content").addClass('fas fa-2x fa-sync fa-spin');
            },
            success: function (data) {
                if (data.type === 'success') {
                    reload_table();
                    notify_view(data.type, data.message);
                    $("#modal-overlay").removeClass();
                    $("#modal-overlay-content").removeClass();
                    $("#submit").prop('disabled', false); // disable button
                    $('#myModal').modal('hide'); // hide bootstrap modal

                } else if (data.type === 'error') {
                    if (data.errors) {
                        $.each(data.errors, function (key, val) {
                            $('#error_' + key).html(val);
                            $("#"+key).addClass('is-invalid');
                        });
                    }
                    $("#status").html(data.message);
                    $("#modal-overlay").removeClass();
                    $("#modal-overlay-content").removeClass();
                    $("#submit").prop('disabled', false); // disable button

                }

            },
            error: function (result) {
              $("#modal-overlay").removeClass();
              $("#modal-overlay-content").removeClass();
              Swal.fire({
                icon: 'error',
                title: 'Terjadi kesalahan pada koneksi! <br>Pastikan koneksi Anda stabil'
              })
            }
        });
    }
  });
});
$("#input_kota").keyup(function(){
    if($("#input_kota" ).val()==''){
        $("#input_kota_id").val('');
    }
});

$("#input_provinsi").keyup(function(){
    if($("#input_provinsi" ).val()==''){
        $("#input_provinsi_id").val('');
        $("#input_kota").val('');
        $("#input_kota_id").val('');
    }
});
$( function() {
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    $("#input_kota" ).autocomplete({
    source: function( request, response ) {
        console.log(request.term);
      $.ajax({
        url:"{{route('admin.autocompleteKota.kota')}}",
        type: 'post',
        dataType: "json",
        data: {
           _token: CSRF_TOKEN,
           search: request.term,
           provinsi_id: $("#input_provinsi_id").val(),

        },
        success: function( data ) {
           response( data );
        },
        error: function (result) {
          Swal.fire({
            icon: 'error',
            title: 'Terjadi kesalahan pada koneksi! <br>Pastikan koneksi Anda stabil'
          })
        }
      });
    },
    select: function (event, ui) {
       // Set selection
       $('#input_kota').val(ui.item.label); // display the selected text
       $('#input_kota_id').val(ui.item.value); // save selected id to input
       return false;
    }
    }).data( "ui-autocomplete" )._renderItem = function( ul, item ) {
      return $( "<li style='padding-left:10px'>" )
        .data( "ui-autocomplete-item", item )
        .append(  item.label + "<br><span style='font-size:11;font-style:italic'>Longitude / Latitude : " + item.longitude + " / " + item.longitude + "</span>" )
        .appendTo( ul );
    };
    $("#input_provinsi" ).autocomplete({
      source: function( request, response ) {
        $.ajax({
          url:"{{route('admin.autocompleteProvinsi.provinsi')}}",
          type: 'post',
          dataType: "json",
          data: {
             _token: CSRF_TOKEN,
             search: request.term
          },
          success: function( data ) {
             response( data );
          },
          error: function (result) {
            Swal.fire({
              icon: 'error',
              title: 'Terjadi kesalahan pada koneksi! <br>Pastikan koneksi Anda stabil'
            })
          }
        });
      },
      select: function (event, ui) {
         // Set selection
         $('#input_provinsi').val(ui.item.label); // display the selected text
         $('#input_provinsi_id').val(ui.item.value); // save selected id to input
         return false;
      }
    }).data( "ui-autocomplete" )._renderItem = function( ul, item ) {
      return $( "<li style='padding-left:10px'>" )
        .data( "ui-autocomplete-item", item )
        .append(  item.label + "<br><span style='font-size:11;font-style:italic'>Longitude / Latitude : " + item.longitude + " / " + item.longitude + "</span>" )
        .appendTo( ul );
    }
});
</script>