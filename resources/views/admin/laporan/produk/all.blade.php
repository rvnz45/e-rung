@extends('admin.layouts.admin')
@section('title','Semua Produk')
@section('breadcumb')
  <li class="breadcrumb-item"><a href="#">Access Managemen</a></li>
  <li class="breadcrumb-item active">Operators</li>
@endsection
@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card card-outline card-info">
      <div class="card-header">
        <h3 class="card-title">
          TABEL PRODUK
        </h3>
        <!-- tools box -->
        <div class="card-tools">
          <button type="button" class="btn btn-tool btn-sm" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
            <i class="fas fa-minus"></i></button>
          <button type="button" class="btn btn-tool btn-sm" data-card-widget="remove" data-toggle="tooltip" title="Remove">
            <i class="fas fa-times"></i></button>
        </div>
        <!-- /. tools -->
      </div>
    <div class="box-header with-border">
    </div>
      <!-- /.card-header -->
      <div class="card-body pad">
          <div class="panel-body">
              <div class="row">
                  <div class="col-md-12" style="padding-bottom: 20px">
                    <button class="btn btn-success" onclick="reload_table()"><i class="fas fa-sync"></i> 
                        Refresh
                    </button>
                  </div>
                  <div class="col-md-12" style="padding-bottom: 20px">
                      <div class="card card-success">
                        <div class="card-header">
                          <h3 class="card-title">Filter Detail</h3>

                          <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                            </button>
                          </div>
                          <!-- /.card-tools -->
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                          <div class="row" style="padding-top: 10px">
                            <div class="col-md-5">
                              <input type="text" class="form-control" id="tgl_pembelian_a" placeholder="Pilih Tanggal Pembelian" name="tgl_pembelian_a" value="{{date('m/d/Y')}}">
                            </div>
                            <div class="col-md-2" style="text-align: center;padding-top: 10px" >
                              s.d
                            </div>
                            <div class="col-md-5">
                              <input type="text" class="form-control" id="tgl_pembelian_b" placeholder="Pilih Tanggal Pembelian" name="tgl_pembelian_b" value="{{date('m/d/Y')}}">
                            </div>
                          </div>
                        </div>
                        <!-- /.card-body -->
                      </div>
                      <!-- /.card -->
                  </div>
                  <div class="col-md-12 col-sm-12 table-responsive">
                      <table id="manage_all" class="table table-collapse table-bordered table-hover  table-striped">
                          <thead>
                          <tr>
                              <th>#</th>
                              <th>Produk</th>
                              <th>Total Item</th>
                              <th>Total Harga Beli</th>
                              <th>Total Harga Jual</th>
                              <th>Keuntungan</th>
                          </tr>
                          </thead>
                      </table>
                  </div>
              </div>
          </div>
      </div>
      <div class="card-body pad">
          <div class="panel-body">
              <div class="row">
                <div class="col-md-7"></div>
                <div class="col-md-5" style="font-size: 30px">
                  <div class="row"> 
                    <div class="col-md-6">Total Harga Beli</div>
                    <div class="col-md-6">
                      Rp. <span id="total_pembelian"></span>
                    </div>
                  </div>
                  <div class="row"> 
                    <div class="col-md-6">Total Harga Terjual</div>
                    <div class="col-md-6">
                      Rp. <span id="total_pendapatan"></span>
                    </div>
                  </div>
                  <div class="row"> 
                    <div class="col-md-6">Total Keuntungan</div>
                    <div class="col-md-6">
                      Rp. <span id="total_keuntungan"></span>
                    </div>
                  </div>
                </div>
              </div>
          </div>
      </div>
    </div>
  </div>
  <!-- /.col-->
</div>
<style>
    @media screen and (min-width: 768px) {
        #myModal .modal-dialog {
            width: 75%;
            border-radius: 5px;
        }
    }
</style>
<script type="text/javascript">
$(document).ready(function () {
  $("#menu_laporan").addClass('menu-open');
  $("#menu_laporan_produk").addClass('active');
  $("#tgl_pembelian_a").datepicker();
  $("#tgl_pembelian_b").datepicker();
  table = $("#manage_all").DataTable({
    processing: true,
    serverSide: true,

    ajax: {
        url: '{!! route('admin.allLaporanProduk.laporan') !!}',
        type: "GET",
        data: function (d) {
          d.tgl_pembelian_a = $("#tgl_pembelian_a").val()
          d.tgl_pembelian_b = $("#tgl_pembelian_b").val()
        },
    },
    "columnDefs": [
      { 
        "targets": [ -1,0 ], //last column
        "orderable": false //set not orderable
      }
    ],
    "autoWidth": false,
  });
  $('.dataTables_filter input[type="search"]').attr('placeholder', 'Type here to search...').css({
      'width': '220px',
      'height': '30px'
  });
  getTotal();
});
function reload_table() {
    table.ajax.reload(null, false); //reload datatable ajax
    getTotal();
}
$("#tgl_pembelian_a").change(function(){
  reload_table();
  getTotal();
});
$("#tgl_pembelian_b").change(function(){
  reload_table();
  getTotal();
});
function getTotal(){
  var myData = new FormData();
  var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
  myData.append('_token', CSRF_TOKEN);
  myData.append('tgl_pembelian_a', $("#tgl_pembelian_a").val());
  myData.append('tgl_pembelian_b', $("#tgl_pembelian_b").val());
  myData.append('search', table.search());
  var base_url = "{!! url('/') !!}";
  $.ajax({
    url: base_url+'/admin_bqs/getTotalKasirDetail',
    type: 'POST',
    data: myData,
    dataType: 'json',
    cache: false,
    processData: false,
    contentType: false,
    success: function (data) {
      $("#total_pembelian").html(data.data.total_asal);
      $("#total_pendapatan").html(data.data.total_bayar);
      let untung = data.data.total_bayar - data.data.total_asal;
      $("#total_keuntungan").html(untung);
    },
    error: function (result) {
      Swal.fire({
        icon: 'error',
        title: 'Terjadi kesalahan pada koneksi! <br>Pastikan koneksi Anda stabil'
      })
    }
  });
}
</script>
@endsection