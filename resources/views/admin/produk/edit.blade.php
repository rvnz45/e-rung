<form role="form" id="edit"   enctype="multipart/form-data" method="post" accept-charset="utf-8">
  {{method_field('PATCH')}}
  <div class="card-body">

    <div class="row" style="padding-top: 10px">
      <div class="col-md-4">
        <img src="@if($produk->foto!='') {{asset($produk->foto)}} @else {{asset('admin/dist/img/imageDefault.jpg')}} @endif" class="product-image" alt="Product Image"  style="height: 220px">
      </div>
      <div class="col-md-8">
        <div class="form-group">
          <label for="input_nama">Nama Barang</label>
          <input type="text" name="input_nama" id="input_nama" placeholder="Nama Barang" class="form-control"  value="{{$produk->nama}}">
          <span id="error_input_nama" class="error invalid-feedback"></span>
        </div>
        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <label for="input_foto">Foto</label>
              <div class="input-group">
                <div class="custom-file">
                  <input type="file" id="input_foto" name="input_foto">
                  <span id="error_input_foto" class="error invalid-feedback"></span>
                </div>
              </div>
            </div>
            <div class="form-group">
              <label for="input_tipe">Tipe</label>
              <div class="row">
                <div class="col-md-10">
                  <input type="text" class="form-control" id="input_tipe" placeholder="Pilih Tipe" name="input_tipe" autocomplete="off" value="{{$produk->mst_tipe_nama}}">
                  <span id="error_input_tipe" class="error invalid-feedback"></span>
                  <input type="hidden" id='input_tipe_id' name="input_tipe_id" readonly class="form-control"  value="{{$produk->tipe_id}}">
                </div>
                <div class="col-md-2" style="padding-top: 3px">
                  <a href="{{ URL :: to('/admin_bqs/tipe') }}" target="_blank">
                  <button class="btn btn-success" type="button"><i class="fa fa-plus"></i></button>
                  </a>
                </div>
              </div>
            </div>
            <div class="form-group">
              <label for="input_kategori">Kategori</label>
              <div class="row">
                <div class="col-md-10">
                  <input type="text" class="form-control" id="input_kategori" placeholder="Pilih Kategori" name="input_kategori" autocomplete="off"  value="{{$produk->mst_kategori_nama}}">
                  <span id="error_input_kategori" class="error invalid-feedback"></span>
                  <input type="hidden" id='input_kategori_id' name="input_kategori_id" readonly class="form-control"  value="{{$produk->kategori_id}}">
                </div>
                <div class="col-md-2" style="padding-top: 3px">
                  <a href="{{ URL :: to('/admin_bqs/kategori') }}" target="_blank">
                  <button class="btn btn-success" type="button"><i class="fa fa-plus"></i></button>
                  </a>
                </div>
              </div>
            </div>
            <div id="harga_detail">
              <div class="form-group">
                <label for="input_harga_beli">Harga Beli</label>
                <input type="number" name="input_harga_beli" id="input_harga_beli" placeholder="Harga Beli" class="form-control" step="any"  value="{{$produk->harga_beli}}">
              </div>
              <div class="form-group">
                <label for="input_harga_jual">Harga Jual</label>
                <input type="number" name="input_harga_jual" id="input_harga_jual" placeholder="Harga Jual" class="form-control" step="any"  value="{{$produk->harga_jual}}">
                <span id="error_input_harga_jual" class="error invalid-feedback"></span>
              </div>
            </div>
            <div class="form-group">
              <label for="input_diskon">Diskon</label>
              <input type="number" name="input_diskon" id="input_diskon" placeholder="Diskon" class="form-control" step="any" value="{{$produk->diskon}}">
              <span id="error_input_diskon" class="error invalid-feedback"></span>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label for="input_stok">Stok</label><br>
              <label for="input_stok"><span id="tampil_stok">{{$produk->stok}}</span></label>
            </div>
            <div class="form-group">
              <label for="input_satuan">Satuan</label>
              <div class="row">
                <div class="col-md-10">
                  <input type="text" class="form-control" id="input_satuan" placeholder="Pilih Satuan" name="input_satuan" autocomplete="off" value="{{$produk->mst_satuan_nama}}">
                  <span id="error_input_satuan" class="error invalid-feedback"></span>
                  <input type="hidden" id='input_satuan_id' name="input_satuan_id" readonly class="form-control" value="{{$produk->satuan_id}}">
                </div>
                <div class="col-md-2" style="padding-top: 3px">
                  <a href="{{ URL :: to('/admin_bqs/satuan') }}" target="_blank">
                  <button class="btn btn-success" type="button"><i class="fa fa-plus"></i></button>
                  </a>
                </div>
              </div>
            </div>
            <div class="form-group">
              <label for="input_stok_min">Stok Minimal</label>
              <input type="number" name="input_stok_min" id="input_stok_min" placeholder="Stok Minimal" class="form-control" step="any" value="{{$produk->stok_min}}">
              <span id="error_input_stok_min" class="error invalid-feedback"></span>
            </div>
            <div class="form-group"  style="padding-top: 5px">
              <label for="input_is_expire">Is Expire</label><br>
              <input type="checkbox" name="input_is_expire" id="input_is_expire" @if($produk->is_expire=='1') checked @endif data-bootstrap-switch data-off-color="danger" data-on-color="success">
            </div>
            <div id="expire_detail">
              <div class="form-group">
                <label for="input_tgl_kadaluarsa">Tgl Kadaluarsa</label><br>
                <input type="text" id="input_tgl_kadaluarsa" class="form-control" name="input_tgl_kadaluarsa"  value="@if($produk->is_expire=='1') {{$produk->tgl_kadaluarsa}} @endif"  autocomplete="off">
              </div>
            </div>
            <div class="form-group"  style="padding-top: 5px">
              <label for="input_is_stok">Status</label><br>
              <input type="checkbox" name="input_status_confirm" id="input_status_confirm"  @if($produk->status=='1') checked @endif data-bootstrap-switch data-off-color="danger" data-on-color="success">
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row" style="float: right;">
      <br>
      <button type="submit" class="btn btn-success">Simpan</button>&nbsp;&nbsp;
      <button type="button" class="btn btn-default"
            data-dismiss="modal">
          Close
      </button>
    </div>
  </div>
  <!-- /.card-body -->
</form>
<div class="card-footer" id="form_tambah_vairan">
  <div class="row">
    <div class="col-md-12">
      <button type="button" class="btn btn-primary" id="btn_add_varian"><span id="label_add_varian"></span></button>
      <button type="button" class="btn btn-success" onclick="reload_tableDetail()" id="refresh_detail"><i class="fa fa-sync"></i> Refresh</button>
      <button type="button" class="btn btn-success" id="btn_save_varian" style="display: none">Simpan Varian</button>&nbsp;&nbsp;
      <button type="button" class="btn btn-default" id="btn_cancel_varian"  style="display: none">Cancel</button>&nbsp;&nbsp;
    </div>
  </div>
</div>
<form role="form" id="form_varian"   enctype="multipart/form-data" method="post" accept-charset="utf-8">
<div class="col-md-12" style="display:none" id="form_variant_id">
  <div class="row" style="padding-top: 10px">
    <div class="col-md-4">
      <div class="form-group">
        <label for="input_detail_nama">Nama Varian</label>
        <input type="text" name="input_detail_nama" id="input_detail_nama" placeholder="Nama Varian" class="form-control">
        <input type="hidden" name="input_detail_action" id="input_detail_action" placeholder="Nama Varian" class="form-control" value="add">
        <input type="hidden" name="input_detail_produk_id" id="input_detail_produk_id" placeholder="ID" class="form-control" value="{{$produk->id}}">
        <input type="hidden" name="input_detail_id" id="input_detail_id" placeholder="ID" class="form-control">
        <span id="error_input_detail_nama" class="error invalid-feedback"></span>
      </div>
    </div>
    <div class="col-md-4">
      <div class="form-group">
        <label for="input_detail_harga_beli">Harga Beli</label>
        <input type="number" name="input_detail_harga_beli" id="input_detail_harga_beli" placeholder="Harga Beli" class="form-control" step="any">
        <span id="error_input_detail_harga_beli" class="error invalid-feedback"></span>
      </div>
    </div>
    <div class="col-md-4">
      <div class="form-group">
        <label for="input_detail_harga_jual">Harga Jual</label>
        <input type="number" name="input_detail_harga_jual" id="input_detail_harga_jual" placeholder="Harga Jual" class="form-control" step="any">
        <span id="error_input_input_detail_harga_jual" class="error invalid-feedback"></span>
      </div>
    </div>
    <div class="col-md-4">
      <div class="form-group">
        <label for="input_detail_stok">Stok</label>
        <input type="number" name="input_detail_stok" id="input_detail_stok" placeholder="Stok" class="form-control" step="any">
        <span id="error_input_detail_stok" class="error invalid-feedback"></span>
      </div>
    </div>
    <div class="col-md-4">
      <div class="form-group">
        <label for="input_detail_tgl_kadaluarsa">Tgl Kadaluarsa</label>
        <input type="text" name="input_detail_tgl_kadaluarsa" id="input_detail_tgl_kadaluarsa" placeholder="Tanggal Kadaluarsa" class="form-control" step="any" autocomplete="off">
        <span id="error_input_detail_tgl_kadaluarsa" class="error invalid-feedback"></span>
      </div>
    </div>
    <div class="col-md-4">
      <div class="form-group" style="padding-top: 5px">
        <label for="input_status_detail">Status</label><br>
        <input type="checkbox" name="input_status_detail" id="input_status_detail"  data-bootstrap-switch data-off-color="danger" data-on-color="success">
      </div>
    </div>
    <div class="col-md-4">
      <div class="form-group">
        <label for="input_detail_foto">Foto</label>
        <div class="input-group">
          <div class="custom-foto">
            <input type="file" id="input_detail_foto" name="input_detail_foto">
            <span id="error_input_detail_foto" class="error invalid-feedback"></span>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</form>
<div class="col-md-12 col-sm-12 table-responsive" id="table_tambah_varian">
    <table id="manage_all_detail" class="table table-collapse table-bordered table-hover  table-striped">
        <thead>
        <tr>
            <th>#</th>
            <th>Foto</th>
            <th>Nama</th>
            <th width="20%">Action</th>
        </tr>
        </thead>
    </table>
</div>
<style type="text/css">
.modal-content {
  max-height: 90%;
  border-radius: 0;
}
.modal-body{
  overflow-y: auto;
}
</style>
<script type="text/javascript">
$(document).ready(function () {
  @if($produk->is_expire!='1')
  $("#input_tgl_kadaluarsa").prop('disabled',true);
  $("#input_detail_tgl_kadaluarsa").prop('disabled',true);
  @endif
  $("input[data-bootstrap-switch]").each(function(){
    $(this).bootstrapSwitch('state', $(this).prop('checked'));
  });
  $("#input_tgl_kadaluarsa").datepicker();
  $("#input_detail_tgl_kadaluarsa").datepicker();
  $('#input_is_expire').on('switchChange.bootstrapSwitch', function (event, state) {
    if($("#input_is_expire").is(':checked')){
      $("#input_tgl_kadaluarsa").prop('disabled',false);
      $("#input_detail_tgl_kadaluarsa").prop('disabled',false);
    }else{
      $("#input_tgl_kadaluarsa").val('');
      $("#input_tgl_kadaluarsa").prop('disabled',true);
      $("#input_detail_tgl_kadaluarsa").val('');
      $("#input_detail_tgl_kadaluarsa").prop('disabled',true);
    }
  });
  $('#input_is_alert').on('switchChange.bootstrapSwitch', function (event, state) {
    if($("#input_is_alert").is(':checked')){
      $("#input_stok_min").prop('disabled',false);
    }else{
      $("#input_stok_min").prop('disabled',true);
    }
  });
   var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
  $("#input_kategori" ).autocomplete({
    source: function( request, response ) {
      $.ajax({
        url:"{{route('admin.autocompleteKategori.kategori')}}",
        type: 'post',
        dataType: "json",
        data: {
           _token: CSRF_TOKEN,
           search: request.term,
        },
        success: function( data ) {
           response( data );
        },
        error: function (result) {
          Swal.fire({
            icon: 'error',
            title: 'Terjadi kesalahan pada koneksi! <br>Pastikan koneksi Anda stabil'
          })
        }
      });
    },
    select: function (event, ui) {
       // Set selection
       $('#input_kategori').val(ui.item.label); // display the selected text
       $('#input_kategori_id').val(ui.item.value); // save selected id to input
       return false;
    }
  }).data( "ui-autocomplete" )._renderItem = function( ul, item ) {
    return $( "<li style='padding-left:10px'>" )
      .data( "ui-autocomplete-item", item )
      .append(item.label)
      .appendTo( ul );
  };
  $("#input_tipe" ).autocomplete({
    source: function( request, response ) {
      $.ajax({
        url:"{{route('admin.autocompleteTipe.tipe')}}",
        type: 'post',
        dataType: "json",
        data: {
           _token: CSRF_TOKEN,
           search: request.term
        },
        success: function( data ) {
           response( data );
        },
        error: function (result) {
          Swal.fire({
            icon: 'error',
            title: 'Terjadi kesalahan pada koneksi! <br>Pastikan koneksi Anda stabil'
          })
        }
      });
    },
    select: function (event, ui) {
       // Set selection
       $('#input_tipe').val(ui.item.label); // display the selected text
       $('#input_tipe_id').val(ui.item.value); // save selected id to input
       return false;
    }
  }).data( "ui-autocomplete" )._renderItem = function( ul, item ) {
    return $( "<li style='padding-left:10px'>" )
      .data( "ui-autocomplete-item", item )
      .append(item.label)
      .appendTo( ul );
  };
  $("#input_satuan" ).autocomplete({
    source: function( request, response ) {
      $.ajax({
        url:"{{route('admin.autocompleteSatuan.satuan')}}",
        type: 'post',
        dataType: "json",
        data: {
           _token: CSRF_TOKEN,
           search: request.term,
        },
        success: function( data ) {
           response( data );
        },
        error: function (result) {
          Swal.fire({
            icon: 'error',
            title: 'Terjadi kesalahan pada koneksi! <br>Pastikan koneksi Anda stabil'
          })
        }
      });
    },
    select: function (event, ui) {
       // Set selection
       $('#input_satuan').val(ui.item.label); // display the selected text
       $('#input_satuan_id').val(ui.item.value); // save selected id to input
       return false;
    }
  }).data( "ui-autocomplete" )._renderItem = function( ul, item ) {
    return $( "<li style='padding-left:10px'>" )
      .data( "ui-autocomplete-item", item )
      .append(item.label)
      .appendTo( ul );
  };
  $('#edit').validate({
    submitHandler: function (form) {
        var myData = new FormData($("#edit")[0]);
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        myData.append('_token', CSRF_TOKEN);
        $.ajax({
            url: 'produk/{{$produk->id}}',
            type: 'POST',
            data: myData,
            dataType: 'json',
            cache: false,
            processData: false,
            contentType: false,
            beforeSend: function () {
                $("#submit").prop('disabled', true); // disable button
                $("#modal-overlay").addClass('overlay d-flex justify-content-center align-items-center');
                $("#modal-overlay-content").addClass('fas fa-2x fa-sync fa-spin');
            },
            success: function (data) {
                if (data.type === 'success') {
                    reload_table();
                    notify_view(data.type, data.message);
                    $("#modal-overlay").removeClass();
                    $("#modal-overlay-content").removeClass();
                    $("#submit").prop('disabled', false); // disable button
                    $('#myModal').modal('hide'); // hide bootstrap modal

                } else if (data.type === 'error') {
                    if (data.errors) {
                        $.each(data.errors, function (key, val) {
                            $('#error_' + key).html(val);
                            $("#"+key).addClass('is-invalid');
                        });
                    }
                    $("#status").html(data.message);
                    $("#modal-overlay").removeClass();
                    $("#modal-overlay-content").removeClass();
                    $("#submit").prop('disabled', false); // disable button
                }
            },
            error: function (result) {
              $("#modal-overlay").removeClass();
              $("#modal-overlay-content").removeClass();
              Swal.fire({
                icon: 'error',
                title: 'Terjadi kesalahan pada koneksi! <br>Pastikan koneksi Anda stabil'
              })
            }
        });
    }
  });
});

$(document).ready(function () {
  getValidateVarian();
  $("#label_add_varian").html('Tambah Varian');
  table_produkDetail = $("#manage_all_detail").DataTable({
    processing: true,
    serverSide: true,
    ajax: {
        url: '{!! route('admin.allProduk.produk') !!}',
        type: "GET",
        data: function (d) {
              d.produk_id = '{{$produk->id}}';
        },
    },
    "columnDefs": [
      { 
        "targets": [ -1,0 ], //last column
        "orderable": false //set not orderable
      }
    ],
    "autoWidth": false,
  });
  $('.dataTables_filter input[type="search"]').attr('placeholder', 'Type here to search...').css({
      'width': '220px',
      'height': '30px'
  });
});
function reload_tableDetail() {
    table_produkDetail.ajax.reload(null, false); //reload datatable ajax
}
$("#btn_add_varian").click(function(){
  $("#form_variant_id").show('slow');
  $("#btn_save_varian").show();
  $("#btn_cancel_varian").show();
  $("#btn_add_varian").hide();
  $("#refresh_detail").hide();
  $("#harga_detail").hide();
  $("#expire_detail").hide();
});
$("#btn_cancel_varian").click(function(){
  $("#btn_add_varian").show();
  $("#refresh_detail").show();
  $("#form_variant_id").hide('slow');
  $("#btn_save_varian").hide();
  $("#btn_cancel_varian").hide();
  getValidateVarian();
  clearForm();
});
$("#btn_save_varian").click(function(){
  if($("#input_detail_action").val()!='edit'){
    var myDataDetail = new FormData($("#form_varian")[0]);
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    myDataDetail.append('_token', CSRF_TOKEN);
    myDataDetail.append('input_is_expire', ($("#input_is_expire").is(':checked') ? 'on' : ''));
    $.ajax({
      url: 'produkDetail',
      type: 'POST',
      data: myDataDetail,
      dataType: 'json',
      cache: false,
      processData: false,
      contentType: false,
      beforeSend: function () {
          $("#btn_save_varian").prop('disabled', true); // disable button
      },
      success: function (data) {
          getValidateVarian();
          if (data.type === 'success') {
              $("#label_add_varian").html('Tambah Varian');
              reload_tableDetail();
              notify_view(data.type, data.message);
              $("#btn_add_varian").show();
              $("#refresh_detail").show();
              $("#form_variant_id").hide('slow');
              $("#btn_save_varian").hide();
              $("#btn_cancel_varian").hide();
              $("#btn_save_varian").prop('disabled', false); // disable button
              clearForm();
          } else if (data.type === 'error') {
              if (data.errors) {
                  $.each(data.errors, function (key, val) {
                      $('#error_' + key).html(val);
                      console.log("#"+key);
                      $("#"+key).addClass('is-invalid');
                  });
              }else{
                Swal.fire({
                  icon: 'error',
                  title: data.message
                })
              }
              $("#btn_save_varian").prop('disabled', false); // disable button
          }

      },
      error: function (result) {
        $("#btn_save_varian").prop('disabled', false); // disable button
        Swal.fire({
          icon: 'error',
          title: 'Terjadi kesalahan pada koneksi! <br>Pastikan koneksi Anda stabil'
        })
      }
    });
  }else{
    var myDataDetail = new FormData($("#form_varian")[0]);
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    myDataDetail.append('_token', CSRF_TOKEN);
    myDataDetail.append('_method', 'PATCH');
    myDataDetail.append('input_is_expire', ($("#input_is_expire").is(':checked') ? 'on' : ''));
    $.ajax({
        url: 'produkDetail/'+$("#input_detail_id").val(),
        type: 'POST',
        data: myDataDetail,
        dataType: 'json',
        cache: false,
        processData: false,
        contentType: false,
        beforeSend: function () {
            $("#btn_save_varian").prop('disabled', true); // disable button
        },
        success: function (data) {
            getValidateVarian();
            if (data.type === 'success') {
              reload_tableDetail();
              notify_view(data.type, data.message);
              $("#btn_add_varian").show();
              $("#refresh_detail").show();
              $("#form_variant_id").hide('slow');
              $("#btn_save_varian").hide();
              $("#btn_cancel_varian").hide();
              $("#btn_save_varian").prop('disabled', false); // disable button
              clearForm();
            } else if (data.type === 'error') {
                if (data.errors) {
                    $.each(data.errors, function (key, val) {
                        $('#error_' + key).html(val);
                        $("#"+key).addClass('is-invalid');
                    });
                }else{
                Swal.fire({
                  icon: 'error',
                  title: data.message
                })
              }
                $("#btn_save_varian").prop('disabled', false); // disable button
            }
        },
        error: function (result) {
          $("#btn_save_varian").prop('disabled', false); // disable button
          Swal.fire({
            icon: 'error',
            title: 'Terjadi kesalahan pada koneksi! <br>Pastikan koneksi Anda stabil'
          })
        }
    });
  }
});
$("#manage_all_detail").on("click", ".edit", function () {
  $("#label_add_varian").html('Edit Varian');
  $("#btn_add_varian").hide();
  $("#refresh_detail").hide();
  $("#form_variant_id").show('slow');
  $("#btn_save_varian").show();
  $("#btn_cancel_varian").show();
  var id = $(this).attr('id');
  var proudk_id = $("#input_detail_produk_id").val();
  clearForm();
  $.ajax({
      url: 'produkDetail/' + id+'__'+proudk_id + '/edit',
      type: 'get',
      success: function (data) {
        getValidateVarian();
        $("#input_detail_action").val('edit');
        $("#input_detail_id").val(data.id);
        $("#input_detail_nama").val(data.nama);
        $("#input_detail_stok").val(data.stok);
        $("#input_detail_harga_jual").val(data.harga_jual);
        $("#input_detail_harga_beli").val(data.harga_beli);
        if(data.is_expire=='1'){
          $("#input_detail_tgl_kadaluarsa").val(data.tgl_kadaluarsa);
        }
        $("#input_detail_bahan_id").val(data.bahan_id);
        $("#label_add_varian").html('Edit Varian');
      },
      error: function (result) {
        $("#label_add_varian").html('Edit Varian');
        Swal.fire({
          icon: 'error',
          title: 'Terjadi kesalahan pada koneksi! <br>Pastikan koneksi Anda stabil'
        })
      }
  });
});
$("#manage_all_detail").on("click", ".delete", function () {
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    var id = $(this).attr('id');
    
    Swal.fire({
      title: 'Apakah kamu yakin?',
      text: "Data tidak bisa dikembalikan",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      cancelButtonText: 'Batal',
      confirmButtonText: 'Ya, Hapus!'
    }).then((result) => {
      if (result.value) {
        $.ajax({
            url: 'produkDetail/' + id,
            data: {"_token": CSRF_TOKEN,"input_detail_produk_id": $("#input_detail_produk_id").val()},
            type: 'DELETE',
            dataType: 'json',
            success: function (data) {
                getValidateVarian();
                if (data.type === 'success') {
                    Swal.fire(
                      'Selesai!',
                      'Data berhasil dihapus',
                      'success'
                    );
                reload_tableDetail();
                } else if (data.type === 'danger') {
                    Swal.fire("Kesalahan!", "Data gagal dihapus", "error");
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                swal("Error deleting!", "Try again", "error");
            }
        });
      }
    })
});
function clearForm(){
  $('#input_detail_nama').val('');
  $('#input_detail_stok').val('');
  $('#input_detail_harga_jual').val('');
  $('#input_detail_harga_beli').val('');
  $('#input_detail_foto').val('');
}
function getValidateVarian(){
  var base_url = "{!! url('/') !!}";
  $.ajax({
      url: base_url+'/admin_bqs/pembelian/getValidateVarian/{{$produk->id}}',
      type: 'get',
      success: function (data) {
        let res = JSON.parse(data);
        if(!res.has_history){
          $("#form_tambah_vairan").show();
          $("#table_tambah_varian").show();

        }else{
          $("#table_tambah_varian").hide();
          $("#form_tambah_vairan").hide();
        }
        let dt_child = res.child;
        $("#span_varian_pembelian").html('');
        if(dt_child.length > 0){
          $("#harga_detail").hide();
          $("#expire_detail").hide();
        }else{
          $("#harga_detail").show();
          $("#expire_detail").show();
        }
        $("#tampil_stok").html(res.stok);
      },
      error: function (result) {
        $("#modal-overlay").removeClass();
        $("#modal-overlay-content").removeClass();
        Swal.fire({
          icon: 'error',
          title: 'Terjadi kesalahan pada koneksi! <br>Pastikan koneksi Anda stabil'
        })
      }
  });
}
</script>