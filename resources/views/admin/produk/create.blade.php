<form role="form" id="create"   enctype="multipart/form-data" method="post" accept-charset="utf-8">
  <div class="card-body">
    <div class="row" style="padding-top: 10px">
      <div class="col-md-4">
        <img src="{{ asset('admin/dist/img/imageDefault.jpg') }}" class="product-image" alt="Product Image"  style="height: 220px">
      </div>
      <div class="col-md-8">
        <div class="form-group">
          <label for="input_nama">Nama Barang</label>
          <input type="text" name="input_nama" id="input_nama" placeholder="Nama Barang" class="form-control">
          <span id="error_input_nama" class="error invalid-feedback"></span>
        </div>
        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <label for="input_foto">Foto</label>
              <div class="input-group">
                <div class="custom-file">
                  <input type="file" id="input_foto" name="input_foto">
                  <span id="error_input_foto" class="error invalid-feedback"></span>
                </div>
              </div>
            </div>
            <div class="form-group">
              <label for="input_tipe">Tipe</label>
              <div class="row">
                <div class="col-md-10">
                  <input type="text" class="form-control" id="input_tipe" placeholder="Pilih Tipe" name="input_tipe" autocomplete="off">
                  <span id="error_input_tipe" class="error invalid-feedback"></span>
                  <input type="hidden" id='input_tipe_id' name="input_tipe_id" readonly class="form-control" >
                </div>
                <div class="col-md-2" style="padding-top: 3px">
                  <a href="{{ URL :: to('/admin_bqs/tipe') }}" target="_blank">
                  <button class="btn btn-success" type="button"><i class="fa fa-plus"></i></button>
                  </a>
                </div>
              </div>
            </div>
            <div class="form-group">
              <label for="input_kategori">Kategori</label>
              <div class="row">
                <div class="col-md-10">
                  <input type="text" class="form-control" id="input_kategori" placeholder="Pilih Kategori" name="input_kategori" autocomplete="off">
                  <span id="error_input_kategori" class="error invalid-feedback"></span>
                  <input type="hidden" id='input_kategori_id' name="input_kategori_id" readonly class="form-control" >
                </div>
                <div class="col-md-2" style="padding-top: 3px">
                  <a href="{{ URL :: to('/admin_bqs/kategori') }}" target="_blank">
                  <button class="btn btn-success" type="button"><i class="fa fa-plus"></i></button>
                  </a>
                </div>
              </div>
            </div>
            <div class="form-group">
              <label for="input_harga_beli">Harga Beli</label>
              <input type="number" name="input_harga_beli" id="input_harga_beli" placeholder="Harga Beli" class="form-control" step="any">
            </div>
            <div class="form-group">
              <label for="input_harga_jual">Harga Jual</label>
              <input type="number" name="input_harga_jual" id="input_harga_jual" placeholder="Harga Jual" class="form-control" step="any">
              <span id="error_input_harga_jual" class="error invalid-feedback"></span>
            </div>
            <div class="form-group">
              <label for="input_diskon">Diskon</label>
              <input type="number" name="input_diskon" id="input_diskon" placeholder="Diskon" class="form-control" step="any">
              <span id="error_input_diskon" class="error invalid-feedback"></span>
            </div>
            <!-- <div class="form-group"  style="padding-top: 5px">
              <label for="input_is_tracking">Is Tracking</label><br>
              <input type="checkbox" name="input_is_tracking" id="input_is_tracking" checked data-bootstrap-switch data-off-color="danger" data-on-color="success">
            </div> -->
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label for="input_stok">Stok</label>
              <input type="number" name="input_stok" id="input_stok" placeholder="Stok" class="form-control" step="any">
              <span id="error_input_stok" class="error invalid-feedback"></span>
            </div>
            <div class="form-group">
              <label for="input_satuan">Satuan</label>
              <div class="row">
                <div class="col-md-10">
                  <input type="text" class="form-control" id="input_satuan" placeholder="Pilih Satuan" name="input_satuan" autocomplete="off">
                  <span id="error_input_satuan" class="error invalid-feedback"></span>
                  <input type="hidden" id='input_satuan_id' name="input_satuan_id" readonly class="form-control" >
                </div>
                <div class="col-md-2" style="padding-top: 3px">
                  <a href="{{ URL :: to('/admin_bqs/satuan') }}" target="_blank">
                  <button class="btn btn-success" type="button"><i class="fa fa-plus"></i></button>
                  </a>
                </div>
              </div>
            </div>
            <!-- <div class="form-group"  style="padding-top: 5px">
              <label for="input_is_alert">Is Alert</label><br>
              <input type="checkbox" name="input_is_alert" id="input_is_alert" checked data-bootstrap-switch data-off-color="danger" data-on-color="success">
            </div> -->
            <div class="form-group">
              <label for="input_stok_min">Stok Minimal</label>
              <input type="number" name="input_stok_min" id="input_stok_min" placeholder="Stok Minimal" class="form-control" step="any">
              <span id="error_input_stok_min" class="error invalid-feedback"></span>
            </div>
            <div class="form-group"  style="padding-top: 5px">
              <label for="input_is_expire">Is Expire</label><br>
              <input type="checkbox" name="input_is_expire" id="input_is_expire" checked data-bootstrap-switch data-off-color="danger" data-on-color="success">
            </div>
            <div class="form-group">
              <label for="input_tgl_kadaluarsa">Tgl Kadaluarsa</label><br>
              <input type="text" id="input_tgl_kadaluarsa" class="form-control" name="input_tgl_kadaluarsa" autocomplete="off">
            </div>
            <div class="form-group"  style="padding-top: 5px">
              <label for="input_is_stok">Status</label><br>
              <input type="checkbox" name="input_status_confirm" id="input_status_confirm"  checked data-bootstrap-switch data-off-color="danger" data-on-color="success">
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- /.card-body -->

  <div class="card-footer">
    <button type="submit" class="btn btn-success">Simpan</button>
    <button type="button" class="btn btn-default"
            data-dismiss="modal">
        Close
    </button>
  </div>
</form>
<script type="text/javascript">
$(document).ready(function () {
  $("#input_tgl_kadaluarsa").datepicker();
  $('#input_is_expire').on('switchChange.bootstrapSwitch', function (event, state) {
    if($("#input_is_expire").is(':checked')){
      $("#input_tgl_kadaluarsa").prop('disabled',false);
    }else{
      $("#input_tgl_kadaluarsa").val('');
      $("#input_tgl_kadaluarsa").prop('disabled',true);
    }
  });
  $('#input_is_alert').on('switchChange.bootstrapSwitch', function (event, state) {
    if($("#input_is_alert").is(':checked')){
      $("#input_stok_min").prop('disabled',false);
    }else{
      $("#input_stok_min").prop('disabled',true);
    }
  });
  $("input[data-bootstrap-switch]").each(function(){
    $(this).bootstrapSwitch('state', $(this).prop('checked'));
  });
  var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
  $("#input_kategori" ).autocomplete({
    source: function( request, response ) {
      $.ajax({
        url:"{{route('admin.autocompleteKategori.kategori')}}",
        type: 'post',
        dataType: "json",
        data: {
           _token: CSRF_TOKEN,
           search: request.term,
        },
        success: function( data ) {
           response( data );
        },
        error: function (result) {
          Swal.fire({
            icon: 'error',
            title: 'Terjadi kesalahan pada koneksi! <br>Pastikan koneksi Anda stabil'
          })
        }
      });
    },
    select: function (event, ui) {
       // Set selection
       $('#input_kategori').val(ui.item.label); // display the selected text
       $('#input_kategori_id').val(ui.item.value); // save selected id to input
       return false;
    }
  }).data( "ui-autocomplete" )._renderItem = function( ul, item ) {
    return $( "<li style='padding-left:10px'>" )
      .data( "ui-autocomplete-item", item )
      .append(item.label)
      .appendTo( ul );
  };
  $("#input_tipe" ).autocomplete({
    source: function( request, response ) {
      $.ajax({
        url:"{{route('admin.autocompleteTipe.tipe')}}",
        type: 'post',
        dataType: "json",
        data: {
           _token: CSRF_TOKEN,
           search: request.term
        },
        success: function( data ) {
           response( data );
        },
        error: function (result) {
          Swal.fire({
            icon: 'error',
            title: 'Terjadi kesalahan pada koneksi! <br>Pastikan koneksi Anda stabil'
          })
        }
      });
    },
    select: function (event, ui) {
       // Set selection
       $('#input_tipe').val(ui.item.label); // display the selected text
       $('#input_tipe_id').val(ui.item.value); // save selected id to input
       return false;
    }
  }).data( "ui-autocomplete" )._renderItem = function( ul, item ) {
    return $( "<li style='padding-left:10px'>" )
      .data( "ui-autocomplete-item", item )
      .append(item.label)
      .appendTo( ul );
  };
  $("#input_satuan" ).autocomplete({
    source: function( request, response ) {
      $.ajax({
        url:"{{route('admin.autocompleteSatuan.satuan')}}",
        type: 'post',
        dataType: "json",
        data: {
           _token: CSRF_TOKEN,
           search: request.term,
        },
        success: function( data ) {
           response( data );
        },
        error: function (result) {
          Swal.fire({
            icon: 'error',
            title: 'Terjadi kesalahan pada koneksi! <br>Pastikan koneksi Anda stabil'
          })
        }
      });
    },
    select: function (event, ui) {
       // Set selection
       $('#input_satuan').val(ui.item.label); // display the selected text
       $('#input_satuan_id').val(ui.item.value); // save selected id to input
       return false;
    }
  }).data( "ui-autocomplete" )._renderItem = function( ul, item ) {
    return $( "<li style='padding-left:10px'>" )
      .data( "ui-autocomplete-item", item )
      .append(item.label)
      .appendTo( ul );
  };
  $('#create').validate({
    submitHandler: function (form) {
        var myData = new FormData($("#create")[0]);
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        myData.append('_token', CSRF_TOKEN);

        $.ajax({
            url: 'produk',
            type: 'POST',
            data: myData,
            dataType: 'json',
            cache: false,
            processData: false,
            contentType: false,
            beforeSend: function () {
                $("#submit").prop('disabled', true); // disable button
                $("#modal-overlay").addClass('overlay d-flex justify-content-center align-items-center');
                $("#modal-overlay-content").addClass('fas fa-2x fa-sync fa-spin');
            },
            success: function (data) {
                if (data.type === 'success') {
                    reload_table();
                    notify_view(data.type, data.message);
                    $("#modal-overlay").removeClass();
                    $("#modal-overlay-content").removeClass();
                    $("#submit").prop('disabled', false); // disable button
                    $('#myModal').modal('hide'); // hide bootstrap modal

                } else if (data.type === 'error') {
                    if (data.errors) {
                        $.each(data.errors, function (key, val) {
                            $('#error_' + key).html(val);
                            $("#"+key).addClass('is-invalid');
                        });
                    }
                    $("#status").html(data.message);
                    $("#modal-overlay").removeClass();
                    $("#modal-overlay-content").removeClass();
                    $("#submit").prop('disabled', false); // disable button
                }
            },
            error: function (result) {
              $("#modal-overlay").removeClass();
              $("#modal-overlay-content").removeClass();
              Swal.fire({
                icon: 'error',
                title: 'Terjadi kesalahan pada koneksi! <br>Pastikan koneksi Anda stabil'
              })
            }
        });
    }
  });
});
</script>