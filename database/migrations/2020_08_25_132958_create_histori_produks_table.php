<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHistoriProduksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('histori_produk', function (Blueprint $table) {
            $table->id();
            $table->string('link');
            $table->enum('jenis',['pembelian','pengeluaran','kasir','stokOpname']);
            $table->string('produk_id',15);
            $table->string('stok_awal');
            $table->string('stok_akhir');
            $table->string('stok_selisih');
            $table->double('harga',11,2);
            $table->integer('time');
            $table->text('keterangan')->nullable();


            $table->foreign('produk_id')->references('id')->on('trans_produk');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('histori_produk');
    }
}
