<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProduksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trans_produk', function (Blueprint $table) {
            $table->string('id',15);
            $table->string('sub_id',15)->nullable();
            $table->string('nama');
            $table->string('foto');
            $table->double('stok',11,2);
            $table->double('harga_beli',11,2);
            $table->double('harga_jual',11,2);
            $table->char('is_expire',1);
            $table->date('tgl_kadaluarsa')->nullable();
            $table->integer('diskon')->nullable();
            $table->char('is_tracking',1);
            $table->char('is_alert',1);
            $table->double('stok_min',11,2);
            $table->integer('total_terbeli')->nullable();
            $table->char('status',1);
            $table->unsignedBigInteger('satuan_id');
            $table->string('user_id',15);

            $table->timestamps();
            $table->softDeletes();
            $table->primary('id');
            $table->foreign('satuan_id')->references('id')->on('mst_satuan');
            $table->foreign('user_id')->references('id')->on('app_user_list');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trans_produk');
    }
}
