<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStokOpnameTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trans_stok_opname', function (Blueprint $table) {
            $table->string('id',15);
            $table->date('tanggal');
            $table->double('total_nilai',11,2);
            $table->integer('total_item');
            $table->char('status',1);
            $table->text('keterangan');
            $table->string('user_id',15);

            $table->foreign('user_id')->references('id')->on('app_user_list');
            $table->timestamps();
            $table->softDeletes();
            $table->primary('id');
        });
        Schema::create('trans_stok_opname_detail', function (Blueprint $table) {
            $table->string('id',5);
            $table->string('stok_opname_id',15);
            $table->string('produk_id',15);
            $table->integer('jumlah');
            $table->integer('jumlah_fisik');
            $table->integer('selisih');
            $table->double('harga',11,2);
            $table->text('keterangan');


            $table->foreign('produk_id')->references('id')->on('trans_produk');
            $table->timestamps();
            $table->softDeletes();
            $table->primary(['id','stok_opname_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trans_stok_opname_detail');
        Schema::dropIfExists('trans_stok_opname');
    }
}
