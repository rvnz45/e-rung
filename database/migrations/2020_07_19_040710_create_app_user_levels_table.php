<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAppUserLevelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('app_user_level', function (Blueprint $table) {
            $table->string('level',15);
            $table->string('value');

            $table->softDeletes();
            $table->primary('level');
        });
        Schema::table('app_user_acces', function(Blueprint $table)
        {
            $table->string('app_user_level_id',15);
            $table->foreign('app_user_level_id')->references('level')->on('app_user_level');
        });
        Schema::table('app_user_list', function(Blueprint $table)
        {
            $table->string('app_user_level_id',15);
            $table->foreign('app_user_level_id')->references('level')->on('app_user_level');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('app_user_level');
    }
}
