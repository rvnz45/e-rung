<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKecamatansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mst_kecamatan', function (Blueprint $table) {
            $table->string('id',15);
            $table->string('nama');
            $table->string('latitude')->nullable();
            $table->string('longitude')->nullable();
            $table->string('kota_id',15);

            $table->softDeletes();
            $table->primary('id');
            $table->foreign('kota_id')->references('id')->on('mst_kota');
        });
        Schema::table('app_user_list', function(Blueprint $table)
        {
            $table->string('kecamatan_id',15)->nullable();
            $table->foreign('kecamatan_id')->references('id')->on('mst_kecamatan');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mst_kecamatan');
    }
}
