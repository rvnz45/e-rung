<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKasirsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trans_kasir_lock', function (Blueprint $table) {
            $table->string('id',15);
            $table->date('tanggal');
            $table->integer('time');
            $table->double('nilai_awal',11,2);
            $table->double('nilai_akhir',11,2)->nullable();
            $table->double('nilai_fisik',11,2)->nullable();
            $table->char('status',1)->nullable();
            $table->text('keterangan')->nullable();
            $table->string('user_id',15);

            $table->foreign('user_id')->references('id')->on('app_user_list');
            $table->timestamps();
            $table->softDeletes();
            $table->primary('id');
        });
        Schema::create('trans_kasir_detail_temp', function (Blueprint $table) {
            $table->string('id',5);
            $table->string('kasir_lock_id',15);
            $table->string('produk_id',15);
            $table->integer('jumlah')->nullable();
            $table->double('harga_beli',11,2)->nullable();
            $table->double('harga_jual',11,2)->nullable();
            $table->integer('diskon')->nullable();
            $table->double('harga_terjual',11,2)->nullable();
            $table->text('keterangan')->nullable();


            $table->foreign('kasir_lock_id')->references('id')->on('trans_kasir_lock');
            // $table->foreign('pengeluaran_id')->references('id')->on('trans_pengeluaran');
            $table->timestamps();
            $table->softDeletes();
            $table->primary(['id','kasir_lock_id']);
        });
        Schema::create('trans_kasir', function (Blueprint $table) {
            $table->string('id',15);
            $table->dateTime('tanggal');
            $table->integer('time');
            $table->string('pembeli')->nullable();
            $table->integer('total_item')->nullable();
            $table->double('total_asal',11,2)->nullable();
            $table->double('total_nilai',11,2)->nullable();
            $table->double('diskon',11,2)->nullable();
            $table->double('total_bayar',11,2)->nullable();
            $table->double('total_terbayar',11,2)->nullable();
            $table->double('total_hutang',11,2)->nullable();
            $table->text('keterangan')->nullable();
            $table->string('kasir_lock_id',15);
            $table->string('user_id',15);

            $table->foreign('user_id')->references('id')->on('app_user_list');
            $table->foreign('kasir_lock_id')->references('id')->on('trans_kasir_lock');
            $table->timestamps();
            $table->softDeletes();
            $table->primary('id');
        });
        Schema::create('trans_kasir_detail', function (Blueprint $table) {
            $table->string('id',5);
            $table->string('kasir_id',15);
            $table->string('produk_id',15);
            $table->integer('jumlah')->nullable();
            $table->double('harga_beli',11,2)->nullable();
            $table->double('harga_jual',11,2)->nullable();
            $table->integer('diskon')->nullable();
            $table->double('harga_terjual',11,2)->nullable();
            $table->text('keterangan')->nullable();


            $table->foreign('kasir_id')->references('id')->on('trans_kasir');
            // $table->foreign('pengeluaran_id')->references('id')->on('trans_pengeluaran');
            $table->timestamps();
            $table->softDeletes();
            $table->primary(['id','kasir_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trans_kasir_detail_temp');
        Schema::dropIfExists('trans_kasir_detail');
        Schema::dropIfExists('trans_kasir');
        Schema::dropIfExists('trans_kasir_lock');
    }
}
