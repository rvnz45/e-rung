<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAppThemesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('app_theme', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('folder');

            $table->softDeletes();
        });
        Schema::table('app_file', function(Blueprint $table)
        {
            $table->unsignedBigInteger('theme_id')->unsigned();
            $table->foreign('theme_id')->references('id')->on('app_theme');
        });

        Schema::table('app_menu', function(Blueprint $table)
        {
            $table->unsignedBigInteger('theme_id')->unsigned();
            $table->foreign('theme_id')->references('id')->on('app_theme');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('app_theme');
    }
}
