<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKotasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mst_kota', function (Blueprint $table) {
            $table->string('id',15);
            $table->string('nama');
            $table->string('latitude')->nullable();
            $table->string('longitude')->nullable();
            $table->string('provinsi_id',15);

            $table->softDeletes();
            $table->primary('id');
            $table->foreign('provinsi_id')->references('id')->on('mst_provinsi');
        });
        Schema::table('app_user_list', function(Blueprint $table)
        {
            $table->string('kota_id',15)->nullable();
            $table->foreign('kota_id')->references('id')->on('mst_kota');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mst_kota');
    }
}
