<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProvinsisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mst_provinsi', function (Blueprint $table) {
            $table->string('id',15);
            $table->string('nama');
            $table->string('latitude')->nullable();
            $table->string('longitude')->nullable();

            $table->softDeletes();
            $table->primary('id');
        });
        Schema::table('app_user_list', function(Blueprint $table)
        {
            $table->string('provinsi_id',15)->nullable();
            $table->foreign('provinsi_id')->references('id')->on('mst_provinsi');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mst_provinsi');
    }
}
