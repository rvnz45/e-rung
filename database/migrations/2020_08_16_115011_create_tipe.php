<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTipe extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mst_tipe', function (Blueprint $table) {
            $table->string('id',2);
            $table->string('nama');
            $table->char('status',1);
            
            $table->softDeletes();
            $table->primary('id');
        });
        Schema::table('trans_produk', function(Blueprint $table)
        {
            $table->string('tipe_id',2);
            $table->foreign('tipe_id')->references('id')->on('mst_tipe');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mst_tipe');
    }
}
