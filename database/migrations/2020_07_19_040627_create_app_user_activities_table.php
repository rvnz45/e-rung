<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAppUserActivitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('app_user_activity', function (Blueprint $table) {
            $table->id();
            $table->text('activity');
            $table->integer('dtime');
            $table->string('user_id',15);

            $table->softDeletes();
            $table->foreign('user_id')->references('id')->on('app_user_list');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('app_user_activity');
    }
}
