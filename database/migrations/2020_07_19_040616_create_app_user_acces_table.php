<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAppUserAccesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('app_user_acces', function (Blueprint $table) {
            $table->id();
            $table->integer('doshow');
            $table->integer('doadd');
            $table->integer('doedit');
            $table->integer('dodel');

            $table->softDeletes();
            $table->unsignedBigInteger('file_id');
            $table->foreign('file_id')->references('id')->on('app_file');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('app_user_acces');
    }
}
