<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAppLangsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('app_lang', function (Blueprint $table) {
            $table->string('id',3);
            $table->string('lang');
            
            $table->softDeletes();
            $table->primary('id');
        });
        Schema::table('app_file', function(Blueprint $table)
        {
            $table->string('lang_id',3);
            $table->foreign('lang_id')->references('id')->on('app_lang');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('app_lang');
    }
}
