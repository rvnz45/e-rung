<?php
use App\User;
use App\Models\App_config;
use App\Models\App_lang;
use App\Models\App_user_level;
use App\Models\App_theme;
use App\Models\App_file;
use App\Models\App_menu;
use App\Models\Masterdata\Kategori;
use App\Models\App_user_acces;
use Illuminate\Database\Seeder;
use Haruncpi\LaravelIdGenerator\IdGenerator;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        App_config::create([
            'id'   => 'name_application',
            'value' => 'OLSHOP BQS'
        ]);
        App_config::create([
            'id'    => 'nama_toko',
            'value' => 'OLSHOP BQS'
        ]);
        App_config::create([
            'id'   => 'pembukaan_toko',
            'value' => 'Selamat Datang di OLSHOP BQS'
        ]);
        App_config::create([
            'id'   => 'pembukaan_toko_tambahan',
            'value' => 'OLSHOP Terpercaya dan dijamin aman 100%'
        ]);
        App_config::create([
            'id'    => 'btn_action',
            'value' => 'CALL TO ACTION'
        ]);
        App_config::create([
            'id'    => 'desc_action',
            'value' => 'Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
        ]);
        App_config::create([
            'id'    => 'desc_contact',
            'value' => 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque'
        ]);
        App_config::create([
            'id'    => 'desc_detail_aboutUs1',
            'value' => 'Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi'
        ]);
        App_config::create([
            'id'    => 'desc_detail_aboutUs2',
            'value' => 'Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum'
        ]);
        
        App_config::create([
            'id'    => 'background_login',
            'value' => 'login/images/logo.svg'
        ]);
        App_config::create([
            'id'    => 'desc_detail_aboutUs3',
            'value' => 'Minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat tarad limino ata'
        ]);
        App_config::create([
            'id'    => 'title_login',
            'value' => 'Selamat Datang e-warung'
        ]);
        App_config::create([
            'id'    => 'desc_detail_service1',
            'value' => 'Voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident'
        ]);
        App_config::create([
            'id'    => 'desc_detail_service2',
            'value' => 'Minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat tarad limino ata'
        ]);
        App_config::create([
            'id'    => 'desc_detail_service3',
            'value' => 'Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur'
        ]);
        App_config::create([
            'id'    => 'desc_detail_service4',
            'value' => 'Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum'
        ]);
        App_config::create([
            'id'    => 'desc_detail_service5',
            'value' => 'At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque'
        ]);
        App_config::create([
            'id'    => 'desc_detail_service6',
            'value' => 'Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi'
        ]);
        App_config::create([
            'id'    => 'desc_service',
            'value' => 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque'
        ]);
        App_config::create([
            'id'    => 'desc_tim',
            'value' => 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque'
        ]);
        App_config::create([
            'id'    => 'desc_title_aboutUs',
            'value' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. '
        ]);
        App_config::create([
            'id'    => 'desc_title_fact',
            'value' => 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque'
        ]);
        App_config::create([
            'id'    => 'detail_aboutUs1',
            'value' => 'Eiusmod Tempor'
        ]);
        App_config::create([
            'id'    => 'detail_aboutUs2',
            'value' => 'Magni Dolores'
        ]);
        App_config::create([
            'id'    => 'detail_aboutUs3',
            'value' => 'Dolor Sitema'
        ]);
        App_config::create([
            'id'    => 'detail_fact_angka1',
            'value' => '232'
        ]);
        App_config::create([
            'id'    => 'detail_fact_angka2',
            'value' => '521'
        ]);
        App_config::create([
            'id'    => 'detail_fact_angka3',
            'value' => '1,463'
        ]);
        App_config::create([
            'id'    => 'detail_fact_angka4',
            'value' => '15'
        ]);
        App_config::create([
            'id'    => 'detail_fact_title1',
            'value' => 'Clients'
        ]);
        App_config::create([
            'id'    => 'detail_fact_title2',
            'value' => 'Projects'
        ]);
        App_config::create([
            'id'    => 'detail_fact_title3',
            'value' => 'Hours Of Support'
        ]);
        App_config::create([
            'id'    => 'detail_fact_title4',
            'value' => 'Hard Workers'
        ]);
        App_config::create([
            'id'    => 'detail_service1',
            'value' => 'Lorem Ipsum'
        ]);
        App_config::create([
            'id'    => 'detail_service2',
            'value' => 'Dolor Sitema'
        ]);
        App_config::create([
            'id'    => 'detail_service3',
            'value' => 'Sed ut perspiciatis'
        ]);
        App_config::create([
            'id'    => 'detail_service4',
            'value' => 'Magni Dolores'
        ]);
        App_config::create([
            'id'    => 'detail_service5',
            'value' => 'Nemo Enim'
        ]);
        App_config::create([
            'id'    => 'detail_service6',
            'value' => 'Eiusmod Tempor'
        ]);
        App_config::create([
            'id'    => 'img_aboutUs',
            'value' => 'landing_page/img/about-img.jpg'
        ]);
        App_config::create([
            'id'    => 'img_tim1',
            'value' => 'landing_page/img/team-1.jpg'
        ]);
        App_config::create([
            'id'    => 'img_tim2',
            'value' => 'landing_page/img/team-2.jpg'
        ]);

        App_config::create([
            'id'    => 'img_tim3',
            'value' => 'landing_page/img/team-3.jpg'
        ]);
        App_config::create([
            'id'    => 'img_tim4',
            'value' => 'landing_page/img/team-4.jpg'
        ]);
        App_config::create([
            'id'    => 'jabatan_img_tim1',
            'value' => 'Chief Executive Officer'
        ]);
        App_config::create([
            'id'    => 'jabatan_img_tim2',
            'value' => 'Product Manager'
        ]);
        App_config::create([
            'id'    => 'jabatan_img_tim3',
            'value' => 'CTO'
        ]);
        App_config::create([
            'id'    => 'jabatan_img_tim4',
            'value' => 'Accountant'
        ]);
        App_config::create([
            'id'    => 'logo',
            'value' => 'landing_page/img/logo.png'
        ]);
        App_config::create([
            'id'    => 'nama_img_tim1',
            'value' => 'Walter White'
        ]);
        App_config::create([
            'id'    => 'nama_img_tim2',
            'value' => 'Sarah Jhinson'
        ]);
        App_config::create([
            'id'    => 'nama_img_tim3',
            'value' => 'William Anderson'
        ]);
        App_config::create([
            'id'    => 'nama_img_tim4',
            'value' => 'Amanda Jepson'
        ]);
        App_config::create([
            'id'    => 'peta_contact',
            'value' => 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d22864.11283411948!2d-73.96468908098944!3d40.630720240038435!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89c24fa5d33f083b%3A0xc80b8f06e177fe62!2sNew+York%2C+NY%2C+USA!5e0!3m2!1sen!2sbg!4v1540447494452'
        ]);
        App_config::create([
            'id'    => 'title_aboutUs',
            'value' => 'Few Words About Us'
        ]);
        App_config::create([
            'id'    => 'title_action',
            'value' => 'Call To Action'
        ]);
        App_config::create([
            'id'    => 'title_contact',
            'value' => 'Contact'
        ]);
        App_config::create([
            'id'    => 'title_fact',
            'value' => 'Facts'
        ]);
        App_config::create([
            'id'    => 'title_service',
            'value' => 'Services'
        ]);
        App_config::create([
            'id'    => 'title_tim',
            'value' => 'Team'
        ]);
        App_config::create([
            'id'    => 'web_alamat',
            'value' => 'A108 Adam Street<br>New York, NY 535022'
        ]);
        App_config::create([
            'id'    => 'web_email',
            'value' => 'info@example.com'
        ]);
        App_config::create([
            'id'    => 'web_facebook',
            'value' => '#'
        ]);
        App_config::create([
            'id'    => 'web_instagram',
            'value' => '#'
        ]);
        App_config::create([
            'id'    => 'web_nohp',
            'value' => '+1 5589 55488 55'
        ]);
        App_config::create([
            'id'    => 'web_twitter',
            'value' => '#'
        ]);
        App_config::create([
            'id'    => 'config_kasir',
            'value' => '#'
        ]);
        App_config::create([
            'id'    => 'config_pengeluaran',
            'value' => '#'
        ]);
        App_user_level::create([
            'level'    => 'root',
            'value' => 'ROOT'
        ]);
        App_user_level::create([
            'level'    => 'admin',
            'value' => 'Administrator'
        ]);
        App_user_level::create([
            'level'     => 'pelanggan',
            'value'     => 'Pelanggan'
        ]);
        App_theme::create([
            'name'      => 'backend',
            'folder'    => 'admin/'
        ]);
        App_theme::create([
            'name'      => 'Tema 1',
            'folder'    => 'fontend_1/'
        ]);
        App_lang::create([
            'id'    => 'ina',
            'lang'  => 'Indonesia'
        ]);
        App_file::create([
            'filename'  => 'Dashboard',
            'module'    => 'DashboardController',
            'path'      => 'admin_bqs/dashboard',
            'lang_id'   => 'ina',
            'theme_id'  => 1,
        ]);
        App_menu::create([
            'postion'   => '1',
            'sub_id'    => 0,
            'sort'      => 1,
            'file_id'   => 1,
            'theme_id'  => 1,
        ]);
        App_user_acces::create([
            'file_id'   => 1,
            'app_user_level_id'  => 'admin',
            'doshow'    => 1,
            'doadd'     => 1,
            'doedit'    => 1,
            'dodel'     => 1,
        ]);
        App_user_acces::create([
            'file_id'   => 1,
            'app_user_level_id'  => 'root',
            'doshow'    => 1,
            'doadd'     => 1,
            'doedit'    => 1,
            'dodel'     => 1,
        ]);
    	$id = IdGenerator::generate(['table' => 'app_user_list', 'length' => 15, 'prefix' =>'1101010001-']);
        $admin = User::create([
        	'id'	=> $id,
            'name' => 'Ginanjar BQS',
            'email' => 'ginanjarroot@bqs.id',
            'phone_number' => '08982200915',
            'alamat' => 'Kp. Cibalok',
            'status_konfirmasi' => '1',
            'password' => bcrypt('12345'),
            'app_user_level_id' => 'root',
        ]);

        $id = IdGenerator::generate(['table' => 'app_user_list', 'length' => 15, 'prefix' =>'1101010001-']);
        $admin = User::create([
            'id'    => $id,
            'name' => 'Administrator',
            'email' => 'ginanjaradmin@bqs.id',
            'phone_number' => '0898220095',
            'alamat' => 'Kp. Cibalok',
            'status_konfirmasi' => '1',
            'password' => bcrypt('12345'),
            'app_user_level_id' => 'admin',
        ]);

    	$id = IdGenerator::generate(['table' => 'app_user_list', 'length' => 15, 'prefix' =>'1101010001-']);
        $user = User::create([
        	'id'	=> $id,
            'name' => 'User',
            'email' => 'user@bqs.id',
            'phone_number' => '089822009150',
            'alamat' => 'Kp. Cibalok',
            'status_konfirmasi' => '1',
            'password' => bcrypt('12345'),
            'app_user_level_id' => 'pelanggan',
        ]);
        Kategori::create([
            'id'    => '01',
            'nama' => 'UNCATEGORIES',
            'status' => '1',
        ]);
    }
}
